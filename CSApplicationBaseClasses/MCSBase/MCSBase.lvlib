﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the base class for a multi channel scaler (MCS). It provides basic events, some example methods and publishes a lot of services "OBJECTNAME_SERVICENAME". This class can be used as parent class for classes that implemented a specific mulit channel scaler.

A scaler is a counter. A MCS is a counter to accumulate events to a selectable number of bins associated with a programmable segment of time over multiple scans. Thus, a MCS can be used to record events or counts as a function of time. A MCS is typically used for counting signals from detectors as photomultipliers, multi channel plates or channeltrons. Thus, typical applications are high speed photon counting or recording of time-of-flight spectra in bunched ion beams.

Glossary:
bin: a channel in a spectrum
record: data acquired after one trigger
scan: if multiple triggering is used, multiple "records" are accumulated into one "scan".

Remark: A MCS should not be confused with an multi channel analyzer (MCA), which is used to measure pulse-heights on several channels, to enable spectral analysis.

For using this class and child classes, see the file MCSBase_mapping.ini for a description of the required database entries.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neihderr@gsi.de

Last update: 21-MAR-2013

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+$!!!*Q(C=\:1RDB."%%7`%1%BFC!'?;3^1&amp;U!;:U\=IR%5!(:*$BV[#P5&amp;79F,O!LV"5M%!("LG12)4,TJK&gt;NP.+O(8C2#/BWD7&gt;_&gt;67^[?ZJK&lt;18UH0NTL8LE[XXN`4LNOV`R9Y?"X^&lt;2F4&lt;^`*YC#__&gt;G`N`@DK(Y)/U?VD^=O)I`D4`/?;4L&lt;``D`XHZ`%XVTG&lt;ZLG=8^4`-U&amp;`O93`[%^\(_I8?Y`P\W8ZT_#P^OE53(&amp;%AP--3NPNUPU2%`U2%`U2!`U1!`U1!`U1(&gt;U2X&gt;U2X&gt;U2T&gt;U1T&gt;U1T&gt;U1S]&gt;8?B#&amp;TKTEB20#C6*EQ4*9&amp;#58"+?B#@B38BY6-+4]#1]#5`#QR!F0!F0QJ0Q*$S%+?&amp;*?"+?B#@B)65BS&gt;,2Y5FY3+_!*_!*?!+?A)?3#HA#A+"9E$B)!I9#:X!4]!1]!1_X#HA#HI!HY!FY=#PA#8A#HI!HY#'ET%I5GL[DQU-;/4Q/D]0D]$A]J*&lt;$Y`!Y0![0QU-Z/4Q/DQ0B&amp;(33AS!HS"HA0$A]$A^`=HA=(I@(Y8&amp;Y=*56]D)T05X@U?%R?!Q?A]@A-8B))90(Y$&amp;Y$"[$B\1S?!Q?A]@A-8AI*90(Y$&amp;Y$"#D+/6F*$-#D5''90$Q+\P&amp;SCJ&amp;)&lt;(3[[=Z&lt;&amp;46$;C[M61XD/J'5&amp;VAV96482$6C6;&gt;1.7*58VBV2&gt;2"61NL*J1&gt;;#W8$@9'OOQ&amp;&lt;&lt;!ZNA5GW$D0P3*"W[X7WUW'[X8;X6&gt;J^6KJ=6CI@F]LOFUKMFEIP&amp;Y@$CNXN+(.DI_F_[O0MW_&lt;&lt;MPN^]`T'Z`XLS`O`IYW`O7D$VL,`@WLNL8U@,6D]&amp;?6XPT[V\-=#\^#W?DHGFX&gt;-QT2\]"#%K$-Q!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.14.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="MCSBase.constructor.vi" Type="VI" URL="../MCSBase.constructor.vi"/>
		<Item Name="MCSBase.destructor.vi" Type="VI" URL="../MCSBase.destructor.vi"/>
		<Item Name="MCSBase.get data to modify.vi" Type="VI" URL="../MCSBase.get data to modify.vi"/>
		<Item Name="MCSBase.set modified data.vi" Type="VI" URL="../MCSBase.set modified data.vi"/>
		<Item Name="MCSBase.get bins per record.vi" Type="VI" URL="../MCSBase.get bins per record.vi"/>
		<Item Name="MCSBase.get bin width.vi" Type="VI" URL="../MCSBase.get bin width.vi"/>
		<Item Name="MCSBase.get records per scan.vi" Type="VI" URL="../MCSBase.get records per scan.vi"/>
		<Item Name="MCSBase.get discriminator level.vi" Type="VI" URL="../MCSBase.get discriminator level.vi"/>
		<Item Name="MCSBase.get discriminator slope.vi" Type="VI" URL="../MCSBase.get discriminator slope.vi"/>
		<Item Name="MCSBase.get library version.vi" Type="VI" URL="../MCSBase.get library version.vi"/>
		<Item Name="MCSBase.get trigger level.vi" Type="VI" URL="../MCSBase.get trigger level.vi"/>
		<Item Name="MCSBase.get trigger slope.vi" Type="VI" URL="../MCSBase.get trigger slope.vi"/>
		<Item Name="MCSBase.reset.vi" Type="VI" URL="../MCSBase.reset.vi"/>
		<Item Name="MCSBase.set discriminator level.vi" Type="VI" URL="../MCSBase.set discriminator level.vi"/>
		<Item Name="MCSBase.set discriminator slope.vi" Type="VI" URL="../MCSBase.set discriminator slope.vi"/>
		<Item Name="MCSBase.set trigger level.vi" Type="VI" URL="../MCSBase.set trigger level.vi"/>
		<Item Name="MCSBase.set trigger slope.vi" Type="VI" URL="../MCSBase.set trigger slope.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="MCSBase.myEventNames.ctl" Type="VI" URL="../MCSBase.myEventNames.ctl"/>
		<Item Name="MCSBase.serviceType.ctl" Type="VI" URL="../MCSBase.serviceType.ctl"/>
		<Item Name="MCSBase.directionType.ctl" Type="VI" URL="../MCSBase.directionType.ctl"/>
		<Item Name="MCSBase.statusType.ctl" Type="VI" URL="../MCSBase.statusType.ctl"/>
		<Item Name="MCSBase.get i attribute.vi" Type="VI" URL="../MCSBase.get i attribute.vi"/>
		<Item Name="MCSBase.set i attribute.vi" Type="VI" URL="../MCSBase.set i attribute.vi"/>
		<Item Name="MCSBase.ProcCases.vi" Type="VI" URL="../MCSBase.ProcCases.vi"/>
		<Item Name="MCSBase.ProcPeriodic.vi" Type="VI" URL="../MCSBase.ProcPeriodic.vi"/>
		<Item Name="MCSBase.delete events.vi" Type="VI" URL="../MCSBase.delete events.vi"/>
		<Item Name="MCSBase.update services and attribute data.vi" Type="VI" URL="../MCSBase.update services and attribute data.vi"/>
		<Item Name="MCSBase.update_mass services and attribute data.vi" Type="VI" URL="../MCSBase.update_mass services and attribute data.vi"/>
		<Item Name="MCSBase.waveform to byte array.vi" Type="VI" URL="../MCSBase.waveform to byte array.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="MCSBase.i attribute.ctl" Type="VI" URL="../MCSBase.i attribute.ctl"/>
		<Item Name="MCSBase.parameters.ctl" Type="VI" URL="../MCSBase.parameters.ctl"/>
		<Item Name="MCSBase.i attribute.vi" Type="VI" URL="../MCSBase.i attribute.vi"/>
		<Item Name="MCSBase.ProcEvents.vi" Type="VI" URL="../MCSBase.ProcEvents.vi"/>
		<Item Name="MCSBase.add services.vi" Type="VI" URL="../MCSBase.add services.vi"/>
		<Item Name="MCSBase.remove services.vi" Type="VI" URL="../MCSBase.remove services.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="INHERITPRIV.ProcCases.vi" Type="VI" URL="../inheritance/INHERITPRIV.ProcCases.vi"/>
	</Item>
	<Item Name="MCSBase.contents.vi" Type="VI" URL="../MCSBase.contents.vi"/>
	<Item Name="MCSBase_db.ini" Type="Document" URL="../MCSBase_db.ini"/>
	<Item Name="MCSBase_mapping.ini" Type="Document" URL="../MCSBase_mapping.ini"/>
</Library>
