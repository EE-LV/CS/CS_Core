﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the constructor of this class.

With this VI one is permitted to graphically demonstrate its services from DIM to the lifespan of its Object.  The only possible Data-Types are I, D and L.  One can select from all-over the Domain up-to-date CS system, however a maximum of 8 items.  Therefore, one can click through over the Objects to the services or with help from the Filters select several (max 8).   The choice can then be stored and once again accessed.
There are two different graphs.  In Tap "Value Display" the Graph is being updated, if new Values are being released in DIM.  In the other Tap "Trending" the Values are being read out periodically from DIM.
To confirm the possibilities, to allow the window to downsize and the graphs scaled logarithmical. 

INFO2SF

author: Melanie Wolf
maintainer: H.Brand@gsi.de
history: 
2007-02-27 H.Brand@gsi.de, Upgrade to CS 3.1
2010-11-24 H.Brand@gsi.de, Upgrade to CS 3.20

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: H.Brand@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation; either version 2 of 
the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 27-FEB-2007
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+[!!!*Q(C=X:3`&lt;B."%)&gt;`BSBI`1&lt;)^QB419F=5:(#+8C![6WZ&gt;?G7&gt;#.&gt;F&gt;)N%EUEHG"[+L_#B92%?XS\8E&gt;*&lt;-=5)#&amp;G-_?^X`T\MH=[K&gt;IL[;8'3^9^;S6?E];O:&amp;9@[\Y*&gt;6&gt;PRP&amp;QVZW/&gt;Y?[-`7(OOZJ`(D_`;4@Y3^W`=3/YD_R"Z@4^48UK-/D_I@W.0\QS+_P&amp;YPD_'*=^(WZDC@L&amp;W/0F4$V@8^=4`(9V`YH[Y=3[UM3^=.Q&amp;/_(7FMS4N&lt;XYT#5F/&amp;-`WIF:8AG0J\FOX2_2`9Y@M\_``A&amp;YTOQOJDUFUXK6$Z%?/+"/WYKI4(2%TX2%TX2%TX1!TX1!TX1!^X2(&gt;X2(&gt;X2(&gt;X1$&gt;X1$&gt;X1$&lt;UO&gt;+%,8?C=3D)]':1U42IES;!IO31]#5`#E`"QKY1HY5FY%J[%BR1F0!F0QJ0Q*$S5+?&amp;*?"+?B#@BI65FS&lt;L1Y5FY;+_!*_!*?!+?A)?2#HA#A'"9U$BI!I;#9,!*?!+?A)?N!J[!*_!*?!)?QAJY!J[!*_!*?#CJJR+6JCRU?'ADB]@B=8A=(I?(VH*Y("[(R_&amp;R?"AHB]@B=3#=A5ZT%/15/1H/D=0D]0!DB]@B=8A=(I?(5(V#8E_GU*3&amp;$I`"9`!90!;0Q5-,'4Q'D]&amp;D]"A]N*8"9`!90!;0Q=-I'4Q'D]&amp;DA"B$'3_DG6&amp;I*"G#Q=.@@6OM0K7I*&amp;;8V&amp;Z?N:&gt;3\764?YH58A[VB[\W-.5?ENLBKRWKWG'J(9,;0[='L1;D.E3NO#2KRX7,X_%&lt;@)UP]4E_Q[@YJ*4_Y=4&gt;&lt;K@N&gt;KO\OTNN.BONVWMNFUP.ZX0.:D..JV..*B-&gt;\$6L&lt;^XBO\2C0^S_P&lt;LZ]O\K:P0R[O&lt;W`&gt;6Q__:4U9^]`.[NPHXI6F]`\`@DD`&lt;\Q%`6H@$^&gt;_F@_$&lt;KB@;`^W@U#QGP!&amp;=!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.20.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DIMTrendGUI.constructor.vi" Type="VI" URL="../public/DIMTrendGUI.constructor.vi"/>
		<Item Name="DIMTrendGUI.destructor.vi" Type="VI" URL="../public/DIMTrendGUI.destructor.vi"/>
		<Item Name="DIMTrendGUI.get data to modify.vi" Type="VI" URL="../public/DIMTrendGUI.get data to modify.vi"/>
		<Item Name="DIMTrendGUI.set modified data.vi" Type="VI" URL="../public/DIMTrendGUI.set modified data.vi"/>
		<Item Name="DIMTrendGUI.panel.vi" Type="VI" URL="../private/DIMTrendGUI.panel.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DIMTrendGUI.get i attribute.vi" Type="VI" URL="../protected/DIMTrendGUI.get i attribute.vi"/>
		<Item Name="DIMTrendGUI.set i attribute.vi" Type="VI" URL="../protected/DIMTrendGUI.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="DIMTrendGUI.i attribute.ctl" Type="VI" URL="../private/DIMTrendGUI.i attribute.ctl"/>
		<Item Name="DIMTrendGUI.i attribute.vi" Type="VI" URL="../private/DIMTrendGUI.i attribute.vi"/>
	</Item>
	<Item Name="forFriendsOnly" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DIMTrendGUI.about me.vi" Type="VI" URL="../private/DIMTrendGUI.about me.vi"/>
		<Item Name="DIMTrendGUI.CSSysteme.vi" Type="VI" URL="../private/DIMTrendGUI.CSSysteme.vi"/>
		<Item Name="DIMTrendGUI.Service.vi" Type="VI" URL="../private/DIMTrendGUI.Service.vi"/>
		<Item Name="DIMTrendGUI.object.vi" Type="VI" URL="../private/DIMTrendGUI.object.vi"/>
		<Item Name="DIMTrendGUI.waveform.vi" Type="VI" URL="../private/DIMTrendGUI.waveform.vi"/>
		<Item Name="DIMTrendGUI.getSimpleNumericServices.vi" Type="VI" URL="../private/DIMTrendGUI.getSimpleNumericServices.vi"/>
	</Item>
	<Item Name="DIMTrendGUI.rtm" Type="Document" URL="../DIMTrendGUI.rtm"/>
	<Item Name="DIMTrendGUI.contents.vi" Type="VI" URL="../DIMTrendGUI.contents.vi"/>
</Library>
