﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">CSSystemLib - A library that is the core of CS

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2001  Dietrich Beck, Holger Brand, Mathias Richter
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de or h.brand@gsi.de
Last update: 20-Mar-2013</Property>
	<Property Name="NI.Lib.FriendGUID" Type="Str">6f7108d9-d8b3-45e8-b87e-8db4d98fd632</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*-!!!*Q(C=\:1^4C."%%9`6A3EXB/M,,D!FLD"8-%X1"73/C8U"5#KA!3N.P!6@)6+(@I+PM,MGZY;9QDQ3I"%1,&gt;L00.V`&lt;TJ\GGJN1PJ80WJJD@&lt;`YTXIR?8@LTP_`\Y;@#1RP`W7.,"\86]3=`R2QE/Q4LSHO+H8#`CX`V_X_0@YV^X`04H@8@3[:/&lt;&gt;$:^\9E&amp;ZJC.XWKC*XKC*XKC*XKA"XKA"XKA"\KD/\KD/\KD/\KB'\KB'\KBWX4GI!N&gt;[,*WD#4&amp;EU**UC2"YAS+EEP#E`!E0!E0DUJY%J[%*_&amp;*?("2QJ0Q*$Q*4]*$G"+?B#@B38A3(F)VEGQ&gt;(:[%B`1+?!+?A#@A#8AIK9!H!!C+"9G$*'!I'!RO!J[!*_$B6A&amp;0Q"0Q"$Q"$]-+?!+?A#@A#8A);&lt;-3D7&lt;I[0#12A[0Q_0Q/$Q/$[HF]$A]$I`$Y`"14A[0Q_.!/!7&gt;Z#$)#8)=H!?(R_(B4Q[0Q_0Q/$Q/$U.NB&lt;T.T%!T&gt;(2Y$"[$R_!R?!Q?5MDA-8A-(I0(Y#'N$"[$R_!R?!Q?3MHA-8A-(A0%+%JZ'=G-1-0*%!Q?@GWX7&amp;OF;#47?HW;YU:6&lt;5$6RF*N'.6'5#WQ;O&amp;5#[+;;.5%KC:'^=+K&amp;V%"KAKL%KI=N??[QT&lt;9'FNB3WS"&gt;&gt;A=GQWB(_SYX__VW_WUW7SU8K_V7KWU8#[V7#T5&gt;:XG]\FGM^HBN0J&amp;(^P:&gt;#\&gt;=4`9Q`8N^M^6^`DX_G&lt;\^00S`O(XT89;_QQ&lt;T[7P=$&lt;KB`KD9ZYZ_A@C)4TJ!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.30.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Friends List" Type="Friends List">
		<Item Name="CSObj.lvlib" Type="Friended Library" URL="../../../CSObj/CSObj.lvlib"/>
		<Item Name="GeneralObjectGUI.lvlib" Type="Friended Library" URL="../../../GUIs/GeneralObjectGUI/GeneralObjectGUI.lvlib"/>
		<Item Name="DeviceBase.lvlib" Type="Friended Library" URL="../../../CSApplicationBaseClasses/DeviceBase/DeviceBase.lvlib"/>
		<Item Name="CS_Start.vi" Type="Friended VI" URL="../../../../../CS_Start.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CSSystemLib._constructorExecuted.vi" Type="VI" URL="../CSSystemLib._constructorExecuted.vi"/>
		<Item Name="CSSystemLib._delete.vi" Type="VI" URL="../CSSystemLib._delete.vi"/>
		<Item Name="CSSystemLib._delete_nw.vi" Type="VI" URL="../CSSystemLib._delete_nw.vi"/>
		<Item Name="CSSystemLib._getDomain_nw.vi" Type="VI" URL="../CSSystemLib._getDomain_nw.vi"/>
		<Item Name="CSSystemLib._getInstances.vi" Type="VI" URL="../CSSystemLib._getInstances.vi"/>
		<Item Name="CSSystemLib._getInstances_nw.vi" Type="VI" URL="../CSSystemLib._getInstances_nw.vi"/>
		<Item Name="CSSystemLib._getMyClasses.vi" Type="VI" URL="../CSSystemLib._getMyClasses.vi"/>
		<Item Name="CSSystemLib._getSystemInstances_nw.vi" Type="VI" URL="../CSSystemLib._getSystemInstances_nw.vi"/>
		<Item Name="CSSystemLib._getSystems_nw.vi" Type="VI" URL="../CSSystemLib._getSystems_nw.vi"/>
		<Item Name="CSSystemLib._new.vi" Type="VI" URL="../CSSystemLib._new.vi"/>
		<Item Name="CSSystemLib._new_nw.vi" Type="VI" URL="../CSSystemLib._new_nw.vi"/>
		<Item Name="CSSystemLib._startThread.vi" Type="VI" URL="../CSSystemLib._startThread.vi"/>
		<Item Name="CSSystemLib._stopThread.vi" Type="VI" URL="../CSSystemLib._stopThread.vi"/>
		<Item Name="CSSystemLib.get classes.vi" Type="VI" URL="../CSSystemLib.get classes.vi"/>
		<Item Name="CSSystemLib.get library version.vi" Type="VI" URL="../CSSystemLib.get library version.vi"/>
		<Item Name="CSSystemLib.get my super proc.vi" Type="VI" URL="../CSSystemLib.get my super proc.vi"/>
		<Item Name="CSSystemLib.get my super proc_nw.vi" Type="VI" URL="../CSSystemLib.get my super proc_nw.vi"/>
		<Item Name="CSSystemLib.get sql data.vi" Type="VI" URL="../CSSystemLib.get sql data.vi"/>
		<Item Name="CSSystemLib.get sql data my class.vi" Type="VI" URL="../CSSystemLib.get sql data my class.vi"/>
		<Item Name="CSSystemLib.get VI path.vi" Type="VI" URL="../CSSystemLib.get VI path.vi"/>
		<Item Name="CSSystemLib.kill all classes.vi" Type="VI" URL="../CSSystemLib.kill all classes.vi"/>
		<Item Name="CSSystemLib.kill class.vi" Type="VI" URL="../CSSystemLib.kill class.vi"/>
		<Item Name="CSSystemLib.kill object.vi" Type="VI" URL="../CSSystemLib.kill object.vi"/>
		<Item Name="CSSystemLib.name2class_nw.vi" Type="VI" URL="../CSSystemLib.name2class_nw.vi"/>
		<Item Name="CSSystemLib.name2ref.vi" Type="VI" URL="../CSSystemLib.name2ref.vi"/>
		<Item Name="CSSystemLib.object condition.ctl" Type="VI" URL="../CSSystemLib.object condition.ctl"/>
		<Item Name="CSSystemLib.objectCoordinates_nw.vi" Type="VI" URL="../CSSystemLib.objectCoordinates_nw.vi"/>
		<Item Name="CSSystemLib.objectExists.vi" Type="VI" URL="../CSSystemLib.objectExists.vi"/>
		<Item Name="CSSystemLib.operate_nw.vi" Type="VI" URL="../CSSystemLib.operate_nw.vi"/>
		<Item Name="CSSystemLib.ref2class.vi" Type="VI" URL="../CSSystemLib.ref2class.vi"/>
		<Item Name="CSSystemLib.ref2classNames.vi" Type="VI" URL="../CSSystemLib.ref2classNames.vi"/>
		<Item Name="CSSystemLib.ref2instanceNames.vi" Type="VI" URL="../CSSystemLib.ref2instanceNames.vi"/>
		<Item Name="CSSystemLib.ref2name.vi" Type="VI" URL="../CSSystemLib.ref2name.vi"/>
		<Item Name="CSSystemLib.systemID_nw.vi" Type="VI" URL="../CSSystemLib.systemID_nw.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="CSSystemLib.concatInstanceList_nw.vi" Type="VI" URL="../CSSystemLib.concatInstanceList_nw.vi"/>
		<Item Name="CSSystemLib.ctl2ref.vi" Type="VI" URL="../CSSystemLib.ctl2ref.vi"/>
		<Item Name="CSSystemLib.objectExists_nw.vi" Type="VI" URL="../CSSystemLib.objectExists_nw.vi"/>
	</Item>
	<Item Name="forFriendsOnly" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">4</Property>
		<Item Name="CSSystemLib.instances options.ctl" Type="VI" URL="../CSSystemLib.instances options.ctl"/>
		<Item Name="CSSystemLib.instances.vi" Type="VI" URL="../CSSystemLib.instances.vi"/>
		<Item Name="CSSystemLib.updateInstanceList_nw.vi" Type="VI" URL="../CSSystemLib.updateInstanceList_nw.vi"/>
		<Item Name="CSSystemLib.instances_nw.vi" Type="VI" URL="../CSSystemLib.instances_nw.vi"/>
		<Item Name="CSSystemLib.create advanced GUI.vi" Type="VI" URL="../CSSystemLib.create advanced GUI.vi"/>
		<Item Name="CSSystemLib.CS_Start domain listener.vi" Type="VI" URL="../CSSystemLib.CS_Start domain listener.vi"/>
		<Item Name="CSSystemLib.CS_Start watchdog.vi" Type="VI" URL="../CSSystemLib.CS_Start watchdog.vi"/>
		<Item Name="CSSystemLib.CS_Start dispatch message box.vi" Type="VI" URL="../CSSystemLib.CS_Start dispatch message box.vi"/>
	</Item>
</Library>
