﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2014_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2014\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2014_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2014\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the base class for a network analyzer to obtain FFT spectras necessary for the FT-ICR detection technique. The class is designed to work together with MM9 (not MM6-MM8!).


author: Dennis Neidherr, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2016  Dennis Neidherr
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.neidherr@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neihderr@gsi.de

Last update: 11-JUL-2016

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="NABase.constructor.vi" Type="VI" URL="../NABase.constructor.vi"/>
		<Item Name="NABase.destructor.vi" Type="VI" URL="../NABase.destructor.vi"/>
		<Item Name="NABase.get data to modify.vi" Type="VI" URL="../NABase.get data to modify.vi"/>
		<Item Name="NABase.get frequency span.vi" Type="VI" URL="../NABase.get frequency span.vi"/>
		<Item Name="NABase.get library version.vi" Type="VI" URL="../NABase.get library version.vi"/>
		<Item Name="NABase.get Number of Lines.vi" Type="VI" URL="../NABase.get Number of Lines.vi"/>
		<Item Name="NABase.get Number of Stored Data.vi" Type="VI" URL="../NABase.get Number of Stored Data.vi"/>
		<Item Name="NABase.get start frequency.vi" Type="VI" URL="../NABase.get start frequency.vi"/>
		<Item Name="NABase.get Time Record Duration.vi" Type="VI" URL="../NABase.get Time Record Duration.vi"/>
		<Item Name="NABase.get trigger level.vi" Type="VI" URL="../NABase.get trigger level.vi"/>
		<Item Name="NABase.get trigger slope.vi" Type="VI" URL="../NABase.get trigger slope.vi"/>
		<Item Name="NABase.reset.vi" Type="VI" URL="../NABase.reset.vi"/>
		<Item Name="NABase.set modified data.vi" Type="VI" URL="../NABase.set modified data.vi"/>
		<Item Name="NABase.set trigger level.vi" Type="VI" URL="../NABase.set trigger level.vi"/>
		<Item Name="NABase.set trigger slope.vi" Type="VI" URL="../NABase.set trigger slope.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="NABase.myEventNames.ctl" Type="VI" URL="../NABase.myEventNames.ctl"/>
		<Item Name="NABase.serviceType.ctl" Type="VI" URL="../NABase.serviceType.ctl"/>
		<Item Name="NABase.directionType.ctl" Type="VI" URL="../NABase.directionType.ctl"/>
		<Item Name="NABase.statusType.ctl" Type="VI" URL="../NABase.statusType.ctl"/>
		<Item Name="NABase.get i attribute.vi" Type="VI" URL="../NABase.get i attribute.vi"/>
		<Item Name="NABase.set i attribute.vi" Type="VI" URL="../NABase.set i attribute.vi"/>
		<Item Name="NABase.ProcCases.vi" Type="VI" URL="../NABase.ProcCases.vi"/>
		<Item Name="NABase.ProcPeriodic.vi" Type="VI" URL="../NABase.ProcPeriodic.vi"/>
		<Item Name="NABase.delete events.vi" Type="VI" URL="../NABase.delete events.vi"/>
		<Item Name="NABase.update services and attribute data.vi" Type="VI" URL="../NABase.update services and attribute data.vi"/>
		<Item Name="NABase.update_mass services and attribute data.vi" Type="VI" URL="../NABase.update_mass services and attribute data.vi"/>
		<Item Name="NABase.waveform to byte array.vi" Type="VI" URL="../NABase.waveform to byte array.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="NABase.i attribute.ctl" Type="VI" URL="../NABase.i attribute.ctl"/>
		<Item Name="NABase.parameters.ctl" Type="VI" URL="../NABase.parameters.ctl"/>
		<Item Name="NABase.i attribute.vi" Type="VI" URL="../NABase.i attribute.vi"/>
		<Item Name="NABase.ProcEvents.vi" Type="VI" URL="../NABase.ProcEvents.vi"/>
		<Item Name="NABase.add services.vi" Type="VI" URL="../NABase.add services.vi"/>
		<Item Name="NABase.remove services.vi" Type="VI" URL="../NABase.remove services.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="INHERITPRIV.ProcCases.vi" Type="VI" URL="../inheritance/INHERITPRIV.ProcCases.vi"/>
	</Item>
	<Item Name="NABase.contents.vi" Type="VI" URL="../NABase.contents.vi"/>
	<Item Name="NABase_db.ini" Type="Document" URL="../NABase_db.ini"/>
	<Item Name="NABase_mapping.ini" Type="Document" URL="../NABase_mapping.ini"/>
</Library>
