﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the base class for a motor. This could be a stepper motor, a servo motor or a piezo motor.It provides basic events, some example methods and publishes a lot of services "OBJECTNAME_SERVICENAME". This class can be used as parent class for classes that implemented a specific motor class for a motion application.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de

Last update: 21-MAR-2013

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*G!!!*Q(C=\:1RDB."%%8`)E9C1R98!+?%&amp;:,[#LZ#8=(JBIZXIZ*)S."M2,B-QA(K#L[#9\,B4&lt;P''!ONR=)C!LJ&gt;^P3PLKIXX?W77HMB0&gt;&gt;YK850.PSND\..7H==&gt;\0?R+Y&lt;D\U.P]@0`J091[YT`UHU?&amp;&lt;`'(`#UVXGP^4U90PP`YP_^[X^LP_(08W%``(V@^+?XH`Z?&amp;^@`B-]&lt;:/O'CG77'#/78O\-&gt;%40&gt;%40&gt;%40&gt;!$0&gt;!$0&gt;!$X&gt;%&gt;X&gt;%&gt;X&gt;%&gt;X&gt;!.X&gt;!.X&gt;!.P86UI1N&gt;[+R+5DQJF#2.%C3416(SF@!E0!F0QM.1#5`#E`!E0!E05Z4Q*$Q*4]+4]"#GB#@B38A3HI3(6)UE7U?(*_%BP1+?A#@A#8A#(EIKY!E!AG*"YC!*'!K=Q50!%`!%0$QKY!FY!J[!*_$"L9!HY!FY!J[!BZ#W+N&amp;IJIY/$WHE]$A]$I`$Y`#17A[0Q_0Q/$Q/$_8E]$A]$I24U%E/AJQA:Y)T=(A=(H\E]$A]$I`$Y`$A;DPE&lt;75GGKGDQW0Q'$Q'D]&amp;D]*"#"I`"9`!90!90;78Q'$Q'D]&amp;D]&amp;"+"I`"9`!9)%:2SMN):A1;EQT"Y/(44IOV89J'9KX88`.Q5&amp;5(5(7Q6!&gt;'&gt;2"5'[T;/.7'K":;N9#KB6'^M/J&amp;6)#KQKK%KIH;]\X$"KT(NNA'7W-L&lt;)ENJN!`0('`XWOXWWE9"P6^L_VWK]VGI`6[L&gt;6KJ?6SK=6C=&lt;SN8N-0\7K_F[ZZPBN?X8__[&gt;`UN_]_^4=@0^T&gt;PLW@^.H`3`&lt;S3^H8MBK@T4P=3``#X;BH'E_O?&gt;&lt;I'`2AD!A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.14.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="MotorBase.constructor.vi" Type="VI" URL="../MotorBase.constructor.vi"/>
		<Item Name="MotorBase.destructor.vi" Type="VI" URL="../MotorBase.destructor.vi"/>
		<Item Name="MotorBase.get data to modify.vi" Type="VI" URL="../MotorBase.get data to modify.vi"/>
		<Item Name="MotorBase.set modified data.vi" Type="VI" URL="../MotorBase.set modified data.vi"/>
		<Item Name="MotorBase.get axis acceleration.vi" Type="VI" URL="../MotorBase.get axis acceleration.vi"/>
		<Item Name="MotorBase.get axis names.vi" Type="VI" URL="../MotorBase.get axis names.vi"/>
		<Item Name="MotorBase.get axis on off.vi" Type="VI" URL="../MotorBase.get axis on off.vi"/>
		<Item Name="MotorBase.get axis position.vi" Type="VI" URL="../MotorBase.get axis position.vi"/>
		<Item Name="MotorBase.get axis range max.vi" Type="VI" URL="../MotorBase.get axis range max.vi"/>
		<Item Name="MotorBase.get axis range min.vi" Type="VI" URL="../MotorBase.get axis range min.vi"/>
		<Item Name="MotorBase.get axis status.vi" Type="VI" URL="../MotorBase.get axis status.vi"/>
		<Item Name="MotorBase.get axis start velocity.vi" Type="VI" URL="../MotorBase.get axis start velocity.vi"/>
		<Item Name="MotorBase.get axis stop velocity.vi" Type="VI" URL="../MotorBase.get axis stop velocity.vi"/>
		<Item Name="MotorBase.get axis units.vi" Type="VI" URL="../MotorBase.get axis units.vi"/>
		<Item Name="MotorBase.get default axis index.vi" Type="VI" URL="../MotorBase.get default axis index.vi"/>
		<Item Name="MotorBase.get library version.vi" Type="VI" URL="../MotorBase.get library version.vi"/>
		<Item Name="MotorBase.get nof axes.vi" Type="VI" URL="../MotorBase.get nof axes.vi"/>
		<Item Name="MotorBase.set axis position.vi" Type="VI" URL="../MotorBase.set axis position.vi"/>
		<Item Name="MotorBase.set axis range max.vi" Type="VI" URL="../MotorBase.set axis range max.vi"/>
		<Item Name="MotorBase.set axis range min.vi" Type="VI" URL="../MotorBase.set axis range min.vi"/>
		<Item Name="MotorBase.set default axis index.vi" Type="VI" URL="../MotorBase.set default axis index.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="MotorBase.myEventNames.ctl" Type="VI" URL="../MotorBase.myEventNames.ctl"/>
		<Item Name="MotorBase.serviceType.ctl" Type="VI" URL="../MotorBase.serviceType.ctl"/>
		<Item Name="MotorBase.directionType.ctl" Type="VI" URL="../MotorBase.directionType.ctl"/>
		<Item Name="MotorBase.statusType.ctl" Type="VI" URL="../MotorBase.statusType.ctl"/>
		<Item Name="MotorBase.ProcCases.vi" Type="VI" URL="../MotorBase.ProcCases.vi"/>
		<Item Name="MotorBase.ProcPeriodic.vi" Type="VI" URL="../MotorBase.ProcPeriodic.vi"/>
		<Item Name="MotorBase.get i attribute.vi" Type="VI" URL="../MotorBase.get i attribute.vi"/>
		<Item Name="MotorBase.set i attribute.vi" Type="VI" URL="../MotorBase.set i attribute.vi"/>
		<Item Name="MotorBase.add services.vi" Type="VI" URL="../MotorBase.add services.vi"/>
		<Item Name="MotorBase.axis index in range.vi" Type="VI" URL="../MotorBase.axis index in range.vi"/>
		<Item Name="MotorBase.delete events.vi" Type="VI" URL="../MotorBase.delete events.vi"/>
		<Item Name="MotorBase.get axis scaling.vi" Type="VI" URL="../MotorBase.get axis scaling.vi"/>
		<Item Name="MotorBase.requested position in range.vi" Type="VI" URL="../MotorBase.requested position in range.vi"/>
		<Item Name="MotorBase.set axis scaling.vi" Type="VI" URL="../MotorBase.set axis scaling.vi"/>
		<Item Name="MotorBase.set status bit.vi" Type="VI" URL="../MotorBase.set status bit.vi"/>
		<Item Name="MotorBase.update services and attribute data.vi" Type="VI" URL="../MotorBase.update services and attribute data.vi"/>
		<Item Name="MotorBase.update_mass services and attribute data.vi" Type="VI" URL="../MotorBase.update_mass services and attribute data.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="MotorBase.i attribute.ctl" Type="VI" URL="../MotorBase.i attribute.ctl"/>
		<Item Name="MotorBase.parameters.ctl" Type="VI" URL="../MotorBase.parameters.ctl"/>
		<Item Name="MotorBase.i attribute.vi" Type="VI" URL="../MotorBase.i attribute.vi"/>
		<Item Name="MotorBase.ProcEvents.vi" Type="VI" URL="../MotorBase.ProcEvents.vi"/>
		<Item Name="MotorBase.remove services.vi" Type="VI" URL="../MotorBase.remove services.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="INHERITPRIV.ProcCases.vi" Type="VI" URL="../inheritance/INHERITPRIV.ProcCases.vi"/>
	</Item>
	<Item Name="MotorBase.contents.vi" Type="VI" URL="../MotorBase.contents.vi"/>
</Library>
