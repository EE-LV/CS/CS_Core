﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">CSAccessLib - provides "user routines" for the CS Access System

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2001  Dietrich Beck, Holger Brand, Mathias Richter
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de or h.brand@gsi.de
Last update: 20-Mar-2013

</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*6!!!*Q(C=\&gt;1R&lt;NN!%)8BZS"&amp;SP!'A8+%/9%$.3Z&gt;K%RB)*D?F6K8;A7E'=#&amp;Y6*85-VOLK!L[!LST_6)IJKIC9%%-/G6R&lt;@,W9^,CF,&lt;PEC@&gt;&lt;CW[9`&lt;U.`XP9&lt;0Y&lt;A`N@\9@TLM*`W[\$]?4];.6=`^9^TXOCBY7@]]][2`YDN8HN3`?HU@`2`^`W``^:`XU^6"\\R*.UV+3VL1H'&lt;N[AZ*HO2*HO2*HO2"(O2"(O2"(O2/\O2/\O2/\O2'&lt;O2'&lt;O2'&lt;O2N*R?ZS%8/KC34*R-F2:-#S7!I3DY34_**0)G(1S7?R*.Y%E`C99A34_**0)EH]8#;%E`C34S**`&amp;1KEGS\?2Y%A`F&amp;8A#4_!*0)'(+26Y!E!Q76!Y+!*$17@Q*@!%HM$$6Q7?Q".Y!E`AI6O"*`!%HM!4?$CFL5IUT&lt;#4Y['-()`D=4S/R`&amp;17I\(]4A?R_.YG%[/R`%Y#'&gt;#JTA%/3=Z!ZQ$R_.Y_#@(YXA=D_.R0(3V/_2N:1&lt;.M*0D-4S'R`!9(M.$#2E?QW.Y$)`BI;Q-D_%R0)&lt;(]$#6$)`B-4Q'R*C5[755-UYU"BG"Y?'P03X7\F)UC&lt;7^@JLDA[J[!&amp;50FOK"54U)KBOMOH'K'[*;;.5#KB:'&gt;='K#V%"62/L#KI';M`HDL;F&lt;7ALWJ+WI-VJ-VIXH0K8"_\X?_VW/WWX7WUW'[V7+SW83SU7#]XH=]VG-X6&gt;&gt;XJ&lt;@7-@NZPD?_G*\]]0X?XL_OPNSY`VY]PX^&gt;XL\`O\ZV]`(Y?_^WLD?_F@?$@KEQ[4VTRL^!9F%3M"!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.30.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CSAccessLib.accessID2priority.vi" Type="VI" URL="../CSAccessLib.accessID2priority.vi"/>
		<Item Name="CSAccessLib.acquire object tree.vi" Type="VI" URL="../CSAccessLib.acquire object tree.vi"/>
		<Item Name="CSAccessLib.calc tree.vi" Type="VI" URL="../CSAccessLib.calc tree.vi"/>
		<Item Name="CSAccessLib.check access ID.vi" Type="VI" URL="../CSAccessLib.check access ID.vi"/>
		<Item Name="CSAccessLib.encrypt access ID.vi" Type="VI" URL="../CSAccessLib.encrypt access ID.vi"/>
		<Item Name="CSAccessLib.get library version.vi" Type="VI" URL="../CSAccessLib.get library version.vi"/>
		<Item Name="CSAccessLib.get tree children.vi" Type="VI" URL="../CSAccessLib.get tree children.vi"/>
		<Item Name="CSAccessLib.login client GUI.vi" Type="VI" URL="../CSAccessLib.login client GUI.vi"/>
		<Item Name="CSAccessLib.login client.vi" Type="VI" URL="../CSAccessLib.login client.vi"/>
		<Item Name="CSAccessLib.obtain and check access ID.vi" Type="VI" URL="../CSAccessLib.obtain and check access ID.vi"/>
		<Item Name="CSAccessLib.register object.vi" Type="VI" URL="../CSAccessLib.register object.vi"/>
		<Item Name="CSAccessLib.release object tree.vi" Type="VI" URL="../CSAccessLib.release object tree.vi"/>
		<Item Name="CSAccessLib.set access ID.vi" Type="VI" URL="../CSAccessLib.set access ID.vi"/>
		<Item Name="CSAccessLib.set subtree color.vi" Type="VI" URL="../CSAccessLib.set subtree color.vi"/>
		<Item Name="CSAccessLib.unregister object.vi" Type="VI" URL="../CSAccessLib.unregister object.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="CSAccessLib.acquire object.vi" Type="VI" URL="../CSAccessLib.acquire object.vi"/>
		<Item Name="CSAccessLib.crypt access ID.vi" Type="VI" URL="../CSAccessLib.crypt access ID.vi"/>
		<Item Name="CSAccessLib.release object.vi" Type="VI" URL="../CSAccessLib.release object.vi"/>
	</Item>
</Library>
