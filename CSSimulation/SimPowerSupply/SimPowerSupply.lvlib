﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This class simulates power supply channels. It may serve as an example for a class for a real device.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: M.Richter@gsi.de, H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 17-JUN-2004

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+J!!!*Q(C=Z:1RCBN"%%7`T!I=/&amp;(O9+X&gt;!RAK=&lt;$A2/"-G&gt;A&lt;V"75+H#A)\A3RU&lt;'O5("[A!&amp;?Q*&gt;1@FCG`'&lt;HN9CVN))AQU'&gt;[MU0&lt;_[KNZU^YR5WH0J1MWZNOFNH8]YX(3^'W0$&lt;L$XN\]C&lt;BYH$E`&amp;V[G(`MW4`*P.M#@_V`J^`/?;?NN`Z0^SU)\[&gt;74YV.]UURZ`?T.N_O)V02V@BKW\/2V@QHPS4U`T(TU5P`0]_`(6V3F`U\F0_PPDD_`0_?/^/0]3`.UG$1IJFFBADFFZOC&lt;2%TX2%TX2%TX1!TX1!TX1!^X2(&gt;X2(&gt;X2(&gt;X1$&gt;X1$&gt;X1L4VT&lt;5=8ON"FZ2QGR:.#3&gt;)E14):&amp;#6`#5`#E`!E0.QKY5FY%J[%*_&amp;BCB+?B#@B38A3(M+5]#1]#5`#E`#1KJ"E[?DQ*$SE6]!4]!1]!5`!1UE&amp;0!&amp;!5#R)(#1"1Y%T'!1]!5`!QV!"4]!4]!1]!1^O"4Q"4]!4]!1]B*26C5,4&gt;H2Y3#/(R_&amp;R?"Q?BY@5=HA=(I@(Y8&amp;Y+#?(R_&amp;R)*S#4H)1Z!1Z%ZQ&lt;B]@BY3+(R_&amp;R?"Q?BQ&gt;8W3%P+^03N"U&gt;(I0(Y$&amp;Y$"[$BR1S?!Q?A]@A-8B)+Y0(Y$&amp;Y$"[$BV)S?!Q?A]=!-9J38E9S)^#9:!A'$\^S7KTM5B13+\W_GNV"64W!KA&gt;,^=#I(A46$6&lt;&gt;/.5.56VIV16582D6"V:^%&amp;6!V=+K#65H;M@`&amp;FND+WS*T&lt;%:.M('W+A.`=-4&gt;\O&gt;NNONVOOV6KO6FMOFZP/Z:L/:*J/*RO/R2K02Y^@KEN[VQ?&amp;X[?(WQ`WX&gt;Z`P(GY`X@WY`=DV[`X?NW"OL\X^0FB=8AQ7L^Y0&amp;N@8X@DVC_\;WN\`ZO6A=8048&gt;'[\^+`]'X5-T5(HXH7[#&gt;^H?5S!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.12.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SimPowerSupply.constructor.vi" Type="VI" URL="../SimPowerSupply.constructor.vi"/>
		<Item Name="SimPowerSupply.destructor.vi" Type="VI" URL="../SimPowerSupply.destructor.vi"/>
		<Item Name="SimPowerSupply.emergency off.vi" Type="VI" URL="../SimPowerSupply.emergency off.vi"/>
		<Item Name="SimPowerSupply.get data to modify.vi" Type="VI" URL="../SimPowerSupply.get data to modify.vi"/>
		<Item Name="SimPowerSupply.get library version.vi" Type="VI" URL="../SimPowerSupply.get library version.vi"/>
		<Item Name="SimPowerSupply.set modified data.vi" Type="VI" URL="../SimPowerSupply.set modified data.vi"/>
		<Item Name="SimPowerSupply.set channel voltage.vi" Type="VI" URL="../SimPowerSupply.set channel voltage.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SimPowerSupply.get i attribute.vi" Type="VI" URL="../SimPowerSupply.get i attribute.vi"/>
		<Item Name="SimPowerSupply.set i attribute.vi" Type="VI" URL="../SimPowerSupply.set i attribute.vi"/>
		<Item Name="SimPowerSupply.ProcCases.vi" Type="VI" URL="../SimPowerSupply.ProcCases.vi"/>
		<Item Name="SimPowerSupply.ProcPeriodic.vi" Type="VI" URL="../SimPowerSupply.ProcPeriodic.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="SimPowerSupply.i attribute.ctl" Type="VI" URL="../SimPowerSupply.i attribute.ctl"/>
		<Item Name="SimPowerSupply.i attribute.vi" Type="VI" URL="../SimPowerSupply.i attribute.vi"/>
		<Item Name="SimPowerSupply.ProcEvents.vi" Type="VI" URL="../SimPowerSupply.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="SimPowerSupply.contents.vi" Type="VI" URL="../SimPowerSupply.contents.vi"/>
	<Item Name="SimPowerSupply_db.ini" Type="Document" URL="../SimPowerSupply_db.ini"/>
	<Item Name="SimPowerSupply_SVTemplate.lvlib" Type="Library" URL="../SimPowerSupply_SVTemplate.lvlib"/>
	<Item Name="SimPowerSupply_SVTemplate.csv" Type="Document" URL="../SimPowerSupply_SVTemplate.csv"/>
</Library>
