﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This class simulates a motion device. It may serve as an example for a class for a real device.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: M.Richter@gsi.de, H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 17-JUN-2004

INFO2SF</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)V!!!*Q(C=\:4.E2J"$)5@LDXY[AR=J+#,!S!&amp;5F!+8$G3AF)A"3Y%]&amp;)A"6,!X`3IW*`;APVTV2\=D7;9*\8U48&gt;03[0^F"ZUO&gt;?/.^PEHY,G0N]ODT&lt;\D]@D^@%;W0&gt;8RL`U0Z0@-,ZL8OP@ZL`8&gt;,0^^\`$`WOUT`K@L=E(`"_P`UL\`P\\WXN\`S0YNUV;$&amp;,-7''*R8C\C^'.&lt;H3D'^XIB6\IB6\IB6\IC:\IC:\IC:\IA2\IA2\IA2\II[-,8?B#:V:-=60)*$5*4$!I-B@$9XA-D_(B59&lt;(]"A?QW.Y#*(B-4S'R`!9(I&lt;*]"A?QW.Y$!_J"IF(2Y@(]*"?"5`"5`!50!50*68Q&amp;!"&amp;M3*RE11-&amp;=\C4]&amp;4]"1]`&amp;8"5`!50!60Q9.&lt;"5`"5`!50!501];MV+#:/DI]J&amp;(#E`!E0!F0QE.K*4Q*4]+4]#1]F&amp;0#E`!E%%H"*$E)3A9F!=F$QJ0Q=&amp;0#E`!E0!F0QI.LL&amp;#/G:FIJIY/4]!4]!1]!5`!1QI&amp;0!&amp;0Q"0Q"$SE6=!4]!1]!5`!1SE&amp;0!&amp;0Q"/!"%5JLS":-$!)#I3!B^`9,4&amp;7K1:*D.[@ZLR2V2N1P&lt;(5'U;^%&gt;1,L&amp;YY^9+I*VI^A?K*5&lt;_Q_E85A/L#[I4K1*WZHL!$NM&gt;WW!:&lt;9SNMC9VT\YM$T_?T4K?4$I?$^PO^&gt;LO&gt;.JO.VOOV6KO6FMOFHJ[YP_FT7\TV8.I3_T\\UX9\&lt;D[8PM0:K"_[0$HGG;/`/,\I&gt;A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.12.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SimMotor.constructor.vi" Type="VI" URL="../SimMotor.constructor.vi"/>
		<Item Name="SimMotor.destructor.vi" Type="VI" URL="../SimMotor.destructor.vi"/>
		<Item Name="SimMotor.get data to modify.vi" Type="VI" URL="../SimMotor.get data to modify.vi"/>
		<Item Name="SimMotor.get library version.vi" Type="VI" URL="../SimMotor.get library version.vi"/>
		<Item Name="SimMotor.set modified data.vi" Type="VI" URL="../SimMotor.set modified data.vi"/>
		<Item Name="SimMotor.find axis reference.vi" Type="VI" URL="../SimMotor.find axis reference.vi"/>
		<Item Name="SimMotor.halt axis.vi" Type="VI" URL="../SimMotor.halt axis.vi"/>
		<Item Name="SimMotor.move axis absolute.vi" Type="VI" URL="../SimMotor.move axis absolute.vi"/>
		<Item Name="SimMotor.move axis relative.vi" Type="VI" URL="../SimMotor.move axis relative.vi"/>
		<Item Name="SimMotor.stop axis.vi" Type="VI" URL="../SimMotor.stop axis.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SimMotor.get i attribute.vi" Type="VI" URL="../SimMotor.get i attribute.vi"/>
		<Item Name="SimMotor.set i attribute.vi" Type="VI" URL="../SimMotor.set i attribute.vi"/>
		<Item Name="SimMotor.ProcCases.vi" Type="VI" URL="../SimMotor.ProcCases.vi"/>
		<Item Name="SimMotor.ProcPeriodic.vi" Type="VI" URL="../SimMotor.ProcPeriodic.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="SimMotor.i attribute.ctl" Type="VI" URL="../SimMotor.i attribute.ctl"/>
		<Item Name="SimMotor.i attribute.vi" Type="VI" URL="../SimMotor.i attribute.vi"/>
		<Item Name="SimMotor.ProcEvents.vi" Type="VI" URL="../SimMotor.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
	</Item>
	<Item Name="SimMotor.contents.vi" Type="VI" URL="../SimMotor.contents.vi"/>
	<Item Name="SimMotor_db.ini" Type="Document" URL="../SimMotor_db.ini"/>
</Library>
