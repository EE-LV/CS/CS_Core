﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">CoreLib - Contains some fundamental routines for CS.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2001  Dietrich Beck, Holger Brand, Mathias Richter
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de or h.brand@gsi.de
Last update: 20-Mar-2013
</Property>
	<Property Name="NI.Lib.FriendGUID" Type="Str">b29fd0e7-74dd-406c-895c-a402583cf223</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*"!!!*Q(C=\:;R51-R%%5`$!(J&gt;=#YB7X"$4CYA*"E=S+HB![=/#$9&amp;NS#7^C%!KY&amp;NW$?[&gt;9'"A9T!]Q1)(HP4H_FX8?34G/JF7PJ3I&gt;T2:_7,`H(S[&amp;O8*J5)]MVNKO_&lt;H`A@WE@XPF0TW`(H`,8K)HB*@_XX_`@`_``M`\TH`@$W5[`8+3,/BE/C18GG%U@&lt;K)H?K)H?K)H?K!(?K!(?K!(OK-\OK-\OK-\OK%&lt;OK%&lt;OK(&lt;];2!&amp;\L1:?XE3*)HC:+A39#E-SB+,AF0QJ0Q*$QUF@!E0!F0QJ0QU%5*4]+4]#1]#1`$F0!E0!F0QJ0Q%+K2:+PI]#1]B&amp;@!%`!%0!&amp;0Q%.+"4Q"1*!M#"Q%!5/"-XA)?!+?A)&gt;("4Q"4]!4]!1]O"8Q"$Q"4]!4]$#ET5IUGL'CQU-9/4Q/D]0D]$A]B*&lt;$Y`!Y0![0QU-[/4Q/DQ0B*(3#AS"HE.0";4A]$A]X/4Q/D]0D]$A]O.I+?:O:E7;M[0!90!;0Q70Q'$S%E-&amp;D]"A]"I`"1VA:0!;0Q70Q'$SEEM&amp;D]"A]"IC2F01SABE$D5['90$Q;\P&amp;WCJ&amp;)\&amp;7[^/=.KJK![IWFGL$K$;#;I&amp;6#[&gt;;%.6%KS:1.4'K&amp;V;^C!J1F6A65.62?[Y$NM/WW!J&lt;9DUWRW:9.Q\^Y9\\`6\$-'CXWWG\X7KV7GGZ8+LP?]XH=]VG-X6&gt;&gt;TKN&lt;KB4O4C?3Q]]D\;/J\N.XSUW`?:_=^MNVI`$X&gt;(X'T;&gt;3X`B&lt;.2F`&gt;=ZT&gt;%TL[IXZA!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.30.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Friends List" Type="Friends List">
		<Item Name="CSSystemLib.lvlib" Type="Friended Library" URL="../../CSSystemLib/CSSystemLib.lvlib"/>
		<Item Name="CSClassUtilities.InheritClass.vi" Type="Friended VI" URL="../../../../../CSUtilities/CSClassUtilities.InheritClass.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CoreLib.BaseProcess flatten remote cluster.vi" Type="VI" URL="../CoreLib.BaseProcess flatten remote cluster.vi"/>
		<Item Name="CoreLib.BaseProcess unflatten remote cluster.vi" Type="VI" URL="../CoreLib.BaseProcess unflatten remote cluster.vi"/>
		<Item Name="CoreLib.byte array 2 data.vi" Type="VI" URL="../CoreLib.byte array 2 data.vi"/>
		<Item Name="CoreLib.CAEObj flatten remote cluster.vi" Type="VI" URL="../CoreLib.CAEObj flatten remote cluster.vi"/>
		<Item Name="CoreLib.CAEObj unflatten remote cluster.vi" Type="VI" URL="../CoreLib.CAEObj unflatten remote cluster.vi"/>
		<Item Name="CoreLib.call process.vi" Type="VI" URL="../CoreLib.call process.vi"/>
		<Item Name="CoreLib.data 2 byte array.vi" Type="VI" URL="../CoreLib.data 2 byte array.vi"/>
		<Item Name="CoreLib.error severity.vi" Type="VI" URL="../CoreLib.error severity.vi"/>
		<Item Name="CoreLib.explain error.vi" Type="VI" URL="../CoreLib.explain error.vi"/>
		<Item Name="CoreLib.get database.vi" Type="VI" URL="../CoreLib.get database.vi"/>
		<Item Name="CoreLib.get error string.vi" Type="VI" URL="../CoreLib.get error string.vi"/>
		<Item Name="CoreLib.get library version.vi" Type="VI" URL="../CoreLib.get library version.vi"/>
		<Item Name="CoreLib.my to LV error.vi" Type="VI" URL="../CoreLib.my to LV error.vi"/>
		<Item Name="CoreLib.parse descriptor.vi" Type="VI" URL="../CoreLib.parse descriptor.vi"/>
		<Item Name="CoreLib.quality 2 status.vi" Type="VI" URL="../CoreLib.quality 2 status.vi"/>
		<Item Name="CoreLib.send command.vi" Type="VI" URL="../CoreLib.send command.vi"/>
		<Item Name="CoreLib.set data type mismatch.vi" Type="VI" URL="../CoreLib.set data type mismatch.vi"/>
		<Item Name="CoreLib.status 2 quality.vi" Type="VI" URL="../CoreLib.status 2 quality.vi"/>
		<Item Name="CoreLib.stringArrayToString.vi" Type="VI" URL="../CoreLib.stringArrayToString.vi"/>
		<Item Name="CoreLib.stringToStringArray.vi" Type="VI" URL="../CoreLib.stringToStringArray.vi"/>
		<Item Name="CoreLib.version info.vi" Type="VI" URL="../CoreLib.version info.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="forFriendsOnly" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">4</Property>
		<Item Name="CoreLib.strip path extended.vi" Type="VI" URL="../CoreLib.strip path extended.vi"/>
		<Item Name="CoreLib.list files.vi" Type="VI" URL="../CoreLib.list files.vi"/>
		<Item Name="CoreLib.byte array 2 double array.vi" Type="VI" URL="../CoreLib.byte array 2 double array.vi"/>
		<Item Name="CoreLib.byte array 2 double.vi" Type="VI" URL="../CoreLib.byte array 2 double.vi"/>
		<Item Name="CoreLib.byte array 2 float array.vi" Type="VI" URL="../CoreLib.byte array 2 float array.vi"/>
		<Item Name="CoreLib.byte array 2 float.vi" Type="VI" URL="../CoreLib.byte array 2 float.vi"/>
		<Item Name="CoreLib.byte array 2 int array.vi" Type="VI" URL="../CoreLib.byte array 2 int array.vi"/>
		<Item Name="CoreLib.byte array 2 int.vi" Type="VI" URL="../CoreLib.byte array 2 int.vi"/>
		<Item Name="CoreLib.byte array 2 short array.vi" Type="VI" URL="../CoreLib.byte array 2 short array.vi"/>
		<Item Name="CoreLib.byte array 2 short.vi" Type="VI" URL="../CoreLib.byte array 2 short.vi"/>
		<Item Name="CoreLib.byte array 2 string.vi" Type="VI" URL="../CoreLib.byte array 2 string.vi"/>
		<Item Name="CoreLib.double 2 byte array.vi" Type="VI" URL="../CoreLib.double 2 byte array.vi"/>
		<Item Name="CoreLib.double array 2 byte array.vi" Type="VI" URL="../CoreLib.double array 2 byte array.vi"/>
		<Item Name="CoreLib.float 2 byte array.vi" Type="VI" URL="../CoreLib.float 2 byte array.vi"/>
		<Item Name="CoreLib.float array 2 byte array.vi" Type="VI" URL="../CoreLib.float array 2 byte array.vi"/>
		<Item Name="CoreLib.int 2 byte array.vi" Type="VI" URL="../CoreLib.int 2 byte array.vi"/>
		<Item Name="CoreLib.int array 2 byte array.vi" Type="VI" URL="../CoreLib.int array 2 byte array.vi"/>
		<Item Name="CoreLib.short 2 byte array.vi" Type="VI" URL="../CoreLib.short 2 byte array.vi"/>
		<Item Name="CoreLib.short array 2 byte array.vi" Type="VI" URL="../CoreLib.short array 2 byte array.vi"/>
		<Item Name="CoreLib.string 2 byte array.vi" Type="VI" URL="../CoreLib.string 2 byte array.vi"/>
	</Item>
</Library>
