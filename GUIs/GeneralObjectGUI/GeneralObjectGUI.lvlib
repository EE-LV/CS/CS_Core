﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">A general GUI to work with objects - good for static parameters or control systems without a sequencer. It can be used interactively or event driven (no configuration). Of course, a certain subsystem can be configured and the settings stored and reloaded.

Think object oriented! 

Example: A control system is divided into sub systems. Each sub system has its own GOG object. A main GOG (MainGOG) object may be used to start up the control system automatically.

The sub system GOGs are configured as "enabled' and "autocreate" objects in the MainGOG. 

By starting the MainGOG the sub system GOGs are created. 

When a sub system GOG is created, it creates all "enabled" and "autocreated" objects of the sub system and executes all "enabled" and "auto execute" methods of the sub system objects. 

If each sub system GOG has its "Execute" method configured in the MainGOG as "enabled", all parameters of the whole control system can be set by just doing an "Execute" of the MainGOG.

Front panels for user action can be created on the fly for each object. Those can either be generic front panels or dedicated (user) front panels. A dedicated user front panel must follow a name convention: The user panel of an object of class "classname" must have the name "classnameGUI"; note the upper case letters "GUI".

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2001  Dietrich Beck, Holger Brand, Mathias Richter
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de or h.brand@gsi.de
Last update: 20-Mar-2013

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+X!!!*Q(C=\:;R&lt;B."%)&lt;`#R&amp;%3"2_!_37=F[!QE6%D3/?9&amp;\"$95LF#*.#C4G%7TZ$&gt;R4T3.AC:&lt;'IL3MEY`0ZX733]"')EA5X'&lt;M]``PTHSXO\?+V&amp;ZHUKG;9Z=/8L`F&lt;T_;]L58&gt;GVP[5:12_D[&lt;;W@_&gt;X7O&lt;V88UWB;JJ&lt;`U_@\\^`?^7&lt;T3&amp;`5[_&lt;!`ZMP?I-P_]XKX6.EO;!0[0'!8^6AV$`QG`7^7LV[O)/1N&gt;@VR?TW?;OV0%:@8][/HZ^]7!W/`Z$_R(7Z`DL04\;[3^@5L5\':IGC3#=M.W,G_C*HOC*HOC*(OC"(OC"(OC"\OC/\OC/\OC/&lt;OC'&lt;OC'&lt;OCW0ST1B3ZU78N)*-741EH3*%(3'21F(QF0QJ0Q*$T]6-+4]#1]#5`#1R=F0!F0QJ0Q*$Q-5]+4]#1]#5`#1[K7*.O'$E`#1XI&amp;0!&amp;0Q"0Q"$S56-!4!!4&amp;AM2"%D!5G-&amp;.Q"0Q"$T=+O!*?!+?A#@AQ6&lt;!%`!%0!&amp;0Q-/1&gt;F;CJ&gt;EW&gt;(B))Y@(Y8&amp;Y("[(B^2S?"Q?B]@B=8AI*Y@(Y8%AH)*/=B$E$()[/$]=(I?(,TE]$I`$Y`!Y0&amp;DN#HE\-VO;&lt;5/(R_!R?!Q?A]@A)95-(I0(Y$&amp;Y$"\3SO!R?!Q?A]@AI:1-(I0(Y$&amp;!D++5FZ(-''BU-A3$B\^WNVC\3N'37.P+K\H&lt;K#I&lt;5'6DK7Q9F9WAMM!K#[?S)#I4L4+"+B/D]M!K$[)#K&amp;*9*;&amp;+2SXZ8""T9EJ=%C.C3!S)0N(&lt;$HXEDMPF5IP&amp;1P0Z8.0J6*?8FRK.2BI/BRI-"OLX__LV?D?HV5P;\KLWZ^+9_WV=R?@*^&gt;PTS@7\]]H(Y:P*V;=0X`&lt;?]@B;D3PCS?NK@*,%&amp;_*^.8ZW6IW@@K`',_DTH$C^(&lt;-\F`[&amp;MV%HZ&lt;_GGTH[!&gt;-@=4!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.30.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="GeneralObjectGUI.check and close front panels.vi" Type="VI" URL="../GeneralObjectGUI.check and close front panels.vi"/>
		<Item Name="GeneralObjectGUI.constructor.vi" Type="VI" URL="../GeneralObjectGUI.constructor.vi"/>
		<Item Name="GeneralObjectGUI.create all objects.vi" Type="VI" URL="../GeneralObjectGUI.create all objects.vi"/>
		<Item Name="GeneralObjectGUI.destructor.vi" Type="VI" URL="../GeneralObjectGUI.destructor.vi"/>
		<Item Name="GeneralObjectGUI.execute all methods.vi" Type="VI" URL="../GeneralObjectGUI.execute all methods.vi"/>
		<Item Name="GeneralObjectGUI.front panel open.vi" Type="VI" URL="../GeneralObjectGUI.front panel open.vi"/>
		<Item Name="GeneralObjectGUI.front panel opened.vi" Type="VI" URL="../GeneralObjectGUI.front panel opened.vi"/>
		<Item Name="GeneralObjectGUI.front panel closed.vi" Type="VI" URL="../GeneralObjectGUI.front panel closed.vi"/>
		<Item Name="GeneralObjectGUI.get data to modify.vi" Type="VI" URL="../GeneralObjectGUI.get data to modify.vi"/>
		<Item Name="GeneralObjectGUI.get library version.vi" Type="VI" URL="../GeneralObjectGUI.get library version.vi"/>
		<Item Name="GeneralObjectGUI.get method parameters.vi" Type="VI" URL="../GeneralObjectGUI.get method parameters.vi"/>
		<Item Name="GeneralObjectGUI.get setting file directory.vi" Type="VI" URL="../GeneralObjectGUI.get setting file directory.vi"/>
		<Item Name="GeneralObjectGUI.load settings.vi" Type="VI" URL="../GeneralObjectGUI.load settings.vi"/>
		<Item Name="GeneralObjectGUI.panel.vi" Type="VI" URL="../GeneralObjectGUI.panel.vi"/>
		<Item Name="GeneralObjectGUI.save settings.vi" Type="VI" URL="../GeneralObjectGUI.save settings.vi"/>
		<Item Name="GeneralObjectGUI.set modified data.vi" Type="VI" URL="../GeneralObjectGUI.set modified data.vi"/>
		<Item Name="GeneralObjectGUI.update object value.vi" Type="VI" URL="../GeneralObjectGUI.update object value.vi"/>
		<Item Name="GeneralObjectGUI.update tag value.vi" Type="VI" URL="../GeneralObjectGUI.update tag value.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="GeneralObjectGUI.get i attribute.vi" Type="VI" URL="../GeneralObjectGUI.get i attribute.vi"/>
		<Item Name="GeneralObjectGUI.set i attribute.vi" Type="VI" URL="../GeneralObjectGUI.set i attribute.vi"/>
		<Item Name="GeneralObjectGUI.ProcCases.vi" Type="VI" URL="../GeneralObjectGUI.ProcCases.vi"/>
		<Item Name="GeneralObjectGUI.ProcPeriodic.vi" Type="VI" URL="../GeneralObjectGUI.ProcPeriodic.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="GeneralObjectGUI.tag.ctl" Type="VI" URL="../GeneralObjectGUI.tag.ctl"/>
		<Item Name="GeneralObjectGUI.i attribute.ctl" Type="VI" URL="../GeneralObjectGUI.i attribute.ctl"/>
		<Item Name="GeneralObjectGUI.method.ctl" Type="VI" URL="../GeneralObjectGUI.method.ctl"/>
		<Item Name="GeneralObjectGUI.object.ctl" Type="VI" URL="../GeneralObjectGUI.object.ctl"/>
		<Item Name="GeneralObjectGUI.panelType.ctl" Type="VI" URL="../GeneralObjectGUI.panelType.ctl"/>
		<Item Name="GeneralObjectGUI.BOI extract column headers.vi" Type="VI" URL="../GeneralObjectGUI.BOI extract column headers.vi"/>
		<Item Name="GeneralObjectGUI.BOI extract items.vi" Type="VI" URL="../GeneralObjectGUI.BOI extract items.vi"/>
		<Item Name="GeneralObjectGUI.BOI extract names and errors.vi" Type="VI" URL="../GeneralObjectGUI.BOI extract names and errors.vi"/>
		<Item Name="GeneralObjectGUI.add object.vi" Type="VI" URL="../GeneralObjectGUI.add object.vi"/>
		<Item Name="GeneralObjectGUI.build methods items.vi" Type="VI" URL="../GeneralObjectGUI.build methods items.vi"/>
		<Item Name="GeneralObjectGUI.build objects items.vi" Type="VI" URL="../GeneralObjectGUI.build objects items.vi"/>
		<Item Name="GeneralObjectGUI.build tag items.vi" Type="VI" URL="../GeneralObjectGUI.build tag items.vi"/>
		<Item Name="GeneralObjectGUI.copy object.vi" Type="VI" URL="../GeneralObjectGUI.copy object.vi"/>
		<Item Name="GeneralObjectGUI.create front panel set.vi" Type="VI" URL="../GeneralObjectGUI.create front panel set.vi"/>
		<Item Name="GeneralObjectGUI.create trend events.vi" Type="VI" URL="../GeneralObjectGUI.create trend events.vi"/>
		<Item Name="GeneralObjectGUI.create generic panel.vi" Type="VI" URL="../GeneralObjectGUI.create generic panel.vi"/>
		<Item Name="GeneralObjectGUI.create object front panels.vi" Type="VI" URL="../GeneralObjectGUI.create object front panels.vi"/>
		<Item Name="GeneralObjectGUI.create user panel.vi" Type="VI" URL="../GeneralObjectGUI.create user panel.vi"/>
		<Item Name="GeneralObjectGUI.delete object.vi" Type="VI" URL="../GeneralObjectGUI.delete object.vi"/>
		<Item Name="GeneralObjectGUI.destroy object front panels.vi" Type="VI" URL="../GeneralObjectGUI.destroy object front panels.vi"/>
		<Item Name="GeneralObjectGUI.destroy trend events.vi" Type="VI" URL="../GeneralObjectGUI.destroy trend events.vi"/>
		<Item Name="GeneralObjectGUI.edit method parameters.vi" Type="VI" URL="../GeneralObjectGUI.edit method parameters.vi"/>
		<Item Name="GeneralObjectGUI.edit object.vi" Type="VI" URL="../GeneralObjectGUI.edit object.vi"/>
		<Item Name="GeneralObjectGUI.edit object view.vi" Type="VI" URL="../GeneralObjectGUI.edit object view.vi"/>
		<Item Name="GeneralObjectGUI.execute method.vi" Type="VI" URL="../GeneralObjectGUI.execute method.vi"/>
		<Item Name="GeneralObjectGUI.export object setting.vi" Type="VI" URL="../GeneralObjectGUI.export object setting.vi"/>
		<Item Name="GeneralObjectGUI.get default timeout.vi" Type="VI" URL="../GeneralObjectGUI.get default timeout.vi"/>
		<Item Name="GeneralObjectGUI.get FP open flag.vi" Type="VI" URL="../GeneralObjectGUI.get FP open flag.vi"/>
		<Item Name="GeneralObjectGUI.get GUI class.vi" Type="VI" URL="../GeneralObjectGUI.get GUI class.vi"/>
		<Item Name="GeneralObjectGUI.get method index.vi" Type="VI" URL="../GeneralObjectGUI.get method index.vi"/>
		<Item Name="GeneralObjectGUI.get object descriptors.vi" Type="VI" URL="../GeneralObjectGUI.get object descriptors.vi"/>
		<Item Name="GeneralObjectGUI.get object index.vi" Type="VI" URL="../GeneralObjectGUI.get object index.vi"/>
		<Item Name="GeneralObjectGUI.get object tags.vi" Type="VI" URL="../GeneralObjectGUI.get object tags.vi"/>
		<Item Name="GeneralObjectGUI.get objects.vi" Type="VI" URL="../GeneralObjectGUI.get objects.vi"/>
		<Item Name="GeneralObjectGUI.get objects to modify.vi" Type="VI" URL="../GeneralObjectGUI.get objects to modify.vi"/>
		<Item Name="GeneralObjectGUI.get setting file path.vi" Type="VI" URL="../GeneralObjectGUI.get setting file path.vi"/>
		<Item Name="GeneralObjectGUI.get tag index.vi" Type="VI" URL="../GeneralObjectGUI.get tag index.vi"/>
		<Item Name="GeneralObjectGUI.i attribute.vi" Type="VI" URL="../GeneralObjectGUI.i attribute.vi"/>
		<Item Name="GeneralObjectGUI.import settings.vi" Type="VI" URL="../GeneralObjectGUI.import settings.vi"/>
		<Item Name="GeneralObjectGUI.launch access viewer.vi" Type="VI" URL="../GeneralObjectGUI.launch access viewer.vi"/>
		<Item Name="GeneralObjectGUI.LoadPanelBounds.vi" Type="VI" URL="../GeneralObjectGUI.LoadPanelBounds.vi"/>
		<Item Name="GeneralObjectGUI.LoadPosSettings.vi" Type="VI" URL="../GeneralObjectGUI.LoadPosSettings.vi"/>
		<Item Name="GeneralObjectGUI.load process.vi" Type="VI" URL="../GeneralObjectGUI.load process.vi"/>
		<Item Name="GeneralObjectGUI.load set.vi" Type="VI" URL="../GeneralObjectGUI.load set.vi"/>
		<Item Name="GeneralObjectGUI.ProcConstructor.vi" Type="VI" URL="../GeneralObjectGUI.ProcConstructor.vi"/>
		<Item Name="GeneralObjectGUI.ProcDestructor.vi" Type="VI" URL="../GeneralObjectGUI.ProcDestructor.vi"/>
		<Item Name="GeneralObjectGUI.ProcEvents.vi" Type="VI" URL="../GeneralObjectGUI.ProcEvents.vi"/>
		<Item Name="GeneralObjectGUI.remove process.vi" Type="VI" URL="../GeneralObjectGUI.remove process.vi"/>
		<Item Name="GeneralObjectGUI.SavePosSettings.vi" Type="VI" URL="../GeneralObjectGUI.SavePosSettings.vi"/>
		<Item Name="GeneralObjectGUI.select name.vi" Type="VI" URL="../GeneralObjectGUI.select name.vi"/>
		<Item Name="GeneralObjectGUI.select object.vi" Type="VI" URL="../GeneralObjectGUI.select object.vi"/>
		<Item Name="GeneralObjectGUI.set modified objects.vi" Type="VI" URL="../GeneralObjectGUI.set modified objects.vi"/>
		<Item Name="GeneralObjectGUI.set objects.vi" Type="VI" URL="../GeneralObjectGUI.set objects.vi"/>
		<Item Name="GeneralObjectGUI.stop access viewer.vi" Type="VI" URL="../GeneralObjectGUI.stop access viewer.vi"/>
		<Item Name="GeneralObjectGUI.update object items.vi" Type="VI" URL="../GeneralObjectGUI.update object items.vi"/>
	</Item>
	<Item Name="GeneralObjectGUI.contents.vi" Type="VI" URL="../GeneralObjectGUI.contents.vi"/>
	<Item Name="GeneralObjectGUI.rtm" Type="Document" URL="../GeneralObjectGUI.rtm"/>
	<Item Name="GeneralObjectGUI_db.ini" Type="Document" URL="../GeneralObjectGUI_db.ini"/>
	<Item Name="GeneralObjectGUI_mapping.ini" Type="Document" URL="../GeneralObjectGUI_mapping.ini"/>
</Library>
