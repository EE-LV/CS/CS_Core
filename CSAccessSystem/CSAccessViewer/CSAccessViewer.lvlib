﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This class provides a viewer on object hierarchy trees. An object hierarchy defines dependencies for the reservation mechanism. This class uses the CSAccessClient class.

author: Dietrich Beck
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de or h.brand@gsi.de
Last update: 20-Mar-2013

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+(!!!*Q(C=\:1^DB."%%9`!Y).3(Q$Z#P5&amp;2S"#(U$6-E+C=SJ1Z^AJ5L2!N,EDBR06J/47#)G]"8-[X9P;XNXM:!!&lt;5#X;X[_KKZ[U^VOK&lt;9,[:FWZZJ_W9K`6^_8;XH9P^T=_O)`]N6_='P_%NP@(8`D0RJ&gt;Y^PNNHZ`7_&gt;E`&amp;H_``Z`[2_'Y7(`O^)?(F^]QV(!E&lt;_["RU'H.9`+H\(0ZS[D`WHTHPS0T\`_&lt;`XYGT18W\3K**CC18GG.7PWS6[IC&gt;[IC&gt;[IA&gt;[I!&gt;[I!&gt;[I$O[ITO[ITO[IRO[I2O[I2O[I&gt;?/,H3B#ZV:39IHB:+E39)E'"1FFY1HY5FY%BZ?F@!E0!F0QJ0Q%++%*_&amp;*?"+?B)&gt;B3HA3HI1HY5FY3&amp;6*MH:U?")?UCPA#8A#HI!HY+'E!JY!)#A7*![3A+(!'4Q%0!&amp;0Q-/D!J[!*_!*?!)?X!JY!J[!*_!*?"B3:S5K4?HI]*"'$I`$Y`!Y0!Y0K?8Q/$Q/D]0D]&amp;"/$I`$YU!Y":XE)-A:Z!1Y,Q[0Q]..$I`$Y`!Y0!Y0LLJ#8G?GU*3/$I`"9`!90!;0Q5-+'4Q'D]&amp;D]"A]J*8"9`!90!;0Q5-J'4Q'D]&amp;DA"B&amp;+3]DG4(1#$)%AY&gt;@X3V76SEKC&gt;8?`JL\D;KW!&gt;5WFNK'5&gt;M);AOMNH"K#[)WU7I4K$9R;B_M^C&amp;KA'K&amp;V2+K"7L,&gt;9/NM1Z&lt;9H.MBEWR#49O1`^QY(;\V7;TU8K^6N&gt;V7C[8GM`HGMVGGE[HGEQG'I`(0U_L6`2^'^W=3QO?LV[PXF^XH^Z]`L[[`.B^?8NVP&lt;IM_O`:B^(C[&lt;@2YDHXFV^(C^*@I&amp;`=(\]`FR\$W;AHWBU=]]T2$UIY3V=!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.30.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CSAccessViewer.constructor.vi" Type="VI" URL="../CSAccessViewer.constructor.vi"/>
		<Item Name="CSAccessViewer.destructor.vi" Type="VI" URL="../CSAccessViewer.destructor.vi"/>
		<Item Name="CSAccessViewer.get data to modify.vi" Type="VI" URL="../CSAccessViewer.get data to modify.vi"/>
		<Item Name="CSAccessViewer.get library version.vi" Type="VI" URL="../CSAccessViewer.get library version.vi"/>
		<Item Name="CSAccessViewer.set modified data.vi" Type="VI" URL="../CSAccessViewer.set modified data.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CSAccessViewer.get i attribute.vi" Type="VI" URL="../CSAccessViewer.get i attribute.vi"/>
		<Item Name="CSAccessViewer.set i attribute.vi" Type="VI" URL="../CSAccessViewer.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="CSAccessViewer.i attribute.ctl" Type="VI" URL="../CSAccessViewer.i attribute.ctl"/>
		<Item Name="CSAccessViewer.i attribute.vi" Type="VI" URL="../CSAccessViewer.i attribute.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="INHERITPRIV.panel.vi" Type="VI" URL="../inheritance/INHERITPRIV.panel.vi"/>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="PRIVATE.panel.vi" Type="VI" URL="../inheritance/PRIVATE.panel.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="CSAccessViewer.contents.vi" Type="VI" URL="../CSAccessViewer.contents.vi"/>
</Library>
