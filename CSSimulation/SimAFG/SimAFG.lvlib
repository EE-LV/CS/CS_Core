﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This class simulates an arbitrary function generator. It may serve as an example for a class for a real device. This class also replaces the SimDS345 class.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: M.Richter@gsi.de, H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 17-JUN-2004

INFO2SF</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,-!!!*Q(C=T:3R&lt;B."%):`IVD1OK2$.H2U]Q)5%7`AU%)T066+O[$Q+UR(&lt;3G]1"I8J*P'5FT[&amp;6Q4-$,@\7VC2VRC#J#9P&gt;HT`&lt;-T_^XO_K2CT[14\9\:YF&amp;LY`X_IGW,RPP6$_,.V?_XN`&lt;JA@RG50_WVFV]M9`P&lt;^XZNRDX]R`B0W:[V0[@?$@U1@T\XDLDR`*XS[PFLP;&gt;]3N;\&lt;PC3S,,WH@&amp;\Y,UH@(&gt;@IK(_&amp;P%"`C/P._XP88'N[]&lt;+XVH@.7%NK0N1`(L*H.5LOZ]O%&lt;F[O34LF@3;+36$OT__&lt;B?U81`@PRY4Y``#@[N3&lt;V#CC=?O/.7XG[8[)G?[)G?[)E?[)%?[)%?[)(O[)\O[)\O[)ZO[):O[):O[):?'LL1B3ZU6C7:0*EI+:I53!;$IK2,?"+?B#@BY6%*4]+4]#1]#1^$F0!E0!F0QJ0QE+;%*_&amp;*?"+?B)&gt;3B32,1Y=HY;']!J[!*_!*?!)?JF4!%Q!%EQ7&amp;AS*A+!A'0Q+?A#@AY;=#HI!HY!FY!B\##HA#HI!HY!FY3#GL%I7G;?DQ5%9/D]0D]$A]$A_FZ@!Y0![0Q_0Q-*U=(I@(A8!G&gt;)K$)#@*'?!]/$Q/$T=Z0![0Q_0Q/$S%SAZZ7:G'JGHI]"A]"I`"9`!90*31Q70Q'$Q'D]&amp;$72E]"I`"9`!90%QFA]@A-8A-%'.3JJ&gt;2T%AU"BG#Q=.64IO689J#9K86PW:\5&amp;50I/L"5DUQKA&gt;"&gt;9.6.UZV1V187H5"62&gt;'^9668U165(6CV9+K!\7B8_/8_"S@Y?@Y'$`&amp;B`CA3@X,!T?&lt;D&gt;&lt;LN3YP,T7@TT7&lt;T82_@K\R?+T4UV-.BU-."I/\L^5,7GO^Q_`3T4O@`(B\M&lt;MZ?T8Z?89RO4H\-LG.42H\GU_W@_:P4HL4^Z^[UY^@?^-0,VPN_?@?^/GCVX[8`I&gt;PIZZI&gt;`#::YV_!@\&lt;91=!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.12.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SimAFG.constructor.vi" Type="VI" URL="../SimAFG.constructor.vi"/>
		<Item Name="SimAFG.destructor.vi" Type="VI" URL="../SimAFG.destructor.vi"/>
		<Item Name="SimAFG.get data to modify.vi" Type="VI" URL="../SimAFG.get data to modify.vi"/>
		<Item Name="SimAFG.get library version.vi" Type="VI" URL="../SimAFG.get library version.vi"/>
		<Item Name="SimAFG.set modified data.vi" Type="VI" URL="../SimAFG.set modified data.vi"/>
		<Item Name="SimAFG.initialize.vi" Type="VI" URL="../SimAFG.initialize.vi"/>
		<Item Name="SimAFG.close.vi" Type="VI" URL="../SimAFG.close.vi"/>
		<Item Name="SimAFG.configure.vi" Type="VI" URL="../SimAFG.configure.vi"/>
		<Item Name="SimAFG.IDQuery.vi" Type="VI" URL="../SimAFG.IDQuery.vi"/>
		<Item Name="SimAFG.reset.vi" Type="VI" URL="../SimAFG.reset.vi"/>
		<Item Name="SimAFG.set amplitude.vi" Type="VI" URL="../SimAFG.set amplitude.vi"/>
		<Item Name="SimAFG.set frequency.vi" Type="VI" URL="../SimAFG.set frequency.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SimAFG.get i attribute.vi" Type="VI" URL="../SimAFG.get i attribute.vi"/>
		<Item Name="SimAFG.set i attribute.vi" Type="VI" URL="../SimAFG.set i attribute.vi"/>
		<Item Name="SimAFG.ProcCases.vi" Type="VI" URL="../SimAFG.ProcCases.vi"/>
		<Item Name="SimAFG.ProcPeriodic.vi" Type="VI" URL="../SimAFG.ProcPeriodic.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="SimAFG.i attribute.ctl" Type="VI" URL="../SimAFG.i attribute.ctl"/>
		<Item Name="SimAFG.i attribute.vi" Type="VI" URL="../SimAFG.i attribute.vi"/>
		<Item Name="SimAFG.ProcEvents.vi" Type="VI" URL="../SimAFG.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="SimAFG.contents.vi" Type="VI" URL="../SimAFG.contents.vi"/>
	<Item Name="SimAFG_db.ini" Type="Document" URL="../SimAFG_db.ini"/>
	<Item Name="SimAFG_SVTemplate.lvlib" Type="Library" URL="../SimAFG_SVTemplate.lvlib"/>
	<Item Name="SimAFG_SVTemplate.csv" Type="Document" URL="../SimAFG_SVTemplate.csv"/>
</Library>
