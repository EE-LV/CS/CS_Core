﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">A library used by classes of the DSC_SCADA package.

author: Dietrich Beck
maintainer: Dennis Neidherr

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: D.Neidherr@gsi.de.
Last update: 21-MAR-2013


</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*4!!!*Q(C=\&gt;4"&lt;&gt;N!%)8B*S%(ZUDY&amp;C#!Q3VB7F!,;G&amp;;U.6(ND!Z'MD"[C"A!U%Q+9%NK!8GZX)N/1A=Z2!$#7$3+V.PF\-@FR3FONV)\T2@W`4&lt;L@;8]^@S`+#]V&amp;^_[C^F_&lt;[W=DF1\:DH/LC=3[\\5V";`W8-,`WV4&amp;HL88L8M/C0LO_N`[X`P_W``P/_PTLIF4&gt;J5[7UJ!8.;6;P&lt;E\S*%`S*%`S*!`S)!`S)!`S)(&gt;S*X&gt;S*X&gt;S*T&gt;S)T&gt;S)T&gt;S)[]\O=B&amp;,H*7*:E]G3AJGB2)"E.2]J&amp;Y%E`C34R]6?**0)EH]31?BCDR**\%EXA3$[=J]33?R*.Y%A_FKC4L4IYH]6"?A3@Q"*\!%XC95I%H!!34"97$)D!5&gt;!9(A3@Q""Y/&amp;8A#4_!*0)'(&lt;A7?Q".Y!E`AY:3[+F%VSU[/BT*S0)\(]4A?RU.J/2\(YXA=D_.B/DE?R_-AH!G&gt;YB$EH/1-=,YY(M@$0TE?R_.Y()`DI;P?);]LMWC7H2S0Y4%]BM@Q'"Z+S0!9(M.D?!Q0:76Y$)`B-4S'B[FE?!S0Y4%ARK2-,[/9=;)RS!A-$X`V;&lt;&amp;[F[*+L/\NJ\E_K'I0I.K$J@&lt;!K$U);D&gt;9\=;JX2#VB6:&lt;1,7&amp;5&lt;NAN1N2![J.L&amp;:1&lt;;"/@%[UE8;E$&lt;1$&lt;5`&lt;U8J;NZT[FQ??4C&gt;.U[2R((5](D5-AQ[(A`&lt;\P8;\H@K_6^&gt;VZ\@6(@O[&lt;:\?3`==$^N..WT@&gt;V]_@@`YL8_]`@LQ?$N__$QN@;`6VP@3P`"OV&amp;&lt;TM^=];`1$S))'TQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.15.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="typedefs" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DSCIntLib.SV cluster.ctl" Type="VI" URL="../DSCIntLib.SV cluster.ctl"/>
		<Item Name="DSCIntLib.SV network.ctl" Type="VI" URL="../DSCIntLib.SV network.ctl"/>
		<Item Name="DSCIntLib.SV scaling.ctl" Type="VI" URL="../DSCIntLib.SV scaling.ctl"/>
		<Item Name="DSCIntLib.SV logging.ctl" Type="VI" URL="../DSCIntLib.SV logging.ctl"/>
		<Item Name="DSCIntLib.SV alarming.ctl" Type="VI" URL="../DSCIntLib.SV alarming.ctl"/>
		<Item Name="DSCIntLib.SV alarming analog.ctl" Type="VI" URL="../DSCIntLib.SV alarming analog.ctl"/>
		<Item Name="DSCIntLib.SV alarming analog single.ctl" Type="VI" URL="../DSCIntLib.SV alarming analog single.ctl"/>
		<Item Name="DSCIntLib.SV alarming u32.ctl" Type="VI" URL="../DSCIntLib.SV alarming u32.ctl"/>
		<Item Name="DSCIntLib.SV alarming boolean.ctl" Type="VI" URL="../DSCIntLib.SV alarming boolean.ctl"/>
		<Item Name="DSCIntLib.SV alarming bad status.ctl" Type="VI" URL="../DSCIntLib.SV alarming bad status.ctl"/>
		<Item Name="DSCIntLib.SV update deadband.ctl" Type="VI" URL="../DSCIntLib.SV update deadband.ctl"/>
		<Item Name="DSCIntLib.SV initial value.ctl" Type="VI" URL="../DSCIntLib.SV initial value.ctl"/>
		<Item Name="DSCIntLib.SV server cluster.ctl" Type="VI" URL="../DSCIntLib.SV server cluster.ctl"/>
		<Item Name="DSCIntLib.SV engine state.ctl" Type="VI" URL="../DSCIntLib.SV engine state.ctl"/>
		<Item Name="DSCIntLib.SV Quality Enum.ctl" Type="VI" URL="../DSCIntLib.SV Quality Enum.ctl"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DSCIntLib.csv 2 cluster.vi" Type="VI" URL="../DSCIntLib.csv 2 cluster.vi"/>
		<Item Name="DSCIntLib.csv 2 server cluster.vi" Type="VI" URL="../DSCIntLib.csv 2 server cluster.vi"/>
		<Item Name="DSCIntLib.get library version.vi" Type="VI" URL="../DSCIntLib.get library version.vi"/>
		<Item Name="DSCIntLib.SV quality to DIM quality.vi" Type="VI" URL="../DSCIntLib.SV quality to DIM quality.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="DSCIntLib.csv 2 cluster main.vi" Type="VI" URL="../DSCIntLib.csv 2 cluster main.vi"/>
		<Item Name="DSCIntLib.csv 2 cluster network.vi" Type="VI" URL="../DSCIntLib.csv 2 cluster network.vi"/>
		<Item Name="DSCIntLib.csv 2 cluster scaling.vi" Type="VI" URL="../DSCIntLib.csv 2 cluster scaling.vi"/>
		<Item Name="DSCIntLib.csv 2 cluster logging.vi" Type="VI" URL="../DSCIntLib.csv 2 cluster logging.vi"/>
		<Item Name="DSCIntLib.csv 2 cluster alarming.vi" Type="VI" URL="../DSCIntLib.csv 2 cluster alarming.vi"/>
		<Item Name="DSCIntLib.csv 2 cluster alarming analog.vi" Type="VI" URL="../DSCIntLib.csv 2 cluster alarming analog.vi"/>
		<Item Name="DSCIntLib.csv 2 cluster alarming analog limits.vi" Type="VI" URL="../DSCIntLib.csv 2 cluster alarming analog limits.vi"/>
		<Item Name="DSCIntLib.csv 2 cluster alarming u32.vi" Type="VI" URL="../DSCIntLib.csv 2 cluster alarming u32.vi"/>
		<Item Name="DSCIntLib.csv 2 cluster alarming boolean.vi" Type="VI" URL="../DSCIntLib.csv 2 cluster alarming boolean.vi"/>
		<Item Name="DSCIntLib.csv 2 cluster alarming bad status.vi" Type="VI" URL="../DSCIntLib.csv 2 cluster alarming bad status.vi"/>
		<Item Name="DSCIntLib.csv 2 cluster deadband.vi" Type="VI" URL="../DSCIntLib.csv 2 cluster deadband.vi"/>
		<Item Name="DSCIntLib.csv 2 cluster initial value.vi" Type="VI" URL="../DSCIntLib.csv 2 cluster initial value.vi"/>
		<Item Name="DSCIntLib.csv 2 server cluster main.vi" Type="VI" URL="../DSCIntLib.csv 2 server cluster main.vi"/>
	</Item>
	<Item Name="DSCIntLib.SV Qualities.xls" Type="Document" URL="../DSCIntLib.SV Qualities.xls"/>
</Library>
