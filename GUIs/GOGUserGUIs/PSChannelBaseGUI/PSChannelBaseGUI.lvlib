﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">A dedicated GUI for the PSChannelBase class. It can be used by all classes that have inherited from the PSChannelBase class.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de

Last update: 21-MAR-2013


INFO2SF</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+W!!!*Q(C=Z:3RDB-R%)&lt;`I#MII%B(18(+^21X$=6*&amp;*=[6[6'IJC#,AVJ5_92G&amp;=)#C_1FH*?);)$[9I]!6,YVON==K&gt;MUI#%B*V:?`_R:\[V(5OF0*=ON$V8&lt;E_7RD_:.&amp;9?J8^\U#X_5G_L\7JZX@MH&gt;5S.^7B_'4FZ.,__VPG0]\@R^P.0]Z]L/FH_)`_SX_]P+5VTT.^PSE.TT,`&gt;.L`'/PU046@]57@]:&gt;&amp;([O)LYGDUU$XO6[@`[+&amp;Y/P^J^^#`[V^&gt;&gt;@GXL&lt;P4@XL_14E+X87]:_@`"(_X3,V#CC57G'.7PG[&lt;[)G?[)G?[)E?[)%?[)%?[)(O[)\O[)\O[)ZO[):O[):O[):?+LL1B3ZU6C6*HC2+AC9"EM'A+(EE0!F0QJ0Q]+K%*_&amp;*?"+?B)=B3HA3HI1HY5FYG+;%*_&amp;*?"+?B)&gt;1B32,29=HY3']!J[!*_!*?!)?5CLA#1##:%(A)!A9#JR"*_!*?!)?OAJY!J[!*_!*?(!LY!FY!J[!*_"B3FG6+$2.29?(-(*Y("[(R_&amp;R?!ANB]@B=8A=(I?(&gt;(*Y("Y(QEHI"!&gt;"TC2HA00C]$A].(*Y("[(R_&amp;R?(#6(@+S-AV.5^(B-8A-(I0(Y$&amp;Y##'$R_!R?!Q?AY?Q-HA-(I0(Y$&amp;Y3#7$R_!R?!Q1)SHJ:11T*BK$$-(AY6&gt;/CZ6&gt;CE*CJ&gt;;`:HN161_A[M&amp;304#K"U&amp;VAV5X4H6$6"&gt;;&gt;1&amp;6&amp;U&lt;VAV5`2"61.&lt;&amp;K1.7"WP"=9SNMA=WR+4&lt;'BNA!+X@K(R[YW7SU8K_V7KWU7#QUH]]VH5YV(I]V(!YV'!RU?*N@5NP3/\S8@FR`OLP@,,\@P`FQ^`0LF`?&lt;[Y^X/^_-M3@N[FP&lt;PPT=G\VYV@9P,`&lt;W\F@&lt;PHX&gt;G^X=N#V;?S`^#X?DHGF\=-WT2L]"`:7IS!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.14.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PSChannelBaseGUI.constructor.vi" Type="VI" URL="../PSChannelBaseGUI.constructor.vi"/>
		<Item Name="PSChannelBaseGUI.destructor.vi" Type="VI" URL="../PSChannelBaseGUI.destructor.vi"/>
		<Item Name="PSChannelBaseGUI.EmergencyOff.vi" Type="VI" URL="../PSChannelBaseGUI.EmergencyOff.vi"/>
		<Item Name="PSChannelBaseGUI.get library version.vi" Type="VI" URL="../PSChannelBaseGUI.get library version.vi"/>
		<Item Name="PSChannelBaseGUI.panel.vi" Type="VI" URL="../PSChannelBaseGUI.panel.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PSChannelBaseGUI.get i attribute.vi" Type="VI" URL="../PSChannelBaseGUI.get i attribute.vi"/>
		<Item Name="PSChannelBaseGUI.set i attribute.vi" Type="VI" URL="../PSChannelBaseGUI.set i attribute.vi"/>
		<Item Name="PSChannelBaseGUI.update listbox.vi" Type="VI" URL="../PSChannelBaseGUI.update listbox.vi"/>
		<Item Name="PSChannelBaseGUI.update simple listbox.vi" Type="VI" URL="../PSChannelBaseGUI.update simple listbox.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="PSChannelBaseGUI.i attribute.ctl" Type="VI" URL="../PSChannelBaseGUI.i attribute.ctl"/>
		<Item Name="PSChannelBaseGUI.i attribute.vi" Type="VI" URL="../PSChannelBaseGUI.i attribute.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.panel.vi" Type="VI" URL="../inheritance/PRIVATE.panel.vi"/>
	</Item>
	<Item Name="PSChannelBaseGUI.contents.vi" Type="VI" URL="../PSChannelBaseGUI.contents.vi"/>
</Library>
