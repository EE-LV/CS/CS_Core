﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the base class for an digital IO device. It provides basic events, some example methods and publishes a lot of services "OBJECTNAME_SERVICENAME". This class can be used as parent class for classes that implemented a specific digital IO device.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de

Last update: 21-MAR-2013

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*Y!!!*Q(C=\:1RDBJ"%%8`7'P:)&gt;T!YAJVB#5H)L&lt;EI)+.0)F*#&lt;F#87&amp;]!X/$66U"/&gt;O!!$H=$,`J;&gt;9)N,#SVL)$&gt;V0-T+_OKD@&gt;03W6^F[[U@Z;O\X9?H`&lt;9O83FPP$1XPQV`\LLDSVNS@_1XQ\"*`&amp;HU3XT^7PV[@YS`T8GC[W``\8^*^0_9F`."J&gt;^O``KB][#%@0RV^ZP]?B`&lt;&lt;`P,VE?S_P@Q2`NEF.)=53#]QR+W_X4`2%4`2%4`2%$`2!$`2!$`2!&gt;X2(&gt;X2(&gt;X2(.X2$.X2$.X2$,RV&gt;[%)8/L/3&amp;%]+*5G4"-FA5*4]*4Q*4]+4]0#IB#@B38A3HI3()5JY%J[%*_&amp;*?!B4QJ0Q*$Q*4]*$KE+3J;0$E`#18A&amp;0Q"0Q"$Q"$S56]!1!1&lt;%A=:!%$!8/Y#&lt;A#8A#(GY6]!1]!5`!%`$A6M!4]!1]!5`!1UC:F3AU@5?(BT2S?"Q?B]@B=8B),9@(Y8&amp;Y("[(BX*S?"Q?"])J[#1(15[1-]"Z=(A=(CZS?"Q?B]@B=8BQF28S-D-^4&gt;`2Y4&amp;Y$"[$R_!R?%ABA]@A-8A-(I/(N$*Y$"[$R_!R?#AFA]@A-8A-%+-IZ75E-Q+.19:A]0!LO]8++E5BM&gt;,LJTFM6.5.K,KR6$?-[E:187$6B6.&gt;%.7*6JV!V9F2@7(6&amp;V%&amp;6#WMGF"VI(&lt;]&lt;\!VVG%L&lt;)(.M3EWQ=:^[#M0X/VWWGQW7K`8[LJ/K^6+C]6#]`F=U_F5E]F%Y`(Y[&lt;4[1"^;=XQO0&gt;R`G7W`&gt;&gt;_XX;@:NPP[]?(_&lt;H&lt;Q,2H\9HPXIVH?@'[7&lt;U_MK&lt;[DM=/Z^#_=D8KD`&gt;%RTRT^"#/7JI-!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.14.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DigiIOBase.constructor.vi" Type="VI" URL="../DigiIOBase.constructor.vi"/>
		<Item Name="DigiIOBase.destructor.vi" Type="VI" URL="../DigiIOBase.destructor.vi"/>
		<Item Name="DigiIOBase.get data to modify.vi" Type="VI" URL="../DigiIOBase.get data to modify.vi"/>
		<Item Name="DigiIOBase.set modified data.vi" Type="VI" URL="../DigiIOBase.set modified data.vi"/>
		<Item Name="DigiIOBase.get bit names.vi" Type="VI" URL="../DigiIOBase.get bit names.vi"/>
		<Item Name="DigiIOBase.get bit.vi" Type="VI" URL="../DigiIOBase.get bit.vi"/>
		<Item Name="DigiIOBase.get library version.vi" Type="VI" URL="../DigiIOBase.get library version.vi"/>
		<Item Name="DigiIOBase.get Nof bits.vi" Type="VI" URL="../DigiIOBase.get Nof bits.vi"/>
		<Item Name="DigiIOBase.get Nof words.vi" Type="VI" URL="../DigiIOBase.get Nof words.vi"/>
		<Item Name="DigiIOBase.get word.vi" Type="VI" URL="../DigiIOBase.get word.vi"/>
		<Item Name="DigiIOBase.set logical bit name.vi" Type="VI" URL="../DigiIOBase.set logical bit name.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DigiIOBase.myEventNames.ctl" Type="VI" URL="../DigiIOBase.myEventNames.ctl"/>
		<Item Name="DigiIOBase.serviceType.ctl" Type="VI" URL="../DigiIOBase.serviceType.ctl"/>
		<Item Name="DigiIOBase.directionType.ctl" Type="VI" URL="../DigiIOBase.directionType.ctl"/>
		<Item Name="DigiIOBase.get i attribute.vi" Type="VI" URL="../DigiIOBase.get i attribute.vi"/>
		<Item Name="DigiIOBase.set i attribute.vi" Type="VI" URL="../DigiIOBase.set i attribute.vi"/>
		<Item Name="DigiIOBase.ProcCases.vi" Type="VI" URL="../DigiIOBase.ProcCases.vi"/>
		<Item Name="DigiIOBase.ProcPeriodic.vi" Type="VI" URL="../DigiIOBase.ProcPeriodic.vi"/>
		<Item Name="DigiIOBase.add services.vi" Type="VI" URL="../DigiIOBase.add services.vi"/>
		<Item Name="DigiIOBase.bit index in range.vi" Type="VI" URL="../DigiIOBase.bit index in range.vi"/>
		<Item Name="DigiIOBase.word index in range.vi" Type="VI" URL="../DigiIOBase.word index in range.vi"/>
		<Item Name="DigiIOBase.delete events.vi" Type="VI" URL="../DigiIOBase.delete events.vi"/>
		<Item Name="DigiIOBase.update_mass services and attribute data.vi" Type="VI" URL="../DigiIOBase.update_mass services and attribute data.vi"/>
		<Item Name="DigiIOBase.update services and attribute data.vi" Type="VI" URL="../DigiIOBase.update services and attribute data.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="DigiIOBase.i attribute.ctl" Type="VI" URL="../DigiIOBase.i attribute.ctl"/>
		<Item Name="DigiIOBase.parameters.ctl" Type="VI" URL="../DigiIOBase.parameters.ctl"/>
		<Item Name="DigiIOBase.i attribute.vi" Type="VI" URL="../DigiIOBase.i attribute.vi"/>
		<Item Name="DigiIOBase.ProcEvents.vi" Type="VI" URL="../DigiIOBase.ProcEvents.vi"/>
		<Item Name="DigiIOBase.remove services.vi" Type="VI" URL="../DigiIOBase.remove services.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="INHERITPRIV.ProcCases.vi" Type="VI" URL="../inheritance/INHERITPRIV.ProcCases.vi"/>
	</Item>
	<Item Name="DigiIOBase.contents.vi" Type="VI" URL="../DigiIOBase.contents.vi"/>
</Library>
