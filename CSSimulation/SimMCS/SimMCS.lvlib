﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">Simulates a Multi Channel Scaler  The main purpose of this class is to generate simulated data. The SimMCS is adapted to the needs of trap experiments using time-of-flight detection. 
It can be used in two different modes that can be selected using the method SetTofMode.
- TRUE: generated data yields a Gaussian, when displaying time-of-flight versus frequency.
- FALSE: generated data yields a Gaussian, when displaying count rate versus frequency.

The frequency at which the data is generated can be set by the method SetActFreq.
The center frequency can be set by the method SetCenterFreq.
The width of the Gaussian can be set using the method SetResWidth.
The average number of counts can be set by the method SetCounts.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: M.Richter@gsi.de, H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 16-JUN-2004

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*P!!!*Q(C=\:9RDB."%%7`%1%BFC!'?9Z1&amp;Q$**U"W2FQH1(,KF:$76[AL//!#DD;P+VDC""9BW@#GJ]:CJ75.]C)2U,UV/`/LK_J.&gt;U`,5GMPJ/@K,T5^WH\00[1:O]:L0Q77@^4&lt;QTB7`70RP?\(4_J0@P6`50`;^`PP(^K8*`&amp;XV`G\LPOVPWP_\AJ`&gt;YX`X"\W0^3O^V`_P,=8"`XF*MXK'_U4#]QR;W`8*XKC*XKC*XKC"XKA"XKA"XKA/\KD/\KD/\KD'\KB'\KB'\J.ZQ7[U)5O;_&gt;%5DQJF#2.%C3$16&amp;S38A3HI1HY?&amp;2#5`#E`!E0!E01Z4Q*$Q*4]+4]"#GB#@B38A3HI3(6)UE7U?(*_%BP1+?A#@A#8A#(EIKY!E!AG*"YC!*'!K=Q5X!%`!%0.QKY!FY!J[!*_$"L9!HY!FY!J[!BZ!W+^&amp;IBIY/$WHE]$A]$I`$Y`#17A[0Q_0Q/$Q/$_8E]$A]$I24U%E/AJQA:Y$TY0!Y00S4Q_0Q/$Q/D]/$K[W1NZE:;);/$I`"9`!90!;0Q5-+'4Q'D]&amp;D]"A]J*8"9`!90!;0Q5-J'4Q'D]&amp;DA"B&amp;+3]DG2&amp;I$$)%AY?`NFOML6)U%GO^0MVRI[IWI'JDK4;-;C/I&amp;FCV=+I&amp;55WU;A*6%[.[9&gt;7,K!"6B65*61.VYHL%$NA?WW%&lt;&lt;)UNM15W(U+@?/$J&gt;.,R?.4B=."_P^&gt;ON^.GM^&amp;[P&gt;:SO&gt;2CM&gt;"]0D_@6G`J9ZN.Z^+7_]&amp;O6R`O0H`]&gt;(/\7NX&gt;L&amp;&lt;PB_@*&gt;^&amp;?4P;O\/NM__L&lt;;+`,XHS`&amp;T/?3``#W;BH^=PI0%=`!.&gt;_3$I!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.12.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SimMCS.constructor.vi" Type="VI" URL="../SimMCS.constructor.vi"/>
		<Item Name="SimMCS.destructor.vi" Type="VI" URL="../SimMCS.destructor.vi"/>
		<Item Name="SimMCS.get bin data.vi" Type="VI" URL="../SimMCS.get bin data.vi"/>
		<Item Name="SimMCS.get data to modify.vi" Type="VI" URL="../SimMCS.get data to modify.vi"/>
		<Item Name="SimMCS.get library version.vi" Type="VI" URL="../SimMCS.get library version.vi"/>
		<Item Name="SimMCS.get scan status.vi" Type="VI" URL="../SimMCS.get scan status.vi"/>
		<Item Name="SimMCS.read data.vi" Type="VI" URL="../SimMCS.read data.vi"/>
		<Item Name="SimMCS.set act freq.vi" Type="VI" URL="../SimMCS.set act freq.vi"/>
		<Item Name="SimMCS.set bin width.vi" Type="VI" URL="../SimMCS.set bin width.vi"/>
		<Item Name="SimMCS.set bins per record.vi" Type="VI" URL="../SimMCS.set bins per record.vi"/>
		<Item Name="SimMCS.set center freq.vi" Type="VI" URL="../SimMCS.set center freq.vi"/>
		<Item Name="SimMCS.set counts.vi" Type="VI" URL="../SimMCS.set counts.vi"/>
		<Item Name="SimMCS.set modified data.vi" Type="VI" URL="../SimMCS.set modified data.vi"/>
		<Item Name="SimMCS.set records per scan.vi" Type="VI" URL="../SimMCS.set records per scan.vi"/>
		<Item Name="SimMCS.set res width.vi" Type="VI" URL="../SimMCS.set res width.vi"/>
		<Item Name="SimMCS.set tof mode.vi" Type="VI" URL="../SimMCS.set tof mode.vi"/>
		<Item Name="SimMCS.start scan.vi" Type="VI" URL="../SimMCS.start scan.vi"/>
		<Item Name="SimMCS.stop scan.vi" Type="VI" URL="../SimMCS.stop scan.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SimMCS.get i attribute.vi" Type="VI" URL="../SimMCS.get i attribute.vi"/>
		<Item Name="SimMCS.set i attribute.vi" Type="VI" URL="../SimMCS.set i attribute.vi"/>
		<Item Name="SimMCS.ProcCases.vi" Type="VI" URL="../SimMCS.ProcCases.vi"/>
		<Item Name="SimMCS.ProcPeriodic.vi" Type="VI" URL="../SimMCS.ProcPeriodic.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="SimMCS.Gauss.vi" Type="VI" URL="../SimMCS.Gauss.vi"/>
		<Item Name="SimMCS.GaussWithWhiteNoiseHistrogramm.vi" Type="VI" URL="../SimMCS.GaussWithWhiteNoiseHistrogramm.vi"/>
		<Item Name="SimMCS.i attribute.ctl" Type="VI" URL="../SimMCS.i attribute.ctl"/>
		<Item Name="SimMCS.i attribute.vi" Type="VI" URL="../SimMCS.i attribute.vi"/>
		<Item Name="SimMCS.ProcConstructor.vi" Type="VI" URL="../SimMCS.ProcConstructor.vi"/>
		<Item Name="SimMCS.ProcDestructor.vi" Type="VI" URL="../SimMCS.ProcDestructor.vi"/>
		<Item Name="SimMCS.ProcEvents.vi" Type="VI" URL="../SimMCS.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
	</Item>
	<Item Name="SimMCS.contents.vi" Type="VI" URL="../SimMCS.contents.vi"/>
	<Item Name="SimMCS_db.ini" Type="Document" URL="../SimMCS_db.ini"/>
	<Item Name="SimMCS_mapping.ini" Type="Document" URL="../SimMCS_mapping.ini"/>
</Library>
