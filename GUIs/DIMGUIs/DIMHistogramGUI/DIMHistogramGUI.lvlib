﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">It will display DIM numeric array data services or a selection of simple DIM data services as Histogram. 

INFO2SF

author: Holger Brand
maintainer: H.Brand@gsi.de
history:
2007-04-20 H.Brand@gsi.de, Upgrade to CS 3.1
2010-11-22 H.Brand@gsi.de, Upgrade to CS 3.20

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: H.Brand@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation; either version 2 of 
the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 27-FEB-2007
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+[!!!*Q(C=X:3`&lt;B."%)&gt;`BSBI`1&lt;)^QB419F=5:(#+8C![6WZ&gt;?G7&gt;#.&gt;F&gt;)N%EUEHG"[+L_#B92%?XS\8E&gt;*&lt;-=5)#&amp;G-_?^X`T\MH=[K&gt;IL[;8'3^9^;S6?E];O:&amp;9@[\Y*&gt;6&gt;PRP&amp;QVZW/&gt;Y?[-`7(OOZJ`(D_`;4@Y3^W`=3/YD_R"Z@4^48UK-/D_I@W.0\QS+_P&amp;YPD_'*=^(WZDC@L&amp;W/0F4$V@8^=4`(9V`YH[Y=3[UM3^=.Q&amp;/_(7FMS4N&lt;XYT#5F/&amp;-`WIF:8AG0J\FOX2_2`9Y@M\_``A&amp;YTOQOJDUFUXK6$Z%?/+"/WYKI4(2%TX2%TX2%TX1!TX1!TX1!^X2(&gt;X2(&gt;X2(&gt;X1$&gt;X1$&gt;X1$&lt;UO&gt;+%,8?C=3D)]':1U42IES;!IO31]#5`#E`"QKY1HY5FY%J[%BR1F0!F0QJ0Q*$S5+?&amp;*?"+?B#@BI65FS&lt;L1Y5FY;+_!*_!*?!+?A)?2#HA#A'"9U$BI!I;#9,!*?!+?A)?N!J[!*_!*?!)?QAJY!J[!*_!*?#CJJR+6JCRU?'ADB]@B=8A=(I?(VH*Y("[(R_&amp;R?"AHB]@B=3#=A5ZT%/15/1H/D=0D]0!DB]@B=8A=(I?(5(V#8E_GU*3&amp;$I`"9`!90!;0Q5-,'4Q'D]&amp;D]"A]N*8"9`!90!;0Q=-I'4Q'D]&amp;DA"B$'3_DG6&amp;I*"G#Q=.@@6OM0K7I*&amp;;8V&amp;Z?N:&gt;3\764?YH58A[VB[\W-.5?ENLBKRWKWG'J(9,;0[='L1;D.E3NO#2KRX7,X_%&lt;@)UP]4E_Q[@YJ*4_Y=4&gt;&lt;K@N&gt;KO\OTNN.BONVWMNFUP.ZX0.:D..JV..*B-&gt;\$6L&lt;^XBO\2C0^S_P&lt;LZ]O\K:P0R[O&lt;W`&gt;6Q__:4U9^]`.[NPHXI6F]`\`@DD`&lt;\Q%`6H@$^&gt;_F@_$&lt;KB@;`^W@U#QGP!&amp;=!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.20.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DIMHistogramGUI.constructor.vi" Type="VI" URL="../public/DIMHistogramGUI.constructor.vi"/>
		<Item Name="DIMHistogramGUI.destructor.vi" Type="VI" URL="../public/DIMHistogramGUI.destructor.vi"/>
		<Item Name="DIMHistogramGUI.get data to modify.vi" Type="VI" URL="../public/DIMHistogramGUI.get data to modify.vi"/>
		<Item Name="DIMHistogramGUI.set modified data.vi" Type="VI" URL="../public/DIMHistogramGUI.set modified data.vi"/>
		<Item Name="DIMHistogramGUI.panel.vi" Type="VI" URL="../private/DIMHistogramGUI.panel.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DIMHistogramGUI.get i attribute.vi" Type="VI" URL="../protected/DIMHistogramGUI.get i attribute.vi"/>
		<Item Name="DIMHistogramGUI.set i attribute.vi" Type="VI" URL="../protected/DIMHistogramGUI.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="DIMHistogramGUI.i attribute.ctl" Type="VI" URL="../private/DIMHistogramGUI.i attribute.ctl"/>
		<Item Name="DIMHistogramGUI.i attribute.vi" Type="VI" URL="../private/DIMHistogramGUI.i attribute.vi"/>
	</Item>
	<Item Name="forFriendsOnly" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DIMHistogramGUI.about me.vi" Type="VI" URL="../private/DIMHistogramGUI.about me.vi"/>
		<Item Name="DIMHistogramGUI.CSSysteme.vi" Type="VI" URL="../private/DIMHistogramGUI.CSSysteme.vi"/>
		<Item Name="DIMHistogramGUI.Service.vi" Type="VI" URL="../private/DIMHistogramGUI.Service.vi"/>
		<Item Name="DIMHistogramGUI.object.vi" Type="VI" URL="../private/DIMHistogramGUI.object.vi"/>
		<Item Name="DIMHistogramGUI.getSimpleNumericServices.vi" Type="VI" URL="../private/DIMHistogramGUI.getSimpleNumericServices.vi"/>
	</Item>
	<Item Name="DIMHistogramGUI.rtm" Type="Document" URL="../DIMHistogramGUI.rtm"/>
	<Item Name="DIMHistogramGUI.contents.vi" Type="VI" URL="../DIMHistogramGUI.contents.vi"/>
</Library>
