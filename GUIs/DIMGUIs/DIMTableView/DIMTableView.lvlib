﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">The aim of this class is to provide a simple viewer of DIM services published in a DIM domain. Just create an object of this class by sending a "CreateObject" event to the SuperProc object.

author: Dietrich Beck, GSI
maintainer: Dietrich Beck, GSI; d.beck@gsi.de

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.beck@gsi.de or h.brand@gsi.de
Last update: 19-Nov-2010

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*F!!!*Q(C=\:1^&lt;NN!%%9`"3Z3GG7+Q)(;F.0E!,K#LD"85+N3&amp;X!RH&gt;-3K&gt;TK#H/"&amp;!2]!C%X9"Z8)VER&lt;%F&amp;!A2)&gt;D53_=X@Y_[+5BPPJ2O.F];8MQ0`&amp;$+WW?Y0QP'[:F5[#2YL`XHO%Z`T8`D@S$`N@]Q@RS0,?@Z,1W@(@``V`N?7^"@`Z`VYU``C^Z`,P\"_LYVLDP@[]J`ATQZJVECRR!*TT.L4D9G?[)G?[)G?[)%?[)%?[)%?[)\O[)\O[)\O[):O[):O[):OUWZ-%VXI1J?V(5K;*YW3IEG"*"A5*6]*4]+4]#1]X#LB38A3HI1HY3&amp;%#5`#E`!E0!E0;5JY%J[%*_&amp;*?#D63,*.&gt;(A3(MILY!FY!J[!*_#BJ1+?!#"I&amp;B1/CI#BQ"F="$Q"4]$$J1+?A#@A#8A#(NQ+?!+?A#@A#8B);;M3D7;;[0"12A[0Q_0Q/$Q/$[8F]$A]$I`$Y`$14A[0Q_.!/!W&gt;YC$)38)#H"O(R_(B2Q[0Q_0Q/$Q/$[[W1^Z7:K+:*DI]"I`"9`!90!90*74Q'$Q'D]&amp;D]&amp;"7"I`"9`!90!90L74Q'$Q'DQ&amp;C.+7^D'*'IB&amp;E#!90HX:;L/V3."*LM`[;_Y/K/I#KA[5[-+K$I.JAV=;J.E3VU+I&amp;6#W-[I&amp;6$[)#6$67&amp;61&amp;;M@XA'WR(NNA+WS*,&lt;!ZVEWJPTFQN^NJ'!:NNVPV@;`.:K06;K8F=KH&amp;9K(Z@+[O[YZPKU`-`:A&gt;XENLLB_`@P`90^VX`&lt;@YU$`=&gt;Y]0-5T[Q8_6X@W9L7^P,^M56\8`DH?DXGE]?=WT2D]"JRKMI1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.21.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DIMTableView.constructor.vi" Type="VI" URL="../DIMTableView.constructor.vi"/>
		<Item Name="DIMTableView.destructor.vi" Type="VI" URL="../DIMTableView.destructor.vi"/>
		<Item Name="DIMTableView.get data to modify.vi" Type="VI" URL="../DIMTableView.get data to modify.vi"/>
		<Item Name="DIMTableView.get library version.vi" Type="VI" URL="../DIMTableView.get library version.vi"/>
		<Item Name="DIMTableView.panel.vi" Type="VI" URL="../DIMTableView.panel.vi"/>
		<Item Name="DIMTableView.set modified data.vi" Type="VI" URL="../DIMTableView.set modified data.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DIMTableView.add services.vi" Type="VI" URL="../DIMTableView.add services.vi"/>
		<Item Name="DIMTableView.edit new-old interval.vi" Type="VI" URL="../DIMTableView.edit new-old interval.vi"/>
		<Item Name="DIMTableView.edit time alarm filter.vi" Type="VI" URL="../DIMTableView.edit time alarm filter.vi"/>
		<Item Name="DIMTableView.load settings.vi" Type="VI" URL="../DIMTableView.load settings.vi"/>
		<Item Name="DIMTableView.save settings.vi" Type="VI" URL="../DIMTableView.save settings.vi"/>
		<Item Name="DIMTableView.remove services.vi" Type="VI" URL="../DIMTableView.remove services.vi"/>
		<Item Name="DIMTableView.get i attribute.vi" Type="VI" URL="../DIMTableView.get i attribute.vi"/>
		<Item Name="DIMTableView.set i attribute.vi" Type="VI" URL="../DIMTableView.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="DIMTableView.i attribute.ctl" Type="VI" URL="../DIMTableView.i attribute.ctl"/>
		<Item Name="DIMTableView.filter type.ctl" Type="VI" URL="../DIMTableView.filter type.ctl"/>
		<Item Name="DIMTableView.i attribute.vi" Type="VI" URL="../DIMTableView.i attribute.vi"/>
		<Item Name="DIMTableView.unpack service list string.vi" Type="VI" URL="../DIMTableView.unpack service list string.vi"/>
		<Item Name="DIMTableView.filter service.vi" Type="VI" URL="../DIMTableView.filter service.vi"/>
		<Item Name="DIMTableView.subscribe services.vi" Type="VI" URL="../DIMTableView.subscribe services.vi"/>
		<Item Name="DIMTableView.service 2 table line.vi" Type="VI" URL="../DIMTableView.service 2 table line.vi"/>
		<Item Name="DIMTableView.update service table.vi" Type="VI" URL="../DIMTableView.update service table.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.panel.vi" Type="VI" URL="../inheritance/PRIVATE.panel.vi"/>
	</Item>
	<Item Name="DIMTableView.contents.vi" Type="VI" URL="../DIMTableView.contents.vi"/>
	<Item Name="DIMTableView.rtm" Type="Document" URL="../DIMTableView.rtm"/>
</Library>
