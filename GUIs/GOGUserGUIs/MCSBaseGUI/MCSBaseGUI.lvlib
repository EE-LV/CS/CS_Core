﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">A dedicated GUI for the MCSBase class. It can be used by all classes that have inherited from the MCSBase class.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de

Last update: 21-MAR-2013


INFO2SF</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+*!!!*Q(C=Z:3RKF."%)&lt;`+R97.A'NF12O,=QDX03J5AM75^C&gt;RL3XT#P-+_3#,Z"8G&amp;=)AI8&amp;P2#MR#Z_W4-*#=2%P!K#MZG4\$_\-^`:X;T5\*HU6*N,&gt;H07NP'/&gt;N.VWU`TAWY@\^K)]FVLX@X]&amp;ONWXBX0LXA`;4_\_VH^.O*A`HH_3[;T^D`&amp;"Y0"`HEC0NB;05`(.ZN"08]P@D\`*4Z^V%A;99_)DRY4X^PJ_#F\@0TS]&lt;[^`#@YOS:&gt;.6)]]=!&gt;N`:WGU20^%20^%20^%!0^%!0^%!0&gt;%&gt;X&gt;%&gt;X&gt;%&gt;X&gt;%-X&gt;%-X&gt;%-X^.&lt;1B3ZUI&lt;-K3@'E5*)U3:!-"E8*)_&amp;*?"+?B)?O%J[%*_&amp;*?")?BCDB38A3HI1HY7';%J[%*_&amp;*?")?5D73&lt;!U&gt;HI3(^!JY!J[!*_!*?#CJA#=!#)I&amp;C9-E9#A)"D]#HI!HY/'H!J[!*_!*?!)?QAJY!J[!*_!*?*D36C5;T&lt;;BQU-;/4Q/D]0D]$A]J*&lt;$Y`!Y0![0QU-Z/4Q/DQ0B&amp;(33AS"HED0![4A]$A^@=HA=(I@(Y8&amp;Y#,5&gt;]L9S7ZJN1Y@(Y$&amp;Y$"[$R_!BB1Q?A]@A-8A-(N,+Y$&amp;Y$"[$R_#BF!Q?A]@A-5#-IJ38E=S9;!QS")/(4TMNVH9J'IGV6H`.`K#K$K$K9+E/D/IAK$:9N8'K$6%NN'I"61OD?G(6C[A!6966#65$N?;ZQJ@Y!J`D-XS+D`%BXG\.0TRQP6ZLN6JJO6RKM6BI0J^L.JNJ/JVK0"ZL/"Q?X&gt;?P;&lt;V&gt;(&gt;Z,$^=@*J`8CU`X8^Z.\L`&gt;P8WY@D`:R7Y:_UP_`%XPO`[,L\W`,(`V`7B]@S`^#X?DHGBT=-WT2D]!B&amp;O)SA!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.14.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="MCSBaseGUI.constructor.vi" Type="VI" URL="../MCSBaseGUI.constructor.vi"/>
		<Item Name="MCSBaseGUI.destructor.vi" Type="VI" URL="../MCSBaseGUI.destructor.vi"/>
		<Item Name="MCSBaseGUI.get library version.vi" Type="VI" URL="../MCSBaseGUI.get library version.vi"/>
		<Item Name="MCSBaseGUI.panel.vi" Type="VI" URL="../MCSBaseGUI.panel.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="MCSBaseGUI.get i attribute.vi" Type="VI" URL="../MCSBaseGUI.get i attribute.vi"/>
		<Item Name="MCSBaseGUI.set i attribute.vi" Type="VI" URL="../MCSBaseGUI.set i attribute.vi"/>
		<Item Name="MCSBaseGUI.acquire data.vi" Type="VI" URL="../MCSBaseGUI.acquire data.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="MCSBaseGUI.i attribute.ctl" Type="VI" URL="../MCSBaseGUI.i attribute.ctl"/>
		<Item Name="MCSBaseGUI.i attribute.vi" Type="VI" URL="../MCSBaseGUI.i attribute.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.panel.vi" Type="VI" URL="../inheritance/PRIVATE.panel.vi"/>
	</Item>
	<Item Name="MCSBaseGUI.contents.vi" Type="VI" URL="../MCSBaseGUI.contents.vi"/>
</Library>
