﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">An object of the SuperProc class loads and unloads other instances of child classes of the BaseProcess class. Such instances are name "processes". It maintains a process table, which is an array of structures. Such a structure contains the name of a process, the reference of the process, data needed for the watchdog funtionality and a connection table of a process. The connection table contains information, about other processes that use this process. As long as the connection table is not empty, the process can not be unloaded.

The SuperProc class also provides other useful function for all kind of objects like opening front panels, killing objects, getting the names of all objects etc.

Definitions: 
An object is an  instances of a class.
A process is an object of the class BaseProcess or one of it's children.
"Loading" a process means to create/instantiate a process.
"Unloading" a porcess means to destroy a process in a friendly way.
"Killing" an object means to destroy the object in an unfriendly way.

All methods that require the name of a process or an object as an  input do now support the "N/A" feature. They do not execute in case the name  equals "N/A".

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

history: 08-JUL-2005

License Agreement for this software:

Copyright (C) 2001  Dietrich Beck, Holger Brand, Mathias Richter
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de or h.brand@gsi.de
Last update: 20-mar-2013

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*4!!!*Q(C=\:1`MB*"%)&gt;`7!:7%8%$#T04TD4F#FSBLU!+EH#&amp;4AXX:9:SB&lt;Y#6_!+_\[&gt;\8XVZ+&amp;9J;C"-`1S__O:\G`HH^4+'_GV_FN&amp;0SQ`[2`CN.J8O\`Q.ZOK[HFF`"4DQK^P@(I:P_7MW(V`:@SP@&gt;^``V4G]\P[ZW/Z\J^@?L`D`VPD8_,`A@7Z@&lt;SX.TP&gt;O5CT[9AG&amp;JBD.B\H2%`U2%`U2%`U1!`U1!`U1!^U2X&gt;U2X&gt;U2X&gt;U1T&gt;U1T&gt;U1\@JYE!8ON"F\8*)EC?*EK"*A+1T+%I?#5`#E`!E0,QKY5FY%J[%*_'BCR+?B#@B38A3(I9JY5FY%J[%*_%B6#0*6N(B38A)LY!HY!FY!J[!BZ1+?!+!)&amp;E1/!A#BA*HU!BY!J[!B[9#HI!HY!FY!B\=#HA#HI!HY!FY'.*G*2L.5.(B)9Q=(I@(Y8&amp;Y("Z#S_&amp;R?"Q?B]@B):U=(I@(A8!3/M&amp;"E$0)[?#]/$Q/$X^S?"Q?B]@B=8BQN28S.D-$T6$2Y4&amp;Y$"[$R_!R?!ABA]@A-8A-(I/(M$*Y$"[$R_!R?%AFA]@A-8A-%#-J[75%-Q9;H1T"Y/(8&gt;IOV69J'9KX7U2QXKGI$KD;7;M/I.I*KA65,JVI1V53L*F!V-;I06HW)#F#67"61V6&amp;HHC@MC(89!&gt;NA;WS&amp;,&lt;(&amp;-01X&gt;TS@TTK&gt;4DI?D_K[4I@$1:P.2OPV7KP63MPF5IP&amp;YOGW?EM&gt;SWS[F\;U"^M`@*R^?P^ZN___\,90\W&lt;\LR^WE_]?.NZ,`],&gt;K&amp;@KHVXTT.%DEJ-T7Q!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.30.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SuperProc.connect process.vi" Type="VI" URL="../SuperProc.connect process.vi"/>
		<Item Name="SuperProc.constructor.vi" Type="VI" URL="../SuperProc.constructor.vi"/>
		<Item Name="SuperProc.create object.vi" Type="VI" URL="../SuperProc.create object.vi"/>
		<Item Name="SuperProc.destroy object.vi" Type="VI" URL="../SuperProc.destroy object.vi"/>
		<Item Name="SuperProc.destructor.vi" Type="VI" URL="../SuperProc.destructor.vi"/>
		<Item Name="SuperProc.disconnect all.vi" Type="VI" URL="../SuperProc.disconnect all.vi"/>
		<Item Name="SuperProc.disconnect process.vi" Type="VI" URL="../SuperProc.disconnect process.vi"/>
		<Item Name="SuperProc.get class names.vi" Type="VI" URL="../SuperProc.get class names.vi"/>
		<Item Name="SuperProc.get data to modify.vi" Type="VI" URL="../SuperProc.get data to modify.vi"/>
		<Item Name="SuperProc.get library version.vi" Type="VI" URL="../SuperProc.get library version.vi"/>
		<Item Name="SuperProc.get object names.vi" Type="VI" URL="../SuperProc.get object names.vi"/>
		<Item Name="SuperProc.kill all classes.vi" Type="VI" URL="../SuperProc.kill all classes.vi"/>
		<Item Name="SuperProc.kill class.vi" Type="VI" URL="../SuperProc.kill class.vi"/>
		<Item Name="SuperProc.kill object.vi" Type="VI" URL="../SuperProc.kill object.vi"/>
		<Item Name="SuperProc.load process.vi" Type="VI" URL="../SuperProc.load process.vi"/>
		<Item Name="SuperProc.open front panel.vi" Type="VI" URL="../SuperProc.open front panel.vi"/>
		<Item Name="SuperProc.register process.vi" Type="VI" URL="../SuperProc.register process.vi"/>
		<Item Name="SuperProc.remove all.vi" Type="VI" URL="../SuperProc.remove all.vi"/>
		<Item Name="SuperProc.set event preset.vi" Type="VI" URL="../SuperProc.set event preset.vi"/>
		<Item Name="SuperProc.set modified data.vi" Type="VI" URL="../SuperProc.set modified data.vi"/>
		<Item Name="SuperProc.set periodic preset.vi" Type="VI" URL="../SuperProc.set periodic preset.vi"/>
		<Item Name="SuperProc.sync proc table.vi" Type="VI" URL="../SuperProc.sync proc table.vi"/>
		<Item Name="SuperProc.unload process.vi" Type="VI" URL="../SuperProc.unload process.vi"/>
		<Item Name="SuperProc.unregister process.vi" Type="VI" URL="../SuperProc.unregister process.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SuperProc.get i attribute.vi" Type="VI" URL="../SuperProc.get i attribute.vi"/>
		<Item Name="SuperProc.ProcCases.vi" Type="VI" URL="../SuperProc.ProcCases.vi"/>
		<Item Name="SuperProc.ProcPeriodic.vi" Type="VI" URL="../SuperProc.ProcPeriodic.vi"/>
		<Item Name="SuperProc.set i attribute.vi" Type="VI" URL="../SuperProc.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="SuperProc.destroy DIM stuff from proc table.vi" Type="VI" URL="../SuperProc.destroy DIM stuff from proc table.vi"/>
		<Item Name="SuperProc.evt watchdog.vi" Type="VI" URL="../SuperProc.evt watchdog.vi"/>
		<Item Name="SuperProc.get processes to load.vi" Type="VI" URL="../SuperProc.get processes to load.vi"/>
		<Item Name="SuperProc.i attribute.ctl" Type="VI" URL="../SuperProc.i attribute.ctl"/>
		<Item Name="SuperProc.i attribute.vi" Type="VI" URL="../SuperProc.i attribute.vi"/>
		<Item Name="SuperProc.pdc watchdog.vi" Type="VI" URL="../SuperProc.pdc watchdog.vi"/>
		<Item Name="SuperProc.proc_tbl_type.ctl" Type="VI" URL="../SuperProc.proc_tbl_type.ctl"/>
		<Item Name="SuperProc.ProcConstructor.vi" Type="VI" URL="../SuperProc.ProcConstructor.vi"/>
		<Item Name="SuperProc.ProcDestructor.vi" Type="VI" URL="../SuperProc.ProcDestructor.vi"/>
		<Item Name="SuperProc.ProcEvents.vi" Type="VI" URL="../SuperProc.ProcEvents.vi"/>
	</Item>
	<Item Name="SuperProc.contents.vi" Type="VI" URL="../SuperProc.contents.vi"/>
	<Item Name="SuperProc_db.ini" Type="Document" URL="../SuperProc_db.ini"/>
	<Item Name="SuperProc_mapping.ini" Type="Document" URL="../SuperProc_mapping.ini"/>
</Library>
