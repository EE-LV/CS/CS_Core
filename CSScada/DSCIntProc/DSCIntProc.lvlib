﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">Interface between the CS framework and a real time data base that used for trending and alarming. Presently the DSC engine from National Instruments is used as the real time data base (RTDB) using Shared Variables (SV).

Configuration of SV:
When an object is created, this class reads SV properties from CSV files. The paths of the CSV files are stored in the configuration data base of CS (Properties :Address0, Address1, ...). There are a couple of possibilites to create such CSV files.
- Using the "Multiple Variable Editor" and "Export Variables..." feature  from the LabVIEW development system.
- Using MS-Excel.
- Using the actual version of the CSDBTool allows for mass configuration.
Remark1: Don't import CSV files into the Multiple Variable Editor, since OPC configuration of a SV may become broken or lost.
Remark2: None of the properties, like the "Description", may contain commas ",". Otherwise the import of the CSV files may not work.

Configuration of IO-Servers.
When a SV should be linked to OPC, you need to create an "I/O server". There are two possibilities for creating an I/O server.
1) Like creating SVs, this can be done when creating an object of this class. The properties of an IO-Server are read obtained from CSV files, of which the path is obtained from the configuration database of CS (Properties: Interface0, Interface1). Edit such a CSV file using MS-Excel. Have a look at the examples. Use this method, if the configuration of OPC server changes frequently.
2) Using the "Distributed System Manager" (Start-&gt;Program Files-&gt;National Instruments-&gt;Distributed System Manager). Create, modify or destroy I/O servers.  Unfortunately, you can not re-load or save settings. However, all changes are persistent and settings are automatically reloaded upon rebooting the machine. Use this method, when changes are less frequent like for production systems.

For configuring the relationship between DIM services and Shared Variables two keywords may be used in the "Description" property of a Shared Variable.
1) "DIM2DSC": This class subscribes to the DIM service having the same name as the shared variable. The DIM service is published elsewhere. Each time an update of the service is received, its value is written to the SV.
2) "DSC2DIM": This class subscribes to changes of the SV. Each time the SV changes, its value is published to a DIM service having the same name as the SV. In case the DIM service is an array of values, the description of a class may contain the keyword ISARRAYNN, where NN is the number of values of the array. In this case, the DSCIntProc class will write updated value received from DIM to SVs named SVNAME_CHANNELMM, where MM is the index of the array element. Each SV of the SVNAME_CHANNELMM must be included in the csv file used to create the SV (Why? Because only if the array elements are created individually, they can have individual properties like alarms...).
-
Remark: For performance reasons there is no name mapping from DIM service names to Shared Variable names. This requires, that DIM service name that are connected to DSC must not use charachter not supported by DSC like "\", "_", ...

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: D.Neidherr@gsi.de.
Last update: 21-MAR-2013

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*8!!!*Q(C=Z:9R&lt;BMR%%7`AB4O$.X!E#K8QFR"H7N&gt;91J@1+V,V5YV2^!;3/.SLT"8U"6UAA$R7_\)#I2!3O!9C'&amp;S:]8^1][]*&lt;G%J&amp;;OJ+`[?;HI&lt;0E$`^"F`+F[V(25R[PF'VOF(M;=^_P5`WP]E`RVP9Z`[`N^(P`XI=S(]G\__@RN`I_?`[T`._7S``,H`8#RUTM8;;)[&amp;B),T$%&lt;0^V%4`2%4`2%4`2!$`2!$`2!$X2(&gt;X2(&gt;X2(&gt;X2$.X2$.X2$N].2ACZUI=P;]:!E4R)F1:-!37&gt;1F.Q3HI1HY5FY?&amp;4#E`!E0!F0QE-8*4Q*4]+4]#1]$&amp;0#E`!E0!F0QE/I2J+NIM/4]""?!5`!%`!%0!%0+28Q"!""MC"Q%!1-"=[A%@!%0!%0415]!5`!%`!%0,A6]!1]!5`!%`!QJ-V+.*KBIM.$'$E]$I`$Y`!Y0)37Q_0Q/$Q/D].$/DE]$I]$Y32UAI-A:Z$4Q8FQ?"Q?@O4Q/$Q/D]0D]/"K+_2N:A;;I;,$9`!90!;0Q70Q%%)'D]&amp;D]"A]"A^B:@!90!;0Q70QE%I'D]&amp;D]"AA2F,3SQBG$$1['9,"Q^6WC\66CE:CL&gt;;H/7Z5V1:5&lt;3T6BF&amp;N".5#KR:/N3#KC6:.I'JC6#_M?B%6I#KR+K#KI`&lt;=&gt;VC0&gt;&gt;A'7W-L&lt;)H.M/EQ^"^XX/`XWOVW[PN?8&gt;&gt;JM^FIP6ZLN6JJO6RK.JNJ/JW_HF9XV,&amp;-$O@3!_X"HO`\28@XO(X#NH@@&amp;M`XD\=(XVG\`P&amp;X6O0'=_F`/"PV:@R&lt;=ZSD&amp;RW)@-M!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.15.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DSCIntProc.constructor.vi" Type="VI" URL="../DSCIntProc.constructor.vi"/>
		<Item Name="DSCIntProc.destructor.vi" Type="VI" URL="../DSCIntProc.destructor.vi"/>
		<Item Name="DSCIntProc.dsc start.vi" Type="VI" URL="../DSCIntProc.dsc start.vi"/>
		<Item Name="DSCIntProc.dsc stop.vi" Type="VI" URL="../DSCIntProc.dsc stop.vi"/>
		<Item Name="DSCIntProc.get data to modify.vi" Type="VI" URL="../DSCIntProc.get data to modify.vi"/>
		<Item Name="DSCIntProc.get library version.vi" Type="VI" URL="../DSCIntProc.get library version.vi"/>
		<Item Name="DSCIntProc.set modified data.vi" Type="VI" URL="../DSCIntProc.set modified data.vi"/>
		<Item Name="DSCIntProc.get tag list.vi" Type="VI" URL="../DSCIntProc.get tag list.vi"/>
		<Item Name="DSCIntProc.get shared variable URLs.vi" Type="VI" URL="../DSCIntProc.get shared variable URLs.vi"/>
		<Item Name="DSCIntProc.get value.vi" Type="VI" URL="../DSCIntProc.get value.vi"/>
		<Item Name="DSCIntProc.set property alarm hihi enabled.vi" Type="VI" URL="../DSCIntProc.set property alarm hihi enabled.vi"/>
		<Item Name="DSCIntProc.set property alarm hi enabled.vi" Type="VI" URL="../DSCIntProc.set property alarm hi enabled.vi"/>
		<Item Name="DSCIntProc.set property alarm lo enabled.vi" Type="VI" URL="../DSCIntProc.set property alarm lo enabled.vi"/>
		<Item Name="DSCIntProc.set property alarm lolo enabled.vi" Type="VI" URL="../DSCIntProc.set property alarm lolo enabled.vi"/>
		<Item Name="DSCIntProc.set property alarm hihi priority.vi" Type="VI" URL="../DSCIntProc.set property alarm hihi priority.vi"/>
		<Item Name="DSCIntProc.set property alarm hi priority.vi" Type="VI" URL="../DSCIntProc.set property alarm hi priority.vi"/>
		<Item Name="DSCIntProc.set property alarm lo priority.vi" Type="VI" URL="../DSCIntProc.set property alarm lo priority.vi"/>
		<Item Name="DSCIntProc.set property alarm lolo priority.vi" Type="VI" URL="../DSCIntProc.set property alarm lolo priority.vi"/>
		<Item Name="DSCIntProc.set property alarm hihi autoACK.vi" Type="VI" URL="../DSCIntProc.set property alarm hihi autoACK.vi"/>
		<Item Name="DSCIntProc.set property alarm hi autoACK.vi" Type="VI" URL="../DSCIntProc.set property alarm hi autoACK.vi"/>
		<Item Name="DSCIntProc.set property alarm lo autoACK.vi" Type="VI" URL="../DSCIntProc.set property alarm lo autoACK.vi"/>
		<Item Name="DSCIntProc.set property alarm lolo autoACK.vi" Type="VI" URL="../DSCIntProc.set property alarm lolo autoACK.vi"/>
		<Item Name="DSCIntProc.set property alarm hihi level.vi" Type="VI" URL="../DSCIntProc.set property alarm hihi level.vi"/>
		<Item Name="DSCIntProc.set property alarm hi level.vi" Type="VI" URL="../DSCIntProc.set property alarm hi level.vi"/>
		<Item Name="DSCIntProc.set property alarm lo level.vi" Type="VI" URL="../DSCIntProc.set property alarm lo level.vi"/>
		<Item Name="DSCIntProc.set property alarm lolo level.vi" Type="VI" URL="../DSCIntProc.set property alarm lolo level.vi"/>
		<Item Name="DSCIntProc.set value.vi" Type="VI" URL="../DSCIntProc.set value.vi"/>
		<Item Name="DSCIntProc.thread.vi" Type="VI" URL="../DSCIntProc.thread.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DSCIntProc.get process properties.vi" Type="VI" URL="../DSCIntProc.get process properties.vi"/>
		<Item Name="DSCIntProc.get engine state.vi" Type="VI" URL="../DSCIntProc.get engine state.vi"/>
		<Item Name="DSCIntProc.get i attribute.vi" Type="VI" URL="../DSCIntProc.get i attribute.vi"/>
		<Item Name="DSCIntProc.set i attribute.vi" Type="VI" URL="../DSCIntProc.set i attribute.vi"/>
		<Item Name="DSCIntProc.shared variable valid.vi" Type="VI" URL="../DSCIntProc.shared variable valid.vi"/>
		<Item Name="DSCIntProc.deploy libraries.vi" Type="VI" URL="../DSCIntProc.deploy libraries.vi"/>
		<Item Name="DSCIntProc.undeploy libraries.vi" Type="VI" URL="../DSCIntProc.undeploy libraries.vi"/>
		<Item Name="DSCIntProc.create shared variables.vi" Type="VI" URL="../DSCIntProc.create shared variables.vi"/>
		<Item Name="DSCIntProc.destroy shared variables.vi" Type="VI" URL="../DSCIntProc.destroy shared variables.vi"/>
		<Item Name="DSCIntProc.start IO servers.vi" Type="VI" URL="../DSCIntProc.start IO servers.vi"/>
		<Item Name="DSCIntProc.stop IO servers.vi" Type="VI" URL="../DSCIntProc.stop IO servers.vi"/>
		<Item Name="DSCIntProc.start SV process.vi" Type="VI" URL="../DSCIntProc.start SV process.vi"/>
		<Item Name="DSCIntProc.stop SV process.vi" Type="VI" URL="../DSCIntProc.stop SV process.vi"/>
		<Item Name="DSCIntProc.ProcCases.vi" Type="VI" URL="../DSCIntProc.ProcCases.vi"/>
		<Item Name="DSCIntProc.ProcPeriodic.vi" Type="VI" URL="../DSCIntProc.ProcPeriodic.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="DSCIntProc.i attribute.ctl" Type="VI" URL="../DSCIntProc.i attribute.ctl"/>
		<Item Name="DSCIntProc.DIM DSC connection.ctl" Type="VI" URL="../DSCIntProc.DIM DSC connection.ctl"/>
		<Item Name="DSCIntProc.SV description 2 connectType.vi" Type="VI" URL="../DSCIntProc.SV description 2 connectType.vi"/>
		<Item Name="DSCIntProc.SV does not exist.vi" Type="VI" URL="../DSCIntProc.SV does not exist.vi"/>
		<Item Name="DSCIntProc.shared variable data type.ctl" Type="VI" URL="../DSCIntProc.shared variable data type.ctl"/>
		<Item Name="DSCIntProc.connect to DIM.vi" Type="VI" URL="../DSCIntProc.connect to DIM.vi"/>
		<Item Name="DSCIntProc.disconnect from DIM.vi" Type="VI" URL="../DSCIntProc.disconnect from DIM.vi"/>
		<Item Name="DSCIntProc.get CSV file names.vi" Type="VI" URL="../DSCIntProc.get CSV file names.vi"/>
		<Item Name="DSCIntProc.i attribute.vi" Type="VI" URL="../DSCIntProc.i attribute.vi"/>
		<Item Name="DSCIntProc.new DIM value.vi" Type="VI" URL="../DSCIntProc.new DIM value.vi"/>
		<Item Name="DSCIntProc.ProcEvents.vi" Type="VI" URL="../DSCIntProc.ProcEvents.vi"/>
		<Item Name="DSCIntProc.set SV props.vi" Type="VI" URL="../DSCIntProc.set SV props.vi"/>
		<Item Name="DSCIntProc.set SV props alarming.vi" Type="VI" URL="../DSCIntProc.set SV props alarming.vi"/>
		<Item Name="DSCIntProc.set SV props deadband.vi" Type="VI" URL="../DSCIntProc.set SV props deadband.vi"/>
		<Item Name="DSCIntProc.set SV props initValue.vi" Type="VI" URL="../DSCIntProc.set SV props initValue.vi"/>
		<Item Name="DSCIntProc.set SV props logging.vi" Type="VI" URL="../DSCIntProc.set SV props logging.vi"/>
		<Item Name="DSCIntProc.set SV props network.vi" Type="VI" URL="../DSCIntProc.set SV props network.vi"/>
		<Item Name="DSCIntProc.set SV props scaling.vi" Type="VI" URL="../DSCIntProc.set SV props scaling.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
	</Item>
	<Item Name="DSCIntProc.contents.vi" Type="VI" URL="../DSCIntProc.contents.vi"/>
	<Item Name="DSCIntProc_db.ini" Type="Document" URL="../DSCIntProc_db.ini"/>
	<Item Name="DSCIntProc_mapping.ini" Type="Document" URL="../DSCIntProc_mapping.ini"/>
</Library>
