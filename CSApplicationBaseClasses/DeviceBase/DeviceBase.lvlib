﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the base class for a device class. It provides basic events, some methods and publishes the services "OBJECTNAME_deviceState" and "OBJECTNAME_deviceID". This class can be used by classes that are implemented for more specific device classes.

The deviceState can have three states:
0  : OK
&gt;0: NOT_OK
&lt;0: ERROR 

Child classes should provide the following methods.
"Initialize": Execute the "Initialize" routine of the instrument driver. Should be called in the constructor of a device class. Eventually, the "Reset" routine is already executed from within the "Initialize" routine.
"Close": Execute the "Close" routine of the instrument driver. Should be called in the destructor of a device class.
"Configure": Execute the "Configure" routine of the instrument driver. Can be called in the constructor of a device class.
"Reset": Execute the "Reset" routine of the instrument driver. Can be called in the constructor of a device class.
"IDQuery": Execute the "IDQuery" routine of the instrument driver.

The events for these methods are already defined in this class but can be overwritten by child classes.


author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de

Last update: 21-MAR-2013

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*B!!!*Q(C=\:1R&lt;BMR%%7`ER2J&gt;9.![NQ/E!N9P3P6!6*-Y7[&lt;K&amp;7J+]Q6F#0I#H-&amp;Q4&gt;19VCN]J9\E;U%7"G)D;1)K6G+@]C:NS385CM@J1][8CIXI[8ZO[Z:KVX@(\L&gt;S&gt;^K.&gt;V4^]T`.,]&lt;*I`/(R\H_7^/T3`T2`EP&amp;9W7``Z8^=^GM\`KVW'=\[X^B]0BD`S`F:==\^8FD_"NCX463,(%!H0-WNM&gt;%TX2%TX2%TX2!TX1!TX1!TX1(&gt;X2(&gt;X2(&gt;X2$&gt;X1$&gt;X1$&gt;X17U58ON#&amp;TKIES:.%3&gt;!E1$)9&amp;#70B#@B38A3(LJ+?"+?B#@B38A9II1HY5FY%J[%BWF+?"+?B#@B38A)V5CS682Y%B\#+_!*?!+?A#@A);5#HA!A3"9%$I+!I=!:`!FY!J[!B\]+?!+?A#@A#8BQ+_!*?!+?A#@A95J&lt;F7AU@57(BT"S?"Q?B]@B=8A),9@(Y8&amp;Y("[(BX2S?"Q?"]**[!1(1=YE:Y$4=8A=(BIZ0![0Q_0Q/$SYWAZZ7ZG?JK`I]"A]"I`"9`!90)31Q70Q'$Q'D]&amp;$7"E]"I`"9`!90+33Q70Q'$Q'C*'5^$+#'2/.19:A]0"LJ]8;,E5DM6&lt;LURQ/KOI!KA[7[M#I$I*KAV5&lt;J^I1V5+L&amp;F#V-+I86LW)#F#67"61.6"\HDNMCWWQ.&lt;&lt;%&amp;NA=GW+4@OIL$^TP^^LN&gt;NJON^JM.FKPVVIOFVIM&amp;JL0ZZJ/JZJ-*K@&lt;[B.V+&amp;@0\[7([W_XDX?&lt;_]@LL\4@PTR]PLP^[6MR^M8W@H6O)W/(?_F@O"PV4M&gt;HVTRL^!/]FH?[!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.14.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DeviceBase.constructor.vi" Type="VI" URL="../DeviceBase.constructor.vi"/>
		<Item Name="DeviceBase.destructor.vi" Type="VI" URL="../DeviceBase.destructor.vi"/>
		<Item Name="DeviceBase.close.vi" Type="VI" URL="../DeviceBase.close.vi"/>
		<Item Name="DeviceBase.configure.vi" Type="VI" URL="../DeviceBase.configure.vi"/>
		<Item Name="DeviceBase.device state 2 value status.vi" Type="VI" URL="../DeviceBase.device state 2 value status.vi"/>
		<Item Name="DeviceBase.get data to modify.vi" Type="VI" URL="../DeviceBase.get data to modify.vi"/>
		<Item Name="DeviceBase.get library version.vi" Type="VI" URL="../DeviceBase.get library version.vi"/>
		<Item Name="DeviceBase.set modified data.vi" Type="VI" URL="../DeviceBase.set modified data.vi"/>
		<Item Name="DeviceBase.initialize.vi" Type="VI" URL="../DeviceBase.initialize.vi"/>
		<Item Name="DeviceBase.IDQuery.vi" Type="VI" URL="../DeviceBase.IDQuery.vi"/>
		<Item Name="DeviceBase.create advanced GUI.vi" Type="VI" URL="../DeviceBase.create advanced GUI.vi"/>
		<Item Name="DeviceBase.reset.vi" Type="VI" URL="../DeviceBase.reset.vi"/>
		<Item Name="DeviceBase.set advanced GUI class.vi" Type="VI" URL="../DeviceBase.set advanced GUI class.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DeviceBase.myEventNames.ctl" Type="VI" URL="../DeviceBase.myEventNames.ctl"/>
		<Item Name="DeviceBase.deviceState.ctl" Type="VI" URL="../DeviceBase.deviceState.ctl"/>
		<Item Name="DeviceBase.get i attribute.vi" Type="VI" URL="../DeviceBase.get i attribute.vi"/>
		<Item Name="DeviceBase.set i attribute.vi" Type="VI" URL="../DeviceBase.set i attribute.vi"/>
		<Item Name="DeviceBase.ProcCases.vi" Type="VI" URL="../DeviceBase.ProcCases.vi"/>
		<Item Name="DeviceBase.ProcPeriodic.vi" Type="VI" URL="../DeviceBase.ProcPeriodic.vi"/>
		<Item Name="DeviceBase.delete events.vi" Type="VI" URL="../DeviceBase.delete events.vi"/>
		<Item Name="DeviceBase.set device ID.vi" Type="VI" URL="../DeviceBase.set device ID.vi"/>
		<Item Name="DeviceBase.set device state.vi" Type="VI" URL="../DeviceBase.set device state.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="DeviceBase.i attribute.ctl" Type="VI" URL="../DeviceBase.i attribute.ctl"/>
		<Item Name="DeviceBase.i attribute.vi" Type="VI" URL="../DeviceBase.i attribute.vi"/>
		<Item Name="DeviceBase.ProcEvents.vi" Type="VI" URL="../DeviceBase.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="INHERITPRIV.ProcCases.vi" Type="VI" URL="../inheritance/INHERITPRIV.ProcCases.vi"/>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="DeviceBase.contents.vi" Type="VI" URL="../DeviceBase.contents.vi"/>
</Library>
