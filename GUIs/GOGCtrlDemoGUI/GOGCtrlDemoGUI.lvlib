﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is simple class providing a demo for a facility specific GUI based on a GOGController objects in the application layer.

author: Dietrich Beck, GSI
maintainer: Dietrich Beck, GSI; d.beck@gsi.de

License Agreement for this software:

Copyright (C) 2009  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.beck@gsi.de
Last update: 16-DEC-2009

INFO2SF</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*R!!!*Q(C=\:8"&lt;2MR%%8`/D;1ES"V9+C&amp;;5%6"'!,5U%-8865#^0#ZJCD,CFA$GF!,;C&amp;^6MO*6B+\%U!"]EBJ%;\`$/=?5N3+[GWD^+^BLGW?,0B(U0'SW+]PJ#H@LZ:$/?9R22W\&lt;`%N\B&lt;`W8=ZL`-`UL^I@FH_/?;XGT``?`LPVXQW_&amp;SO2R?^Q^,ZF]&amp;`*Z`,P]MXQ`N\`PHD`&gt;O`E@Q:ZP543M\$)E&amp;ZJD6JRM30&gt;%40&gt;%40&gt;%$0&gt;!$0&gt;!$0&gt;!&gt;X&gt;%&gt;X&gt;%&gt;X&gt;%.X&gt;!.X&gt;!.X&gt;"L2R?[U)8/KC4&amp;EU**UC2"%AS+EK_%*_&amp;*?")?BEJY%J[%*_&amp;*?!B2QJ0Q*$Q*4],$.#5]#5`#E`!E0+3K*&amp;E\/DQ*$_E6]!1]!5`!%`"15A&amp;0!"!5#R)(3="1Y!RO!J[!*_$B6A&amp;0Q"0Q"$Q"$WY&amp;0!&amp;0Q"0Q"$R-K;M3F7&lt;M[0#12A[0Q_0Q/$Q/$[HF]$A]$I`$Y`"14A[0Q_.!/!7&gt;Z#$)G?1%/!/(R_(B)I@(Y8&amp;Y("[("V@&gt;);]L-^+-(2U?A]@A-8A-(I/(&amp;$*Y$"[$R_!R?%ALA]@A-8A-(I/(5D*Y$"[$RQ!RCF*?2D*DIB&amp;E#!90HXJ;L/Z36"+LP@UUJY/K&gt;A$6$J&lt;;A6%\#'I&lt;L,:R;BOCNN"K#[CW-'I0L09A;I"KB&gt;53KA8KR0=2/W!^NM?W7-%WW"J&lt;D60@/@"U/OFY0/JQ/+DP?_XX?WWX7Z63N.FMN&amp;[PN6KN,G_L2`L5OP.\;=@^VU]@3P^U8\[5T[5P$Y8RU[C@`&lt;^MX&lt;&gt;O&gt;`@9\2[_4T&lt;?D^J09K@XUL`Q&lt;N4&gt;V&lt;]/;`1-TQ&gt;^&gt;Q!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">0.2.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="GOGCtrlDemoGUI.constructor.vi" Type="VI" URL="../GOGCtrlDemoGUI.constructor.vi"/>
		<Item Name="GOGCtrlDemoGUI.destructor.vi" Type="VI" URL="../GOGCtrlDemoGUI.destructor.vi"/>
		<Item Name="GOGCtrlDemoGUI.get data to modify.vi" Type="VI" URL="../GOGCtrlDemoGUI.get data to modify.vi"/>
		<Item Name="GOGCtrlDemoGUI.get library version.vi" Type="VI" URL="../GOGCtrlDemoGUI.get library version.vi"/>
		<Item Name="GOGCtrlDemoGUI.set modified data.vi" Type="VI" URL="../GOGCtrlDemoGUI.set modified data.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="GOGCtrlDemoGUI.get i attribute.vi" Type="VI" URL="../GOGCtrlDemoGUI.get i attribute.vi"/>
		<Item Name="GOGCtrlDemoGUI.set i attribute.vi" Type="VI" URL="../GOGCtrlDemoGUI.set i attribute.vi"/>
		<Item Name="GOGCtrlDemoGUI.ProcCases.vi" Type="VI" URL="../GOGCtrlDemoGUI.ProcCases.vi"/>
		<Item Name="GOGCtrlDemoGUI.ProcPeriodic.vi" Type="VI" URL="../GOGCtrlDemoGUI.ProcPeriodic.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="GOGCtrlDemoGUI.i attribute.ctl" Type="VI" URL="../GOGCtrlDemoGUI.i attribute.ctl"/>
		<Item Name="GOGCtrlDemoGUI.i attribute.vi" Type="VI" URL="../GOGCtrlDemoGUI.i attribute.vi"/>
		<Item Name="GOGCtrlDemoGUI.ProcEvents.vi" Type="VI" URL="../GOGCtrlDemoGUI.ProcEvents.vi"/>
	</Item>
	<Item Name="GUIStuff" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="GOGCtrlDemoGUI.anotherSubSystem.ctl" Type="VI" URL="../GUIStuff/GOGCtrlDemoGUI.anotherSubSystem.ctl"/>
		<Item Name="GOGCtrlDemoGUI.DemoMainSystem.vi" Type="VI" URL="../GUIStuff/GOGCtrlDemoGUI.DemoMainSystem.vi"/>
		<Item Name="GOGCtrlDemoGUI.DemoSubSystem2.vi" Type="VI" URL="../GUIStuff/GOGCtrlDemoGUI.DemoSubSystem2.vi"/>
		<Item Name="GOGCtrlDemoGUI.DemoSubSystem1-1.vi" Type="VI" URL="../GUIStuff/GOGCtrlDemoGUI.DemoSubSystem1-1.vi"/>
		<Item Name="GOGCtrlDemoGUI.DemoSubSystem1.vi" Type="VI" URL="../GUIStuff/GOGCtrlDemoGUI.DemoSubSystem1.vi"/>
		<Item Name="GOGCtrlDemoGUI.DemoSubSystem3.vi" Type="VI" URL="../GUIStuff/GOGCtrlDemoGUI.DemoSubSystem3.vi"/>
		<Item Name="GOGCtrlDemoGUI.subSystem.ctl" Type="VI" URL="../GUIStuff/GOGCtrlDemoGUI.subSystem.ctl"/>
	</Item>
	<Item Name="settings" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DemoMainSystem_test1.gog" Type="Document" URL="../DemoSystemSettings/DemoMainSystem_test1.gog"/>
		<Item Name="DemoMainSystem_tmp.gog" Type="Document" URL="../DemoSystemSettings/DemoMainSystem_tmp.gog"/>
		<Item Name="DemoMainSystem_tmpBackup.gog" Type="Document" URL="../DemoSystemSettings/DemoMainSystem_tmpBackup.gog"/>
		<Item Name="DemoSubSystem1-1_test1.gog" Type="Document" URL="../DemoSystemSettings/DemoSubSystem1-1_test1.gog"/>
		<Item Name="DemoSubSystem1-1_tmp.gog" Type="Document" URL="../DemoSystemSettings/DemoSubSystem1-1_tmp.gog"/>
		<Item Name="DemoSubSystem1-1_tmpBackup.gog" Type="Document" URL="../DemoSystemSettings/DemoSubSystem1-1_tmpBackup.gog"/>
		<Item Name="DemoSubSystem1_test1.gog" Type="Document" URL="../DemoSystemSettings/DemoSubSystem1_test1.gog"/>
		<Item Name="DemoSubSystem1_tmp.gog" Type="Document" URL="../DemoSystemSettings/DemoSubSystem1_tmp.gog"/>
		<Item Name="DemoSubSystem1_tmpBackup.gog" Type="Document" URL="../DemoSystemSettings/DemoSubSystem1_tmpBackup.gog"/>
		<Item Name="DemoSubSystem2_test1.gog" Type="Document" URL="../DemoSystemSettings/DemoSubSystem2_test1.gog"/>
		<Item Name="DemoSubSystem2_tmp.gog" Type="Document" URL="../DemoSystemSettings/DemoSubSystem2_tmp.gog"/>
		<Item Name="DemoSubSystem2_tmpBackup.gog" Type="Document" URL="../DemoSystemSettings/DemoSubSystem2_tmpBackup.gog"/>
		<Item Name="DemoSubSystem3_test1.gog" Type="Document" URL="../DemoSystemSettings/DemoSubSystem3_test1.gog"/>
		<Item Name="DemoSubSystem3_tmp.gog" Type="Document" URL="../DemoSystemSettings/DemoSubSystem3_tmp.gog"/>
		<Item Name="DemoSubSystem3_tmpBackup.gog" Type="Document" URL="../DemoSystemSettings/DemoSubSystem3_tmpBackup.gog"/>
		<Item Name="DemoSystemTest.gog" Type="Document" URL="../DemoSystemSettings/DemoSystemTest.gog"/>
		<Item Name="GOGCtrlDemoGUI.csv" Type="Document" URL="../DemoSystemSettings/GOGCtrlDemoGUI.csv"/>
		<Item Name="SimDevices4Demo_db.ini" Type="Document" URL="../DemoSystemSettings/SimDevices4Demo_db.ini"/>
	</Item>
	<Item Name="GOGCtrlDemoGUI.contents.vi" Type="VI" URL="../GOGCtrlDemoGUI.contents.vi"/>
	<Item Name="GOGCtrlDemoGUI_mapping.ini" Type="Document" URL="../GOGCtrlDemoGUI_mapping.ini"/>
	<Item Name="GOGCtrlDemoGUI_db.ini" Type="Document" URL="../GOGCtrlDemoGUI_db.ini"/>
</Library>
