﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This class provides a client for object hierarchy trees. An object of this client class connects to a CS Access Server for reserving an object. The client is typically created by objects that have inherited from the CAEObj class. The "data in" of this class contains the reference of the object creating THIS object.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de or h.brand@gsi.de
Last update: 20-Mar-2013

INFO2SF</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+(!!!*Q(C=\:3R&lt;B."%):`!Y)5.(Y$Z)YCV&lt;S#+8A!FX2I;DKX,PU%E;:&amp;%!F,0)&amp;L&amp;Z(G?BJ,N$2_"@0&gt;?O,%RC%A!5L"LO&gt;W\Z`:G?`WVC?V&gt;C9^U@;_JJ_WXL`3;N6@W[RO7W@%P`/NLJUVVJ,S\Y,X`N72PV;@8H^4=$_\#@Y6`P`_@_HPOOZO`^O_X&lt;W_^X5(!1@_ZOZU/_#Y`E(R(`T&gt;M@P1@_Q]E@`B_?``?]`O$@L,42IU5CSRQ"ST^H4&lt;2%`U2%`U2%`U1!`U1!`U1!^U2X&gt;U2X&gt;U2X&gt;U1T&gt;U1T&gt;U1T@UVN'&amp;,H3BMSN*];21ED2*E!3$IO33]#1]#5`#Q[U3HI1HY5FY%BZ#F0!E0!F0QJ0QM%Q*4]+4]#1]#1_J'EGWDAZ0QE.["4Q"4]!4]!1]F&amp;4!%Q!%R9,%12)Q&amp;$C$3=!4]!1]4"8Q"$Q"4]!4]/"7Q"0Q"$Q"4]$$EL9LU7D[DAY0;?4Q/$Q/D]0D]*";$I`$Y`!Y0!Y0Z?4Q/$Q/B&amp;0131[#H%6/A(0D]$A]$(*Y("[(R_&amp;R?(#V._2N:XK;PK0$9`!90!;0Q70QE%)'D]&amp;D]"A]"A^J:@!90!;0Q70Q5%I'D]&amp;D]"AA2F(+SUBG,$3#$-(AY&gt;&gt;/C\7X&amp;)X%7K_`ZO[AKA[A[G#J$ITK)+B?M/L&amp;K6[);K.6'[D;'.5$KRZ%";AKL%KI#N3'[RJ&lt;9ANMDEWR#4&lt;'2NCQ8`K(!T?&lt;D&gt;&lt;LN:&lt;,J2;,B?&lt;TO;&lt;4K3;4C=&lt;DM5;DE9&lt;$Y@ZL^9+_;Y0L\^+-_=7L]T=@0LW]OPRW@P7?]?,DZ^?^`HPW&lt;D"\`(5Q?]LY`-NAVP&gt;H['?HYX@@J9@Q&lt;&gt;1D&lt;7^^ZNGD\UT/21%!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.30.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CSAccessClient.constructor.vi" Type="VI" URL="../CSAccessClient.constructor.vi"/>
		<Item Name="CSAccessClient.destructor.vi" Type="VI" URL="../CSAccessClient.destructor.vi"/>
		<Item Name="CSAccessClient.get data to modify.vi" Type="VI" URL="../CSAccessClient.get data to modify.vi"/>
		<Item Name="CSAccessClient.get library version.vi" Type="VI" URL="../CSAccessClient.get library version.vi"/>
		<Item Name="CSAccessClient.panel.vi" Type="VI" URL="../CSAccessClient.panel.vi"/>
		<Item Name="CSAccessClient.set modified data.vi" Type="VI" URL="../CSAccessClient.set modified data.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CSAccessClient.get i attribute.vi" Type="VI" URL="../CSAccessClient.get i attribute.vi"/>
		<Item Name="CSAccessClient.set i attribute.vi" Type="VI" URL="../CSAccessClient.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="CSAccessClient.i attribute.ctl" Type="VI" URL="../CSAccessClient.i attribute.ctl"/>
		<Item Name="CSAccessClient.i attribute.vi" Type="VI" URL="../CSAccessClient.i attribute.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Item Name="INHERITPRIV.panel.vi" Type="VI" URL="../inheritance/INHERITPRIV.panel.vi"/>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="PRIVATE.panel.vi" Type="VI" URL="../inheritance/PRIVATE.panel.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="CSAccessClient.contents.vi" Type="VI" URL="../CSAccessClient.contents.vi"/>
	<Item Name="CSAccessClient.rtm" Type="Document" URL="../CSAccessClient.rtm"/>
</Library>
