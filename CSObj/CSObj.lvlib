﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">The CSObj class is the base class to all other classes. It provides objects and their attribute data. The attribute data can be locked for exclusive access with the help of semaphores. Moreover, it provides a reference to the active front panel of an object.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2001  Dietrich Beck, Holger Brand, Mathias Richter
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de or h.brand@gsi.de
Last update: 20-Mar-2013

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*?!!!*Q(C=\:;\&lt;2N"%):`'1Y-8'*W9,'&amp;;5!''#JF#R-\-F-":M!7*H2[5!&gt;M96JAI!&lt;9AP4&gt;XBR"-_$:-!5Y]#\H(P`MTHT\O!7F6DZ*(`5[6X3V`+;@WR4K&gt;24LN;ZF\4&lt;7%M\],=&lt;I09HHDS7=V7PZ&gt;8,=9HT``&lt;@S&gt;^@^8&gt;&gt;&gt;]X@&gt;29.@`6VXW?$0`(0R:`FGRX&gt;:`NY``XE`T4:[ZS,&gt;4&gt;^X9I%Z:O."E?C*HOC*HOC*(OC"(OC"(OC"\OC/\OC/\OC/&lt;OC'&lt;OC'&lt;OAW(2DI1B?[L*U.3@)E52)U#:!U"E8**?&amp;*?"+?B)&gt;8*4Q*4]+4]#1].&amp;(#E`!E0!F0QE-X*4Q*4]+4]#1]B'IEW3I[0!E0Y28Q"$Q"4]!4]*"3!5]!%#1,!A&gt;"Q&amp;$A$"Y#HI!HY/&amp;2!5`!%`!%0!%0&lt;A5]!5`!%`!%0(2JMR+.:KDI]""'$I`$Y`!Y0!Y0I?8Q/$Q/D]0D]*"/$I`$YU!Y#:XA)-DJZ$2Q8BQ?BY?&lt;("[(R_&amp;R?"Q?8'W&amp;P-X-1$.5&gt;(A-(I0(Y$&amp;Y$"Z#S/!R?!Q?A]@A);Q-(I0(Y$&amp;Y$"Z3S?!R?!Q?!]2)3HI:Q9S/2C.$-(DYN&gt;VC&lt;:7CE6CL^7G/'V7V!65&lt;3\6B6"N"N=#KB6-NC'KC62/IGBD6A&amp;5$51'K%KM#KBLKS07!\&lt;%?WW%&lt;&lt;)WNM#7W',L?O/(R?.4B=."_PV@@^^LN&gt;NJM.FKPVVKN6FIOFVIM&amp;K@4[ANV,(@4O@4%]W!`PDW_&lt;"`[__XXZ`PNZ`YH\V]HXXP9?#\^#W?D0JT_O^1=P1'4]STV!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.30.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CSObj.constructor.vi" Type="VI" URL="../CSObj.constructor.vi"/>
		<Item Name="CSObj.destructor.vi" Type="VI" URL="../CSObj.destructor.vi"/>
		<Item Name="CSObj.get age.vi" Type="VI" URL="../CSObj.get age.vi"/>
		<Item Name="CSObj.get data to modify.vi" Type="VI" URL="../CSObj.get data to modify.vi"/>
		<Item Name="CSObj.get library version.vi" Type="VI" URL="../CSObj.get library version.vi"/>
		<Item Name="CSObj.is locked.vi" Type="VI" URL="../CSObj.is locked.vi"/>
		<Item Name="CSObj.open front panel.vi" Type="VI" URL="../CSObj.open front panel.vi"/>
		<Item Name="CSObj.set modified data.vi" Type="VI" URL="../CSObj.set modified data.vi"/>
		<Item Name="CSObj.set object condition.vi" Type="VI" URL="../CSObj.set object condition.vi"/>
		<Item Name="CSObj.panel.vi" Type="VI" URL="../CSObj.panel.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CSObj.acquire.vi" Type="VI" URL="../CSObj.acquire.vi"/>
		<Item Name="CSObj.get front panel ref.vi" Type="VI" URL="../CSObj.get front panel ref.vi"/>
		<Item Name="CSObj.get i attribute.vi" Type="VI" URL="../CSObj.get i attribute.vi"/>
		<Item Name="CSObj.release.vi" Type="VI" URL="../CSObj.release.vi"/>
		<Item Name="CSObj.set front panel ref.vi" Type="VI" URL="../CSObj.set front panel ref.vi"/>
		<Item Name="CSObj.set i attribute.vi" Type="VI" URL="../CSObj.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="CSObj.i attribute.ctl" Type="VI" URL="../CSObj.i attribute.ctl"/>
		<Item Name="CSObj.i attribute.vi" Type="VI" URL="../CSObj.i attribute.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="INHERITPRIV.i attribute.ctl" Type="VI" URL="../inheritance/INHERITPRIV.i attribute.ctl"/>
		<Item Name="INHERITPRIV.i attribute.vi" Type="VI" URL="../inheritance/INHERITPRIV.i attribute.vi"/>
		<Item Name="INHERITPROT.get i attribute.vi" Type="VI" URL="../inheritance/INHERITPROT.get i attribute.vi"/>
		<Item Name="INHERITPROT.set i attribute.vi" Type="VI" URL="../inheritance/INHERITPROT.set i attribute.vi"/>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="INHERITPUB.get data to modify.vi" Type="VI" URL="../inheritance/INHERITPUB.get data to modify.vi"/>
		<Item Name="INHERITPUB.set modified data.vi" Type="VI" URL="../inheritance/INHERITPUB.set modified data.vi"/>
		<Item Name="PRIVATE.i attribute.ctl" Type="VI" URL="../inheritance/PRIVATE.i attribute.ctl"/>
		<Item Name="PRIVATE.i attribute.vi" Type="VI" URL="../inheritance/PRIVATE.i attribute.vi"/>
		<Item Name="PROTECTED.get i attribute.vi" Type="VI" URL="../inheritance/PROTECTED.get i attribute.vi"/>
		<Item Name="PROTECTED.set i attribute.vi" Type="VI" URL="../inheritance/PROTECTED.set i attribute.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PUBLIC.get data to modify.vi" Type="VI" URL="../inheritance/PUBLIC.get data to modify.vi"/>
		<Item Name="PUBLIC.set modified data.vi" Type="VI" URL="../inheritance/PUBLIC.set modified data.vi"/>
	</Item>
	<Item Name="CSObj.contents.vi" Type="VI" URL="../CSObj.contents.vi"/>
</Library>
