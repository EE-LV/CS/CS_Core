﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the constructor of the CSTimer class.

This class can be used to dispatch events at an absolute time, with a releative delay, periodically or on trigger.

You need to specify the CSTimer properties at construction. Connect the configured CSTimer.i_attrubute as flattened string to &lt;B&gt;data in&lt;/B&gt;.

Refer to CSTimer.thread.vit documentation for details.

INFO2SF

author: Holger Brand, GSI
maintainer: Holger Brand, GSI
history:
08-FEB-2007 H.Brand@gsi.de Upgraded to CS 3.1
18-NOV-2010 H.Brand@gsi.de Upgraded to CS 3.2

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: H.Brand@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: M.Richter@gsi.de, H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 13-JAN-2005</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+"!!!*Q(C=\:5REN.!%%7`NR11IO)#7]YAW_K-48U!%F_BL_$5I5-)OUC!$'^%L)1$^"6]"6^"0)V':LUO&lt;&amp;/V7U8!D&amp;MD`?\J@BK.:+GU6V+D`F,4W4&lt;YK`74;4I@`7610:413?M0`E01Z+^J$`\CG)9[P^?DWI^T`_&lt;1.@T``=`H@\JB4O;@R0`6`,ZJGP\0`O)_#DDR^]VZ@X0/@[H_&amp;@@`J,W]``,LP&lt;Y9^-*.GEWP@7+"/7&lt;DWZXIC:\IC:\IC2\IA2\IA2\IA?\IDO\IDO\IDG\IBG\IBG\I.HV/U)5O&gt;&amp;H:J%HRJ&amp;#3.%G1")/CZ*$Q*$Q*4],$J2+?B#@B38A3(E+5]#1]#5`#E`!Q41F0QJ0Q*$Q*$[E+3:;/$E`#1XI&amp;0!&amp;0Q"0Q"$S56-!4!!4&amp;AM2"%D!5/)/4A#@A#8AY6=!4]!1]!5`!AVM"4]!4]!1]!1^4SKJ%I2E[/DSEE=0D]$A]$I`$1WIZ0![0Q_0Q/$S5E]0D]$A14E%H/1BS*DE"TI8$Y`!QS/&amp;R?"Q?B]@BQ67?E*?6'7C'DA[0Q70Q'$Q'D]&amp;$#BE]"I`"9`!90+36Q70Q'$Q'D]&amp;$+2E]"I`"9Y!923EP)ZERU1AS")/(8^EN6JZ3&amp;")LP&lt;[;YU:6X9#K'UNVQ[BO".5(L0LA6"_)[E+L,K$KQKD?M/K.K!+K&amp;F:.K"KI0==&gt;VG&amp;&lt;&lt;)/NM#7WQ/:9/UR^ZM$^@K`&gt;&lt;K?O[\4&gt;&lt;L8:&lt;,2;L&lt;2=,L69,$3@T^7W\?&amp;L&gt;5M@WWT[,KUZD_\BR\@W`&gt;OP\@X&gt;FU`P\D[`?@A_[&amp;@&lt;\#@D?BRP&lt;G@LZA0W%8M^8B@`]:TRO`1P@"NV=`3PQBL^!H9_66M!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.20.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CSTimer.constructor.vi" Type="VI" URL="../public/CSTimer.constructor.vi"/>
		<Item Name="CSTimer.destructor.vi" Type="VI" URL="../public/CSTimer.destructor.vi"/>
		<Item Name="CSTimer.get data to modify.vi" Type="VI" URL="../public/CSTimer.get data to modify.vi"/>
		<Item Name="CSTimer.set modified data.vi" Type="VI" URL="../public/CSTimer.set modified data.vi"/>
		<Item Name="CSTimer.TimerType.ctl" Type="VI" URL="../public/CSTimer.TimerType.ctl"/>
		<Item Name="CSTimer.Test.vi" Type="VI" URL="../public/CSTimer.Test.vi"/>
		<Item Name="CSTimer.i attribute.ctl" Type="VI" URL="../private/CSTimer.i attribute.ctl"/>
		<Item Name="CSTimer.get library version.vi" Type="VI" URL="../CSTimer.get library version.vi"/>
		<Item Name="CSTimer.thread.vi" Type="VI" URL="../private/CSTimer.thread.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CSTimer.dispatchEvents.vi" Type="VI" URL="../protected/CSTimer.dispatchEvents.vi"/>
		<Item Name="CSTimer.get i attribute.vi" Type="VI" URL="../protected/CSTimer.get i attribute.vi"/>
		<Item Name="CSTimer.set i attribute.vi" Type="VI" URL="../protected/CSTimer.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="CSTimer.i attribute.vi" Type="VI" URL="../private/CSTimer.i attribute.vi"/>
	</Item>
	<Item Name="CSTimer.contents.vi" Type="VI" URL="../CSTimer.contents.vi"/>
</Library>
