﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the constructor of the CSPopUpMsgBox class.
You can configure up to three buttons (with several actions).

Use the CSPopUpMsgBox.configureButton.vi and the CSPopUpMsgBox.createPopUp.vi for convenient handling. 

INFO2SF

author: Thomas Rechel
maintainer: H.Brand@gsi.de
history: 15-FEB-2007 Upgrade to CS 3.1, H.Brand@gsi.de

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*N!!!*Q(C=\:9^DBJ"%%9`L!W=EDOQZAB&lt;6S$R!&lt;B#\2&amp;)=5:I6EZ+7GXAE#NQB&lt;I#Q6[!+YT@^"1ML.?QMGT*FNR.$4V@&gt;6=^_E^)L&lt;S8&lt;N2@+\J9XO*`%;1P?`;@P0`=@YQU_CPIK&gt;I@(Y@G=@RV`QFJ@Q,^)`JZM\LW&amp;`XHHN@GZRGF@]6`!PQSTJP]6_*@+P`^`\L`_P&amp;?8OXUBYMU'1^=XS=7G'-W&lt;NF%4`2%4`2%4`2!$`2!$`2!$X2(&gt;X2(&gt;X2(&gt;X2$.X2$.X2$N`&amp;)^%)8ON"F\:!ES:.%3&gt;!E1.):&amp;#70B#@B38A3(F[6]#1]#5`#E`$121F0QJ0Q*$Q*$]/5]#1]#5`#E`!1KJ&amp;EK_DQ*$S%6]!4]!1]!5`!1UI&amp;0!&amp;!E#Q)(!1"1Y%T;!1]!5`!1V-"4]!4]!1]!1^O"4Q"4]!4]!1]$'GT%IVGK/DQ%%9/D]0D]$A]$A_BZ@!Y0![0Q_0QE%Y/D]0D1$A*H?!AS"HE&gt;("?("[(BS]Z0![0Q_0Q/$SYWAJZGZG":KDI]"A]"I`"9`!90)31Q70Q'$Q'D]&amp;$7"E]"I`"9`!90+33Q70Q'$Q'C*'5^$+#'1/.4I:A]0"JO]8;+E5DM6&lt;L;)Y&lt;6&lt;5"62N,N7&amp;5'U'VQ+K&amp;5SW);K*6%[C;'.506PU1&amp;;!KM3KAKK0W0(@9&amp;NNA+WS"T&lt;%:VG(49?BP\LD@\\8&lt;\&lt;4&gt;&lt;L8:&lt;,2;L&lt;29,$3@TT7&lt;T&gt;2VH;&lt;4[@'W_EA&gt;S_2Q,SVJ$`;A&lt;H)`W&amp;XX^0$VW`L,9(=@HJ9X_PS)L?HT?,O=L$_JW&gt;!_D0U6'__FP_&amp;OV,OT@V$-U8==!FK^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.20.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CSPopUpMsgBox.configureButton.vi" Type="VI" URL="../public/CSPopUpMsgBox.configureButton.vi"/>
		<Item Name="CSPopUpMsgBox.constructor.vi" Type="VI" URL="../public/CSPopUpMsgBox.constructor.vi"/>
		<Item Name="CSPopUpMsgBox.createPopUp.vi" Type="VI" URL="../public/CSPopUpMsgBox.createPopUp.vi"/>
		<Item Name="CSPopUpMsgBox.destructor.vi" Type="VI" URL="../public/CSPopUpMsgBox.destructor.vi"/>
		<Item Name="CSPopUpMsgBox.get data to modify.vi" Type="VI" URL="../public/CSPopUpMsgBox.get data to modify.vi"/>
		<Item Name="CSPopUpMsgBox.set modified data.vi" Type="VI" URL="../public/CSPopUpMsgBox.set modified data.vi"/>
		<Item Name="CSPopUpMsgBox.Test.vi" Type="VI" URL="../public/CSPopUpMsgBox.Test.vi"/>
		<Item Name="CSPopUpMsgBox.Button.ctl" Type="VI" URL="../public/CSPopUpMsgBox.Button.ctl"/>
		<Item Name="CSPopUpMsgBox.panel.vi" Type="VI" URL="../private/CSPopUpMsgBox.panel.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CSPopUpMsgBox.get i attribute.vi" Type="VI" URL="../protected/CSPopUpMsgBox.get i attribute.vi"/>
		<Item Name="CSPopUpMsgBox.set i attribute.vi" Type="VI" URL="../protected/CSPopUpMsgBox.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="CSPopUpMsgBox.i attribute.ctl" Type="VI" URL="../private/CSPopUpMsgBox.i attribute.ctl"/>
		<Item Name="CSPopUpMsgBox.i attribute.vi" Type="VI" URL="../private/CSPopUpMsgBox.i attribute.vi"/>
	</Item>
	<Item Name="forFriendsOnly" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CSPopUpMsgBox.ButtonSize2BooleanText.vi" Type="VI" URL="../private/CSPopUpMsgBox.ButtonSize2BooleanText.vi"/>
	</Item>
	<Item Name="CSPopUpMsgBox.contents.vi" Type="VI" URL="../CSPopUpMsgBox.contents.vi"/>
</Library>
