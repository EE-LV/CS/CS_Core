﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">A dedicated GUI for the AFGBase class. It can be used by all classes that have inherited from the AFGBase class.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de

Last update: 21-MAR-2013

INFO2SF</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+!!!!*Q(C=\:1^DBJ"%%;`N3T:+4?Q)(@A/A,E2-37(&amp;3QA35=G(2$LF"8Q%=A*KML).]!\1:)FBTA.TW^?'SE:G7N*1@OJG$GK\]XX=V):&lt;S78OJU&lt;5S&lt;I`=P_@47`5Q(F]7`,(/[0&amp;_?&lt;[O`DS\_Z=`&lt;S`R@MA@ZUX0S]D&amp;OG.`EPT&lt;5(0`^T_G`80,@`&lt;OW@V=#'P\*J/8@4&lt;I/OZ:`UP+@]&amp;/AQ&gt;`O`[U@@_S`(%]ZXH@8`Q2`&gt;UAXB22,,$$(L$T&gt;+&gt;%40&gt;%40&gt;%40&gt;!$0&gt;!$0&gt;!$X&gt;%&gt;X&gt;%&gt;X&gt;%&gt;X&gt;!.X&gt;!.X&gt;!.P5RUI1N&gt;[+R+UDRJF"2.#C4"I#DZ3HA3HI1HY?&amp;7#5`#E`!E0!E0)5JY%J[%*_&amp;*?%B4QJ0Q*$Q*4]*$K5+3:;,$E`"18A&amp;0Q"0Q"$Q"$SU6]!1!1&lt;/A=&amp;!%$!8/Y#,A#8A#(CY6]!1]!5`!%`$A6M!4]!1]!5`!1UJ:F3AUX53(BT*S?"Q?B]@B=8AI,9@(Y8&amp;Y("[(BX:S?"Q?"]*J["1(15[3%_$=/$Q/$T^S?"Q?B]@B=8BQF2XSMD)&gt;44@2Y4&amp;Y$"[$R_!R?#ABA]@A-8A-(I/(MD*Y$"[$R_!R?'AFA]@A-8A-%+-J\755-R+.)%-Q?0C5UW*FF[+17*HVL^E@6.5$K(KQ6!_-[E&amp;1X7$6D60&gt;%.7&amp;6FV!V962@7$6"V%&amp;6'WM7F!V5!?_^^A7WW"L&lt;)5NM"EWRE:&gt;[D-((AY(\@&gt;\&lt;&lt;&gt;&lt;&lt;49&lt;L&gt;&gt;LL69L,29,T79TD=&gt;DD5;D]^PK$&lt;-@.]0XUP&gt;XH_@(DZOP$W]`T)_X8^Y@0^X/(XVXR$\:8NWX&lt;2$&lt;PZ@_B8?D8OAU?-WT2D]!Y3_N/A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.14.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="AFGBaseGUI.constructor.vi" Type="VI" URL="../AFGBaseGUI.constructor.vi"/>
		<Item Name="AFGBaseGUI.destructor.vi" Type="VI" URL="../AFGBaseGUI.destructor.vi"/>
		<Item Name="AFGBaseGUI.get library version.vi" Type="VI" URL="../AFGBaseGUI.get library version.vi"/>
		<Item Name="AFGBaseGUI.panel.vi" Type="VI" URL="../AFGBaseGUI.panel.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="AFGBaseGUI.get i attribute.vi" Type="VI" URL="../AFGBaseGUI.get i attribute.vi"/>
		<Item Name="AFGBaseGUI.set i attribute.vi" Type="VI" URL="../AFGBaseGUI.set i attribute.vi"/>
		<Item Name="AFGBaseGUI.update simple listbox.vi" Type="VI" URL="../AFGBaseGUI.update simple listbox.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="AFGBaseGUI.i attribute.ctl" Type="VI" URL="../AFGBaseGUI.i attribute.ctl"/>
		<Item Name="AFGBaseGUI.i attribute.vi" Type="VI" URL="../AFGBaseGUI.i attribute.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.panel.vi" Type="VI" URL="../inheritance/PRIVATE.panel.vi"/>
	</Item>
	<Item Name="AFGBaseGUI.contents.vi" Type="VI" URL="../AFGBaseGUI.contents.vi"/>
</Library>
