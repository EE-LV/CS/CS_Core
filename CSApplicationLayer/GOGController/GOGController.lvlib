﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">Class for "GeneralObjectGUI controller objects" of an application layer. The infrastructure provided by a GOG is used for creating objects and executing methods. Each GOGController object has a GOG object associated. The GOG object does not need to be configured via the database, but is configured via the constructor of THIS class. The configuration file of the GOG object can be set-up by using another GOG like the user GOG.

Subcontrollers are defined via the database as "Interface" elements.
Devices may be defined via the associated GOG object.

author: Dietrich Beck, GSI
maintainer: Dietrich Beck, GSI; d.beck@gsi.de



License Agreement for this software:

Copyright (C) 2009  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.beck@gsi.de
Last update: 02-DEC-2009

INFO2SF</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+$!!!*Q(C=\:5\DBJ"%):`6IPEF"MALF$"8I!&lt;7)D)=F&lt;C"A1EB+1/3UY==Q.%MA?IV/&amp;)DH9TLD$\4&gt;.GV_RKRJ,8EA.X5`0YK\PK[]=U5CE@J&amp;OV1W867TL`Y9#NTP&gt;S021L)P\C+L7Y[[5K6`\$M`^&amp;`_@[6P`8_3`_QWK9@[CIN`TXP[``?M+P8]@D=&gt;PL&lt;``1XR^@VTPCD@(VRB`Q$YX`&gt;&lt;*"``$WXAZ`"(_X3+0TS.MWM=!=MT+[.N%40&gt;%40&gt;%40&gt;!$0&gt;!$0&gt;!$X&gt;%&gt;X&gt;%&gt;X&gt;%&gt;X&gt;!.X&gt;!.X&gt;#N7Y[OIAN&gt;[,+S2%HS*&amp;%3.!G1.!:&amp;S38B38A3HI3(6S5]#5`#E`!E0$22QJ0Q*$Q*4]*$.S5]#5`#E`!E0)1K*&amp;EK/DQ*$_%6]!1]!5`!%`#15A&amp;0!"!E#Q)(1="1Y!Q?!J[!*_$B51&amp;0Q"0Q"$Q"$WY&amp;0!&amp;0Q"0Q"$RU+&lt;-3B;;L[0!12A[0Q_0Q/$Q/$[(F]$A]$I`$Y`#14A[0Q_.!/!G&gt;Y#$)[?1U=&amp;Y=(I?(GRQ?B]@B=8A=(FRFB&lt;T-4%@4682Y$"[$R_!R?!Q?1MDA-8A-(I0(Y#'M$"[$R_!R?!Q?5MHA-8A-(A0%3%J['='-DE9D1T"Y_*8&gt;9G76IJ"9K@840'^5V1WIOL&amp;5.YTK2F"&gt;9.7&amp;5VU1V9F7H5$6C6%&gt;M/J!6!&amp;6%[M'6'WI%^='/W*\&lt;)?NM15WRW&lt;9J/P[TAV0JZ/;JN(R?.2_P^&gt;ON^.[P&gt;:CM&gt;"]0N&gt;M.N.E-LG=6F0KO9R?HEM0DV`OGM_&lt;:@.J=`&gt;^O6E_0(\^_./XJ?VPW_B_N,W:9N_K]4T[A7^;H_]P&lt;=`HUL^Q.OLGFX]&amp;ZOA*3#OKC!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">0.2.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="GOGController.apply nominal values.vi" Type="VI" URL="../GOGController.apply nominal values.vi"/>
		<Item Name="GOGController.constructor.vi" Type="VI" URL="../GOGController.constructor.vi"/>
		<Item Name="GOGController.destructor.vi" Type="VI" URL="../GOGController.destructor.vi"/>
		<Item Name="GOGController.get data to modify.vi" Type="VI" URL="../GOGController.get data to modify.vi"/>
		<Item Name="GOGController.get library version.vi" Type="VI" URL="../GOGController.get library version.vi"/>
		<Item Name="GOGController.save configuration.vi" Type="VI" URL="../GOGController.save configuration.vi"/>
		<Item Name="GOGController.set modified data.vi" Type="VI" URL="../GOGController.set modified data.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="GOGController.autosave.vi" Type="VI" URL="../GOGController.autosave.vi"/>
		<Item Name="GOGController.get i attribute.vi" Type="VI" URL="../GOGController.get i attribute.vi"/>
		<Item Name="GOGController.GOG create all.vi" Type="VI" URL="../GOGController.GOG create all.vi"/>
		<Item Name="GOGController.GOG execute.vi" Type="VI" URL="../GOGController.GOG execute.vi"/>
		<Item Name="GOGController.GOG execute all.vi" Type="VI" URL="../GOGController.GOG execute all.vi"/>
		<Item Name="GOGController.GOG load settings.vi" Type="VI" URL="../GOGController.GOG load settings.vi"/>
		<Item Name="GOGController.set i attribute.vi" Type="VI" URL="../GOGController.set i attribute.vi"/>
		<Item Name="GOGController.ProcCases.vi" Type="VI" URL="../GOGController.ProcCases.vi"/>
		<Item Name="GOGController.ProcPeriodic.vi" Type="VI" URL="../GOGController.ProcPeriodic.vi"/>
		<Item Name="GOGController.ProcState.vi" Type="VI" URL="../GOGController.ProcState.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="GOGController.i attribute.ctl" Type="VI" URL="../GOGController.i attribute.ctl"/>
		<Item Name="GOGController.compose file name.vi" Type="VI" URL="../GOGController.compose file name.vi"/>
		<Item Name="GOGController.i attribute.vi" Type="VI" URL="../GOGController.i attribute.vi"/>
		<Item Name="GOGController.my configured entry action.vi" Type="VI" URL="../GOGController.my configured entry action.vi"/>
		<Item Name="GOGController.my initializing entry action.vi" Type="VI" URL="../GOGController.my initializing entry action.vi"/>
		<Item Name="GOGController.my preparing entry action.vi" Type="VI" URL="../GOGController.my preparing entry action.vi"/>
		<Item Name="GOGController.ProcEvents.vi" Type="VI" URL="../GOGController.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.ProcState.vi" Type="VI" URL="../inheritance/PRIVATE.ProcState.vi"/>
	</Item>
	<Item Name="GOGController.contents.vi" Type="VI" URL="../GOGController.contents.vi"/>
	<Item Name="GOGController_mapping.ini" Type="Document" URL="../GOGController_mapping.ini"/>
	<Item Name="GOGController_db.ini" Type="Document" URL="../GOGController_db.ini"/>
</Library>
