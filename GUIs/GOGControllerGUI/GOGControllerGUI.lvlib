﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This class provides methods and functionality that aid for implementing GUIs for GOGController objects in the application layer.

author: Dietrich Beck, GSI
maintainer: Dietrich Beck, GSI; d.beck@gsi.de

License Agreement for this software:

Copyright (C) 2009  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.beck@gsi.de
Last update: 08-DEC-2009

INFO2SF</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*Y!!!*Q(C=\:5RDB."%%7`]3*)@10E+^1=Q2(;&gt;+Z1MMC25Y?/36#F'`I+4DB!8=%3!&lt;(&amp;$:IX07X$,LM?E":"1,@,HPZ68@7GOW=MV@:;OF':;NX6.PB,.Y36=8#27[_/[KS"T&gt;VV4`H,G0&amp;R`TF`[8\)`^VZS6](Z?S`TD`6&gt;,8^^T_P`_'#0RSOV_PSN,_MG8]PY0@]5`EH_8ZK@^]`@&lt;SXUQ`"HWX3&lt;&amp;T:5B),T$'L&gt;V=30&gt;%40&gt;%40&gt;%$0&gt;!$0&gt;!$0&gt;!&gt;X&gt;%&gt;X&gt;%&gt;X&gt;%.X&gt;!.X&gt;!.X&gt;"L2R?[U)8/KC4&amp;EU**UC2"%AS+EK_%*_&amp;*?")?BEJY%J[%*_&amp;*?!B2QJ0Q*$Q*4],$.#5]#5`#E`!E0+3K*&amp;E\/DQ*$_E6]!1]!5`!%`"15A&amp;0!"!5#R)(3="1Y!QO!J[!*_$B5A&amp;0Q"0Q"$Q"$WY&amp;0!&amp;0Q"0Q"$R-K;M3F7&lt;I[0#12A[0Q_0Q/$Q/$[HF]$A]$I`$Y`"14A[0Q_.!/!7&gt;Z#$)G?1%/!/(R_(B2Q[0Q_0Q/$Q/$[[[1VZ8:K!:/DI]"I`"9`!90!90+74Q'$Q'D]&amp;D]*"7"I`"9`!90!90J74Q'$Q'DQ&amp;C&amp;+7]D'4'2#0)%!Q?0P7U7.WFK#27?XMURY/K&gt;A$6$J&lt;;A6%\#'I&lt;L,:R;BOCNN"K#[CW-'IXL(9D;I"KB&gt;53KA8KR0=2/W"\&lt;)&gt;NM"Z&lt;95NM-5R^ZM$4[;4D];D$Y;$^@K`&gt;&lt;K@.:K/_\\6;L&lt;2=,L69,#ZPKT@UM=X/\[5NVR^PZ`X&gt;WRPM3X^X_\,`]'\_@N$0`F_WW;@:&gt;PZZNHXV&gt;&lt;4B?N!?C2X@3``#OV%P\PXLM%&lt;@!&amp;&amp;4E3-!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">0.2.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="GOGControllerGUI.constructor.vi" Type="VI" URL="../GOGControllerGUI.constructor.vi"/>
		<Item Name="GOGControllerGUI.destructor.vi" Type="VI" URL="../GOGControllerGUI.destructor.vi"/>
		<Item Name="GOGControllerGUI.get data to modify.vi" Type="VI" URL="../GOGControllerGUI.get data to modify.vi"/>
		<Item Name="GOGControllerGUI.get library version.vi" Type="VI" URL="../GOGControllerGUI.get library version.vi"/>
		<Item Name="GOGControllerGUI.set modified data.vi" Type="VI" URL="../GOGControllerGUI.set modified data.vi"/>
		<Item Name="GOGControllerGUI.GUI handle ignored controllers.vi" Type="VI" URL="../GOGControllerGUI.GUI handle ignored controllers.vi"/>
		<Item Name="GOGControllerGUI.GUI set error.vi" Type="VI" URL="../GOGControllerGUI.GUI set error.vi"/>
		<Item Name="GOGControllerGUI.GUI set selected subcontroller.vi" Type="VI" URL="../GOGControllerGUI.GUI set selected subcontroller.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="GOGControllerGUI.apply settings.vi" Type="VI" URL="../GOGControllerGUI.apply settings.vi"/>
		<Item Name="GOGControllerGUI.get i attribute.vi" Type="VI" URL="../GOGControllerGUI.get i attribute.vi"/>
		<Item Name="GOGControllerGUI.convert status info.vi" Type="VI" URL="../GOGControllerGUI.convert status info.vi"/>
		<Item Name="GOGControllerGUI.GUI open front panel.vi" Type="VI" URL="../GOGControllerGUI.GUI open front panel.vi"/>
		<Item Name="GOGControllerGUI.GUI register controller.vi" Type="VI" URL="../GOGControllerGUI.GUI register controller.vi"/>
		<Item Name="GOGControllerGUI.GUI register device.vi" Type="VI" URL="../GOGControllerGUI.GUI register device.vi"/>
		<Item Name="GOGControllerGUI.GUI set nominal value.vi" Type="VI" URL="../GOGControllerGUI.GUI set nominal value.vi"/>
		<Item Name="GOGControllerGUI.read setting file.vi" Type="VI" URL="../GOGControllerGUI.read setting file.vi"/>
		<Item Name="GOGControllerGUI.register controller.vi" Type="VI" URL="../GOGControllerGUI.register controller.vi"/>
		<Item Name="GOGControllerGUI.register device.vi" Type="VI" URL="../GOGControllerGUI.register device.vi"/>
		<Item Name="GOGControllerGUI.set i attribute.vi" Type="VI" URL="../GOGControllerGUI.set i attribute.vi"/>
		<Item Name="GOGControllerGUI.ProcCases.vi" Type="VI" URL="../GOGControllerGUI.ProcCases.vi"/>
		<Item Name="GOGControllerGUI.ProcPeriodic.vi" Type="VI" URL="../GOGControllerGUI.ProcPeriodic.vi"/>
		<Item Name="GOGControllerGUI.update controller element.vi" Type="VI" URL="../GOGControllerGUI.update controller element.vi"/>
		<Item Name="GOGControllerGUI.update device element.vi" Type="VI" URL="../GOGControllerGUI.update device element.vi"/>
		<Item Name="GOGControllerGUI.user event get ignored controllers.vi" Type="VI" URL="../GOGControllerGUI.user event get ignored controllers.vi"/>
		<Item Name="GOGControllerGUI.user event set ignored controllers.vi" Type="VI" URL="../GOGControllerGUI.user event set ignored controllers.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="GOGControllerGUI.i attribute.ctl" Type="VI" URL="../GOGControllerGUI.i attribute.ctl"/>
		<Item Name="GOGControllerGUI.controller element.ctl" Type="VI" URL="../GOGControllerGUI.controller element.ctl"/>
		<Item Name="GOGControllerGUI.device data type.ctl" Type="VI" URL="../GOGControllerGUI.device data type.ctl"/>
		<Item Name="GOGControllerGUI.device data kind.ctl" Type="VI" URL="../GOGControllerGUI.device data kind.ctl"/>
		<Item Name="GOGControllerGUI.device element.ctl" Type="VI" URL="../GOGControllerGUI.device element.ctl"/>
		<Item Name="GOGControllerGUI.i attribute.vi" Type="VI" URL="../GOGControllerGUI.i attribute.vi"/>
		<Item Name="GOGControllerGUI.ProcEvents.vi" Type="VI" URL="../GOGControllerGUI.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="PRIVATE.panel.vit" Type="VI" URL="../inheritance/PRIVATE.panel.vit"/>
	</Item>
	<Item Name="GOGControllerGUI.contents.vi" Type="VI" URL="../GOGControllerGUI.contents.vi"/>
	<Item Name="GOGControllerGUI_mapping.ini" Type="Document" URL="../GOGControllerGUI_mapping.ini"/>
	<Item Name="GOGControllerGUI_db.ini" Type="Document" URL="../GOGControllerGUI_db.ini"/>
</Library>
