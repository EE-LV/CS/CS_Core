﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the base class for a state machine.

The BaseSM class provides a thread for running a Havel state machine. The states, entry and exit actions are defined in the method ProcState of THIS class. The ProcState method of THIS class (BaseSM or child) is called from the method "main".

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2001  Dietrich Beck, Holger Brand, Mathias Richter
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de or h.brand@gsi.de
Last update: 20-Mar-2013

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*I!!!*Q(C=\:;R;BN"%):`"2?'&amp;%&amp;P9.3Z#US68J8,A#LX]QJK6?I&amp;$"F3J:3[V(K&amp;?9/A6^!4'-\@\=X*4G*]!3=13(9^J\N`:G?_W^V&lt;,,6W+6WIGWJ[M@W+P]5-PVWT1?^+5X@O'C+\#O]?R^@!-@T:]=/FE*\[RZCKLR`KP`&lt;^`PP0$T^NGO`^&lt;[P^K`[*_8GGP&gt;Y``8FP*I0_=*.GYR??7'#/W@$F*HKC*XKC*XKC"XKA"XKA"XKA/\KD/\KD/\KD'\KB'\KB'\K."Q7[U)5O;U&gt;%5DQJF#2.%C4"I#CZ*$Q*4]+4]0#IB#@B38A3HI3(%#5]#5`#E`!E0!R4QJ0Q*$Q*4]*$KE;3L;0$E`#18A&amp;0Q"0Q"$Q"$S56]!1!1&lt;%A=:!%$!8/Y#&lt;A#8A#(GY6]!1]!5`!%`$A6M!4]!1]!5`!QZ!W+^&amp;I_IY/$WHE]$A]$I`$Y`#17A[0Q_0Q/$Q/$_8E]$A]$I24U%E/AJR"4I$TY0!Y00T)Y8&amp;Y("[(R_("V6&lt;)W]TU.(V(B]@A-8A-(I0(Y#'&amp;$"[$R_!R?!Q?UMLA-8A-(I0(Y+'5$"[$R_!R1)SCF*?2T"BI""G#Q=.@WSX76CE;C&lt;6?H_;Q565&lt;5,7R6"N'N2&amp;5#[R;/.7#K#:;.9'KC6'^M/J&amp;6)#KQKK%KE#&gt;O"[R!\&lt;(NNA;7W&amp;,&lt;)(._['`/@"U/OFY0/JQ/'C`XWO\X7K^8GOV7GGZ8'KR7'A_HZ^0KSP[U'&lt;DO&lt;4BPL&gt;0H\`O\DZ?@YBPV\O\G`?\O0WS'XW4^OZ_NLG]?.H[G#&gt;DBH0J&lt;TA&lt;^7&lt;Y6_BRDBY!D?VJU!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.30.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="BaseSM.check state.vi" Type="VI" URL="../BaseSM.check state.vi"/>
		<Item Name="BaseSM.constructor.vi" Type="VI" URL="../BaseSM.constructor.vi"/>
		<Item Name="BaseSM.destructor.vi" Type="VI" URL="../BaseSM.destructor.vi"/>
		<Item Name="BaseSM.get data to modify.vi" Type="VI" URL="../BaseSM.get data to modify.vi"/>
		<Item Name="BaseSM.get library version.vi" Type="VI" URL="../BaseSM.get library version.vi"/>
		<Item Name="BaseSM.request state change.vi" Type="VI" URL="../BaseSM.request state change.vi"/>
		<Item Name="BaseSM.set modified data.vi" Type="VI" URL="../BaseSM.set modified data.vi"/>
		<Item Name="BaseSM.set state 0.vi" Type="VI" URL="../BaseSM.set state 0.vi"/>
		<Item Name="BaseSM.stop.vi" Type="VI" URL="../BaseSM.stop.vi"/>
		<Item Name="BaseSM.thread.vi" Type="VI" URL="../BaseSM.thread.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="BaseSM.stateQuality.ctl" Type="VI" URL="../BaseSM.stateQuality.ctl"/>
		<Item Name="BaseSM.change state.vi" Type="VI" URL="../BaseSM.change state.vi"/>
		<Item Name="BaseSM.get i attribute.vi" Type="VI" URL="../BaseSM.get i attribute.vi"/>
		<Item Name="BaseSM.ProcState.vi" Type="VI" URL="../BaseSM.ProcState.vi"/>
		<Item Name="BaseSM.set i attribute.vi" Type="VI" URL="../BaseSM.set i attribute.vi"/>
		<Item Name="BaseSM.set state error.vi" Type="VI" URL="../BaseSM.set state error.vi"/>
		<Item Name="BaseSM.set state quality.vi" Type="VI" URL="../BaseSM.set state quality.vi"/>
		<Item Name="BaseSM.set state status.vi" Type="VI" URL="../BaseSM.set state status.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="BaseSM.i attribute.ctl" Type="VI" URL="../BaseSM.i attribute.ctl"/>
		<Item Name="BaseSM.i attribute.vi" Type="VI" URL="../BaseSM.i attribute.vi"/>
		<Item Name="BaseSM.call proc state.vi" Type="VI" URL="../BaseSM.call proc state.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="INHERITPRIV.ProcState.vi" Type="VI" URL="../inheritance/INHERITPRIV.ProcState.vi"/>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="PRIVATE.ProcState.vi" Type="VI" URL="../inheritance/PRIVATE.ProcState.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="BaseSM.contents.vi" Type="VI" URL="../BaseSM.contents.vi"/>
</Library>
