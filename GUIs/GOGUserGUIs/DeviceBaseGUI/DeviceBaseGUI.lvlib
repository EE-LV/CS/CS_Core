﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">A dedicated GUI for the DeviceBase class. It can be used by all classes that have inherited from the DeviceBase class. The main purpose of THIS class is avoiding duplicate code in GUI classes.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de

Last update: 21-MAR-2013

INFO2SF</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*_!!!*Q(C=\:1RDB."%%7`%5+EPA'SMUV,YA,L@#0()))+.OM%JQZ^B&lt;K#/9)4$F"8M,C"E^5[.7^['K^XR=Y!9C5#KFUT\F`&gt;67_[?U;K^F:[L&gt;/981^;D:&gt;3P&lt;&lt;3^@NO/=&gt;L;\@SU(U5@ZB@_MG$]`P,Y`L8Z^O4_90]9[:"_R``P@B]0H`"?#HT?3H0RDF9H:&gt;HY[&gt;4K@[H=?QYE"]\(I]$`/0RJT9?(T`?[`'8Y'6.GF2300(!(&lt;@[&gt;+&gt;%4`2%4`2%4`2!$`2!$`2!$X2(&gt;X2(&gt;X2(&gt;X2$.X2$.X2$.`4;U)5O&gt;+'T+EHRJ&amp;#3.%G1$!:&amp;S38B38A3HI3(LB+?B#@B38A3(I9IY5FY%J[%*_&amp;BGB+?B#@B38A3(F*6EKQ.(:[%B`1+?!+?A#@A#8AIK9!H!!C+"9G$*'!I#!:`!J[!*_$BLQ+?A#@A#8A#(M)+?!+?A#@A#8C95F=F+EX8U/%BD2Q?B]@B=8A=(F,,Y8&amp;Y("[(R_'BH"Q?B]?"=!I[S5'1-]E:Y(1=(I?(GRQ?B]@B=8A=(E*VB\SO4%@4.82Y$"[$R_!R?!Q?5MDA-8A-(I0(Y#'N$"[$R_!R?!Q?3MHA-8A-(A0%+%JZ'=G-C=9A1T"Y_.849H78IJ*9&lt;?X6\!_KWA&amp;5/VBK"U&lt;N)+BNM.L'K7W)WE+L,;$;QKA^M.K$K!'K&amp;6:,K$:1"[Z\@)&gt;P]1W_QJ@Y!J`BUW\K8RZY/"SUX__VW_WUX7[VW7SU7KWU8#[V7#QUG]UUH5\08[NXN.YGF^_FO[P0.`?XWW`X6Z_Y@`FQ^`\WZE&gt;MT&gt;B@]MH8#V`XWJO0P@^E@0^&gt;_B?_D8KFU]6HHD8[$HRFCIU!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.14.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DeviceBaseGUI.constructor.vi" Type="VI" URL="../DeviceBaseGUI.constructor.vi"/>
		<Item Name="DeviceBaseGUI.destructor.vi" Type="VI" URL="../DeviceBaseGUI.destructor.vi"/>
		<Item Name="DeviceBaseGUI.get library version.vi" Type="VI" URL="../DeviceBaseGUI.get library version.vi"/>
		<Item Name="DeviceBaseGUI.panel.vi" Type="VI" URL="../DeviceBaseGUI.panel.vi"/>
		<Item Name="DeviceBaseGUI.set channel value.vi" Type="VI" URL="../DeviceBaseGUI.set channel value.vi"/>
		<Item Name="DeviceBaseGUI.set value.vi" Type="VI" URL="../DeviceBaseGUI.set value.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DeviceBaseGUI.close panel.vi" Type="VI" URL="../DeviceBaseGUI.close panel.vi"/>
		<Item Name="DeviceBaseGUI.get i attribute.vi" Type="VI" URL="../DeviceBaseGUI.get i attribute.vi"/>
		<Item Name="DeviceBaseGUI.get GOG object name.vi" Type="VI" URL="../DeviceBaseGUI.get GOG object name.vi"/>
		<Item Name="DeviceBaseGUI.get method parameters from GOG.vi" Type="VI" URL="../DeviceBaseGUI.get method parameters from GOG.vi"/>
		<Item Name="DeviceBaseGUI.get object name.vi" Type="VI" URL="../DeviceBaseGUI.get object name.vi"/>
		<Item Name="DeviceBaseGUI.register to GOG.vi" Type="VI" URL="../DeviceBaseGUI.register to GOG.vi"/>
		<Item Name="DeviceBaseGUI.set i attribute.vi" Type="VI" URL="../DeviceBaseGUI.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="DeviceBaseGUI.i attribute.ctl" Type="VI" URL="../DeviceBaseGUI.i attribute.ctl"/>
		<Item Name="DeviceBaseGUI.i attribute.vi" Type="VI" URL="../DeviceBaseGUI.i attribute.vi"/>
		<Item Name="DeviceBaseGUI.construct setting file path.vi" Type="VI" URL="../DeviceBaseGUI.construct setting file path.vi"/>
		<Item Name="DeviceBaseGUI.load settings.vi" Type="VI" URL="../DeviceBaseGUI.load settings.vi"/>
		<Item Name="DeviceBaseGUI.save settings.vi" Type="VI" URL="../DeviceBaseGUI.save settings.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.panel.vi" Type="VI" URL="../inheritance/PRIVATE.panel.vi"/>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="INHERITPRIV.panel.vi" Type="VI" URL="../inheritance/INHERITPRIV.panel.vi"/>
	</Item>
	<Item Name="DeviceBaseGUI.contents.vi" Type="VI" URL="../DeviceBaseGUI.contents.vi"/>
	<Item Name="DeviceBaseGUI.rtm" Type="Document" URL="../DeviceBaseGUI.rtm"/>
</Library>
