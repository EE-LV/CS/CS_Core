﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">A dedicated GUI for the MotorBase class. It can be used by all classes that have inherited from the MotorBase class.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de

Last update: 21-MAR-2013

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+&amp;!!!*Q(C=Z:3RDN.!%)&lt;`H,#Y$E7]!+3FH*+')K_16ZB83*MS^6UV5BI[Z+MI42I?9&amp;YBLZ#;THR?&lt;U)Y8=Y3!OEE&gt;D//`=`/T/@&gt;^5KFX5KPV%_VZNG'P`4_:)07H*_&lt;EV\%JOH0P4T_CD`Z,W,(8)`]&amp;^(^I`LH_!O?:JJ`KOH:^D`ZO_ZU?&gt;,@&gt;8UX8K\ZOWYX8+\HX_VWV`.,O^*_7Z-`]%`FP_Z`ILV]``4WXER`"0_W3&lt;.#CC57G'.7XKZ0^%20^%20^%10^%!0^%!0^%"X&gt;%&gt;X&gt;%&gt;X&gt;%=X&gt;%-X&gt;%-X&gt;%-P(6XI1B=[MZ)54QIF3:-%S7"1F&amp;Q3HI1HY5FY?&amp;4#E`!E0!F0QM-1*4Q*4]+4]#1]B#HB38A3HI1HY3&amp;6)=H3U?&amp;*?%CPA#@A#8A#HI#(EAJY!I#A7*!Y3!+'!G&gt;Q%`!%0!%0NQJY!J[!*_!*?(!LY!FY!J[!*_!BJ-R+&amp;*KBI].$'DE]$I`$Y`!Y0+37Q_0Q/$Q/D].$/4E]$I]$Y22UEI-A*]A:Y$QY0!Y0@X*Y("[(R_&amp;R?(#6&amp;@)S-Q0.U.(B-8A-(I0(Y$&amp;Y3#'$R_!R?!Q?AY?U-HA-(I0(Y$&amp;Y+#7$R_!R?!Q1ISDF:31T!IV"BG$Q]#O\R=IK23'RUOOH/7Z5V1WIOL&amp;5.YTK2F"&gt;9.7&amp;5VU1V9F7H5$6C6&amp;^9&gt;585165,;S;5(7ADFQ0W"ZLM3WWRF&lt;9%FNA]S(U,Q]](I][(!\;\`&gt;KWV&lt;&lt;\6&lt;L^6KLV5L,Z6+,R5,T_@R]7LWDDWVW/J=WX$`MXX&lt;@\NLX\@X(L_X&gt;F]]0^R_[14`Z*_XWUWRTQ`^L\-W0;N^(OR)THEMPY7T5D@K,9ZYZ_AEMJ*?`!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.14.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="MotorBaseGUI.constructor.vi" Type="VI" URL="../MotorBaseGUI.constructor.vi"/>
		<Item Name="MotorBaseGUI.destructor.vi" Type="VI" URL="../MotorBaseGUI.destructor.vi"/>
		<Item Name="MotorBaseGUI.EmergencyOff.vi" Type="VI" URL="../MotorBaseGUI.EmergencyOff.vi"/>
		<Item Name="MotorBaseGUI.get library version.vi" Type="VI" URL="../MotorBaseGUI.get library version.vi"/>
		<Item Name="MotorBaseGUI.panel.vi" Type="VI" URL="../MotorBaseGUI.panel.vi"/>
		<Item Name="MotorBaseGUI.stop move.vi" Type="VI" URL="../MotorBaseGUI.stop move.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="MotorBaseGUI.get i attribute.vi" Type="VI" URL="../MotorBaseGUI.get i attribute.vi"/>
		<Item Name="MotorBaseGUI.set i attribute.vi" Type="VI" URL="../MotorBaseGUI.set i attribute.vi"/>
		<Item Name="MotorBaseGUI.update listbox.vi" Type="VI" URL="../MotorBaseGUI.update listbox.vi"/>
		<Item Name="MotorBaseGUI.update simple listbox.vi" Type="VI" URL="../MotorBaseGUI.update simple listbox.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="MotorBaseGUI.i attribute.ctl" Type="VI" URL="../MotorBaseGUI.i attribute.ctl"/>
		<Item Name="MotorBaseGUI.i attribute.vi" Type="VI" URL="../MotorBaseGUI.i attribute.vi"/>
		<Item Name="MotorBaseGUI.calc GUI colors.vi" Type="VI" URL="../MotorBaseGUI.calc GUI colors.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.panel.vi" Type="VI" URL="../inheritance/PRIVATE.panel.vi"/>
	</Item>
	<Item Name="MotorBaseGUI.contents.vi" Type="VI" URL="../MotorBaseGUI.contents.vi"/>
</Library>
