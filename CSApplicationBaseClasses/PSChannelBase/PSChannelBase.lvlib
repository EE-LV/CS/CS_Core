﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the base class for one or more power supply channels. It provides basic events, some example methods and publishes a lot of services "OBJECTNAME_SERVICENAME". This class can be used as parent class for classes that implemented a specific power supply channel.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de

Last update: 21-MAR-2013

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+R!!!*Q(C=Z:3R&lt;B."%):`IR15./YJAJ-;I5R$%9ECLJX+.2,&amp;&amp;(2O=/O#QK]QLW!%,_"8G&amp;?Q[%"+Y3&gt;!/L\&lt;7R-(W7&gt;&amp;!AG*8=`&gt;XD]\-^`NLE]K\;FUJO:5O_FNL8]W;[V=SPBG&lt;VD]J&gt;^5W`8S?/_@V4EVVY0Y-H0W),Y_VPC(^&lt;N]^`(^`+?;?NN`Z0_]VQ\[&gt;7$YO\^J*DX_^G(3^-6L=DS_$&amp;NX=TS_B0@EHRTH0XAI(P0_O`(&amp;R4&amp;`U\G0_PPD$_`0[?/^/0UH_,N.'B23,,(!(,0S&gt;EWC*XKC*XKC*XKA"XKA"XKA"\KD/\KD/\KD/\KB'\KB'\KB7XPGWIYO&gt;+(,SDF-CC?&amp;EK2*AG1S+%IO#5`#E`!E0$QKY5FY%J[%*_&amp;BCB+?B#@B38A3(M+5]#1]#5`#E`#1KJ"E[?DQ*$SE6]!4]!1]!5`!1UE&amp;0!&amp;!5#R)(#1"1Y%T'!1]!5`!QV!"4]!4]!1]!1^O"4Q"4]!4]!1]B*26C5,4&gt;H2Y3#/(R_&amp;R?"Q?BY@5=HA=(I@(Y8&amp;Y+#?(R_&amp;R)*S#4H)1Z!1Z%ZQ(B]@BY3;(R_&amp;R?"Q?BQ&gt;8W3%P+^03N"U&gt;(I0(Y$&amp;Y$"[$BR1S?!Q?A]@A-8B)+Y0(Y$&amp;Y$"[$BV)S?!Q?A]=!-9J38E9S)^#9:!A'$\^S7KTM5B13+\X_.&lt;O$KHI!61_7[I&amp;201CK'[S[=;I&lt;ILL1KAOIOD#K,[T[)KK!KI66%[J/V*&lt;L"FND+WS*T&lt;%J.M:'W,!.`=-4N^ON.JO.VOOV6KO6FMOFZP/ZJN/JRO/R2K/2BM0BL[`6/&lt;VLA`XPUL?L$\&gt;XW^88OZ@P&lt;L^`_@2W?`8_&gt;O&gt;&lt;-,@8XPQ9,-\0"IM8(Q?,S]NO`/J:&gt;W^NZX`^@,#YPO\O;.VX[6`Y.OK*GLX00'PU%S'(L2A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.14.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PSChannelBase.constructor.vi" Type="VI" URL="../PSChannelBase.constructor.vi"/>
		<Item Name="PSChannelBase.destructor.vi" Type="VI" URL="../PSChannelBase.destructor.vi"/>
		<Item Name="PSChannelBase.get data to modify.vi" Type="VI" URL="../PSChannelBase.get data to modify.vi"/>
		<Item Name="PSChannelBase.set modified data.vi" Type="VI" URL="../PSChannelBase.set modified data.vi"/>
		<Item Name="PSChannelBase.get channel current.vi" Type="VI" URL="../PSChannelBase.get channel current.vi"/>
		<Item Name="PSChannelBase.get channel names.vi" Type="VI" URL="../PSChannelBase.get channel names.vi"/>
		<Item Name="PSChannelBase.get channel on off.vi" Type="VI" URL="../PSChannelBase.get channel on off.vi"/>
		<Item Name="PSChannelBase.get channel ramp down speed.vi" Type="VI" URL="../PSChannelBase.get channel ramp down speed.vi"/>
		<Item Name="PSChannelBase.get channel ramp up speed.vi" Type="VI" URL="../PSChannelBase.get channel ramp up speed.vi"/>
		<Item Name="PSChannelBase.get channel status.vi" Type="VI" URL="../PSChannelBase.get channel status.vi"/>
		<Item Name="PSChannelBase.get channel trip current.vi" Type="VI" URL="../PSChannelBase.get channel trip current.vi"/>
		<Item Name="PSChannelBase.get channel voltage.vi" Type="VI" URL="../PSChannelBase.get channel voltage.vi"/>
		<Item Name="PSChannelBase.get default channel index.vi" Type="VI" URL="../PSChannelBase.get default channel index.vi"/>
		<Item Name="PSChannelBase.get library version.vi" Type="VI" URL="../PSChannelBase.get library version.vi"/>
		<Item Name="PSChannelBase.get nof channels.vi" Type="VI" URL="../PSChannelBase.get nof channels.vi"/>
		<Item Name="PSChannelBase.set default channel index.vi" Type="VI" URL="../PSChannelBase.set default channel index.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PSChannelBase.myEventNames.ctl" Type="VI" URL="../PSChannelBase.myEventNames.ctl"/>
		<Item Name="PSChannelBase.serviceType.ctl" Type="VI" URL="../PSChannelBase.serviceType.ctl"/>
		<Item Name="PSChannelBase.directionType.ctl" Type="VI" URL="../PSChannelBase.directionType.ctl"/>
		<Item Name="PSChannelBase.statusType.ctl" Type="VI" URL="../PSChannelBase.statusType.ctl"/>
		<Item Name="PSChannelBase.get i attribute.vi" Type="VI" URL="../PSChannelBase.get i attribute.vi"/>
		<Item Name="PSChannelBase.set i attribute.vi" Type="VI" URL="../PSChannelBase.set i attribute.vi"/>
		<Item Name="PSChannelBase.get status bit.vi" Type="VI" URL="../PSChannelBase.get status bit.vi"/>
		<Item Name="PSChannelBase.set status bit.vi" Type="VI" URL="../PSChannelBase.set status bit.vi"/>
		<Item Name="PSChannelBase.ProcCases.vi" Type="VI" URL="../PSChannelBase.ProcCases.vi"/>
		<Item Name="PSChannelBase.ProcPeriodic.vi" Type="VI" URL="../PSChannelBase.ProcPeriodic.vi"/>
		<Item Name="PSChannelBase.PS status 2 common status .vi" Type="VI" URL="../PSChannelBase.PS status 2 common status .vi"/>
		<Item Name="PSChannelBase.add services.vi" Type="VI" URL="../PSChannelBase.add services.vi"/>
		<Item Name="PSChannelBase.channel index in range.vi" Type="VI" URL="../PSChannelBase.channel index in range.vi"/>
		<Item Name="PSChannelBase.delete events.vi" Type="VI" URL="../PSChannelBase.delete events.vi"/>
		<Item Name="PSChannelBase.update services and attribute data.vi" Type="VI" URL="../PSChannelBase.update services and attribute data.vi"/>
		<Item Name="PSChannelBase.update_mass services and attribute data.vi" Type="VI" URL="../PSChannelBase.update_mass services and attribute data.vi"/>
		<Item Name="PSChannelBase.remove services.vi" Type="VI" URL="../PSChannelBase.remove services.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="PSChannelBase.i attribute.ctl" Type="VI" URL="../PSChannelBase.i attribute.ctl"/>
		<Item Name="PSChannelBase.parameters.ctl" Type="VI" URL="../PSChannelBase.parameters.ctl"/>
		<Item Name="PSChannelBase.i attribute.vi" Type="VI" URL="../PSChannelBase.i attribute.vi"/>
		<Item Name="PSChannelBase.ProcEvents.vi" Type="VI" URL="../PSChannelBase.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="INHERITPRIV.ProcCases.vi" Type="VI" URL="../inheritance/INHERITPRIV.ProcCases.vi"/>
	</Item>
	<Item Name="PSChannelBase.contents.vi" Type="VI" URL="../PSChannelBase.contents.vi"/>
</Library>
