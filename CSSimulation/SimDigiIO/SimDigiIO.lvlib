﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This class simulates a digital I/O device. It may serve as an example for a class for a real device.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: M.Richter@gsi.de, H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 17-JUN-2004

INFO2SF</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*U!!!*Q(C=\:1RLN.!%):`IW&gt;"G&gt;Q!J;3&gt;+_1+O=,5&gt;'F4ZAJ4)V%Y.]#.[]?=!#E8I)AI[=,HT1:#(C^_1A^"Q7T'M@`:H@G]OV[JW#PJ4M=J'WY;]&lt;9&gt;;GO(9@S.^U.Z&lt;EP]@(^OQTE_N$_0PYCX0_,$&gt;&lt;R^&lt;0SP[U`Q4ZFOWP`Y=]9@4PF6@$;&lt;X9Y@`WI=/ABHDY_@?,_P*`PN_%.\SP&lt;?4(]%@^;EJJ$CC1@OO*7X/S:[IC&gt;[IC&gt;[IA&gt;[I!&gt;[I!&gt;[I$O[ITO[ITO[IRO[I2O[I2O[I:?',H3B#ZV:39IHB:+E39+E-SB+,AF0QJ0Q*$Q]+O&amp;*?"+?B#@BI9M3HI1HY5FY%B['+?&amp;*?"+?B#@B)65BS&gt;,1Y5FY3+_!*_!*?!+?A)?3#HA#A+"9E$B)!I;#9(!4]!1]!1_X#HA#HI!HY!FY##PA#8A#HI!HY'&amp;)G:5I.'.$BY=U=HA=(I@(Y8&amp;Y3#W(R_&amp;R?"Q?BY&gt;S=HA=(A@#+?AE"U(/)+?$]_$Q/$T]S?&amp;R?"Q?B]@B)627S-P-D$2D1Y@(Y$&amp;Y$"[$R_!BB1Q?A]@A-8A-(N,+Y$&amp;Y$"[$R_#BF!Q?A]@A-5#-IJ38E=Q9;(1S")/(8^EN6F9J#IG66D`.UU:6X9#K'UNVQ[BO".5&amp;6FUYV162H7D6#63&gt;'.586HU265$6QKI*64PKQ(70^XC(&lt;`%VPM+8_!+@DU/@O?0B=."_PV@@^_K[4NPN6OPV7KP63MPF5IP&amp;1P0Z`0NJ^:JWMO&lt;S8,J```(&gt;JT@^\L\\M0P=\8&lt;D]TGWI?_4`?78:H0XNNGU6^\5W%8@U\HU,ZS.?K(DR4(0((U$9+D0/!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.12.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SimDigiIO.constructor.vi" Type="VI" URL="../SimDigiIO.constructor.vi"/>
		<Item Name="SimDigiIO.destructor.vi" Type="VI" URL="../SimDigiIO.destructor.vi"/>
		<Item Name="SimDigiIO.get data to modify.vi" Type="VI" URL="../SimDigiIO.get data to modify.vi"/>
		<Item Name="SimDigiIO.get library version.vi" Type="VI" URL="../SimDigiIO.get library version.vi"/>
		<Item Name="SimDigiIO.set modified data.vi" Type="VI" URL="../SimDigiIO.set modified data.vi"/>
		<Item Name="SimDigiIO.set bit.vi" Type="VI" URL="../SimDigiIO.set bit.vi"/>
		<Item Name="SimDigiIO.set word.vi" Type="VI" URL="../SimDigiIO.set word.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SimDigiIO.get i attribute.vi" Type="VI" URL="../SimDigiIO.get i attribute.vi"/>
		<Item Name="SimDigiIO.set i attribute.vi" Type="VI" URL="../SimDigiIO.set i attribute.vi"/>
		<Item Name="SimDigiIO.ProcCases.vi" Type="VI" URL="../SimDigiIO.ProcCases.vi"/>
		<Item Name="SimDigiIO.ProcPeriodic.vi" Type="VI" URL="../SimDigiIO.ProcPeriodic.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="SimDigiIO.i attribute.ctl" Type="VI" URL="../SimDigiIO.i attribute.ctl"/>
		<Item Name="SimDigiIO.i attribute.vi" Type="VI" URL="../SimDigiIO.i attribute.vi"/>
		<Item Name="SimDigiIO.ProcEvents.vi" Type="VI" URL="../SimDigiIO.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="SimDigiIO.contents.vi" Type="VI" URL="../SimDigiIO.contents.vi"/>
	<Item Name="SimDigiIO_db.ini" Type="Document" URL="../SimDigiIO_db.ini"/>
</Library>
