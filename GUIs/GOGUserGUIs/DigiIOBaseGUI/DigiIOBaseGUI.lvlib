﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">A dedicated GUI for the DigiIOBase class. It can be used by all classes that have inherited from the DigiIOBase class.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI

License Agreement for this software:

Copyright (C) 2006  Dietrich Beck
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidher@gsi.de

Last update: 21-MAR-2013

INFO2SF</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+9!!!*Q(C=Z:3REB*"%):`^#QP8.\!YB8[%9[=C&amp;D,I!-D.Z'5E&amp;@I6VD@1",DKXY&amp;SOQ#!ML1$,]&gt;BB-/Y;YMN;RSBG:W`Z\O`H:G&gt;K83LK5L&lt;2^L.R&gt;&lt;\W^&lt;L!RNO&gt;\@N(N`\4_OSFV\]]#`DW^XQ3@R$[,&lt;=`8L?"^`G@_RJIPNP`)X2]/*PWG;A_(%XT4&lt;XF/(H]@X#XYOHAH&lt;-O6=@?[9=&lt;RF@^&gt;@HKWZQ(&gt;SJ)\^XX&lt;NF`WH\3H(?`\Y3`"HGT1IJ&amp;BCA4FGZ?GWC:\IC:\IC:\IA2\IA2\IA2\IDO\IDO\IDO\IBG\IBG\IBG\IJ;-,8?B#:V73YEGB*'G3)*E-CJ+`B#@B38A3(G[6]#1]#5`#E`!Q21F0QJ0Q*$Q*$W&amp;+?"+?B#@B38B)65CS&gt;(2Y%B\3+_!*?!+?A#@AI;1#HA!A+"9E$J+!I=!:8!1]!5`!Q[5#HI!HY!FY!B\=#HA#HI!HY!FY##GL%I7G\_DQE%9/D]0D]$A]$A_JZ@!Y0![0Q_0Q5%Y/D]0D1$A&amp;H?1AS!FS*DAX$I`$QS#(R_&amp;R?"Q?BQ&gt;8W3%P+^04^"U&gt;(I0(Y$&amp;Y$"[$BR1S?!Q?A]@A-8B)+Y0(Y$&amp;Y$"[$BV)S?!Q?A]=!-9J38E9S)^#9:!A'$\^S7KTM5B13+\W_GLO$KHI!61_7[I&amp;201CK'[S[=;I&lt;ILL1KAOIOD#K$[T[)+K!KI66%[J/V)&lt;`&amp;&lt;&lt;%/GS"T&lt;!J.M:'W,!0`=U4.ZO.6KO6FMOFOK\49L(1&lt;$&lt;4&gt;$L6?$T7;$43=$C]`VK^IO`;Y0#\&gt;(@\9&lt;,_V(V:&gt;W]H[_\D[\P&lt;&gt;Z/^&lt;]\=*^FV-ZA``TS98\U:T&amp;_]0\9"`J&gt;@D_&lt;PPEP`QL&gt;2T\1^_-ST2N]"&amp;+?FW1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.14.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DigiIOBaseGUI.constructor.vi" Type="VI" URL="../DigiIOBaseGUI.constructor.vi"/>
		<Item Name="DigiIOBaseGUI.destructor.vi" Type="VI" URL="../DigiIOBaseGUI.destructor.vi"/>
		<Item Name="DigiIOBaseGUI.get library version.vi" Type="VI" URL="../DigiIOBaseGUI.get library version.vi"/>
		<Item Name="DigiIOBaseGUI.panel.vi" Type="VI" URL="../DigiIOBaseGUI.panel.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="DigiIOBaseGUI.get i attribute.vi" Type="VI" URL="../DigiIOBaseGUI.get i attribute.vi"/>
		<Item Name="DigiIOBaseGUI.set i attribute.vi" Type="VI" URL="../DigiIOBaseGUI.set i attribute.vi"/>
		<Item Name="DigiIOBaseGUI.update listbox.vi" Type="VI" URL="../DigiIOBaseGUI.update listbox.vi"/>
		<Item Name="DigiIOBaseGUI.update simple listbox.vi" Type="VI" URL="../DigiIOBaseGUI.update simple listbox.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="DigiIOBaseGUI.i attribute.ctl" Type="VI" URL="../DigiIOBaseGUI.i attribute.ctl"/>
		<Item Name="DigiIOBaseGUI.i attribute.vi" Type="VI" URL="../DigiIOBaseGUI.i attribute.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.panel.vi" Type="VI" URL="../inheritance/PRIVATE.panel.vi"/>
	</Item>
	<Item Name="DigiIOBaseGUI.contents.vi" Type="VI" URL="../DigiIOBaseGUI.contents.vi"/>
</Library>
