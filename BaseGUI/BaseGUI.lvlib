﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the base class for a GUI class. It provides methods to start a GUI. By executing the constructor, a front panel will be created from VI template (*.vit). That template can have any name that must be passed to the "data" control of the constructor of THIS class (data type string). The VI template must provide a control named "object ref". During instantiation, "object ref" will be set to the reference to the object.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2001  Dietrich Beck, Holger Brand, Mathias Richter
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de or h.brand@gsi.de
Last update: 20-Mar-2013

INFO2SF</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*A!!!*Q(C=\:9RDB."%%7`%=&amp;+*0A%I!EXL3MY)E2S2&amp;Z8=/L1&amp;VC*9K.*@16@I7[Q]B5=EZEX047,T;)&gt;J!5*#&lt;J&gt;^MSP[KIXX4UN3[X&gt;3+^VHGN[NPW+P]7-P_?[(`PE@_SKS"`]T=\D_+PQ[`'F0@60-2@VTR@_FT\@@`^U`8408&amp;W`K@;P_G@GZW@NZ@\ZVXM\'`3(G\39XP$%!H0-RD=XU2-^U2-^U2-^U!-^U!-^U!0&gt;U2X&gt;U2X&gt;U2X&gt;U!X&gt;U!X&gt;U'U[,N#&amp;,H2:/RK3YEGB*'G3)!E'2=F8QJ0Q*$Q*$\&gt;+?"+?B#@B38A)5=+4]#1]#5`#QT!F0!F0QJ0Q*$SE;C4:/DI]#1`J&amp;@!%0!&amp;0Q"0Q5&amp;)"4Q!1&amp;!M3"UH!5/!-,A+?A#@AY6)"4]!4]!1]!1^O"4Q"4]!4]!1]$'GT%IVG[/DQE%9/D]0D]$A]$A_JZ@!Y0![0Q_0Q5%Y/D]0D1$A&amp;H?1AS"HE"$AX$I`$QY]=(I@(Y8&amp;Y("Z=&lt;97]T=R!-X2U?!Q?A]@A-8A-(F,)Y$&amp;Y$"[$R_!BL1Q?A]@A-8A-(EL*Y$&amp;Y$"Y$R#B+?2H*D)&amp;'E#%90(T;&lt;L'W3N&amp;)L06[.=?.KNK!KIWFWD#KD;";9.8#K2:%.&gt;'K#62.D/K"61_C!F166C65"?L%^R%\9(NMBWWQ.&lt;&lt;#/GQZ$0X.A;@43=@D59@$1@P^8LP&gt;4JP.2OPV7KP63FX8;&lt;F=0JZ7\_FD7UTHUJ&lt;LQ4\@@_HP0H:^0.TW&gt;R^O_`BUXU__78PX&gt;&lt;&amp;^?`/]$4%89]:T[7]Y'`6K`.PS@9[_!;UY:WI!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.30.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="BaseGUI.constructor.vi" Type="VI" URL="../BaseGUI.constructor.vi"/>
		<Item Name="BaseGUI.destructor.vi" Type="VI" URL="../BaseGUI.destructor.vi"/>
		<Item Name="BaseGUI.get data to modify.vi" Type="VI" URL="../BaseGUI.get data to modify.vi"/>
		<Item Name="BaseGUI.get library version.vi" Type="VI" URL="../BaseGUI.get library version.vi"/>
		<Item Name="BaseGUI.set modified data.vi" Type="VI" URL="../BaseGUI.set modified data.vi"/>
		<Item Name="BaseGUI.thread_example.vi" Type="VI" URL="../BaseGUI.thread_example.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="BaseGUI.get i attribute.vi" Type="VI" URL="../BaseGUI.get i attribute.vi"/>
		<Item Name="BaseGUI.set front panel title.vi" Type="VI" URL="../BaseGUI.set front panel title.vi"/>
		<Item Name="BaseGUI.set i attribute.vi" Type="VI" URL="../BaseGUI.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="BaseGUI.i attribute.ctl" Type="VI" URL="../BaseGUI.i attribute.ctl"/>
		<Item Name="BaseGUI.i attribute.vi" Type="VI" URL="../BaseGUI.i attribute.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="INHERITPRIV.panel.vi" Type="VI" URL="../inheritance/INHERITPRIV.panel.vi"/>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="PRIVATE.panel.vi" Type="VI" URL="../inheritance/PRIVATE.panel.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="BaseGUI.contents.vi" Type="VI" URL="../BaseGUI.contents.vi"/>
</Library>
