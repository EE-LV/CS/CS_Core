﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">The CS own implementation of a concurrent active event object.

This class provides the following:

- methods for event driven communication (EvtAwait, SendCmd, ...)
- an active thread required for the CS access system

In case unbuffered message events (notifiers) are defined, an object of the NotifierObj class is created for handling these events. The communication between the NotifierObj and the CAEObj is done via buffered message events and direct method calls.

This class may serve a parent class for active objects that do not require the funtionality of the BaseProcess class.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2001  Dietrich Beck, Holger Brand, Mathias Richter
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de or h.brand@gsi.de
Last update: 20-Mar-2013

INFO2SF
</Property>
	<Property Name="NI.Lib.FriendGUID" Type="Str">f092c808-0319-4255-8bb4-87b9e66ded3e</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*I!!!*Q(C=\:9^DB."%%;`22N!/.JE1O31N#*S(W!4:]2V";=&lt;'AG#$5PC!JAD_!JV!1*@Q6?9@&gt;/O-;S20)P!%A(&gt;LPHZKLLK45^0SV*LL[6&lt;$8..&amp;^O,`/.B`&amp;8Q5-IQH&amp;T9I%IVH'Z_^H.2];U0/P0`/H[9`$]'0;OP#?W0H__``_8_`L+`\`N,`LY`#XDO\`PTA.`TT_7@Z:N^PP.W@@`]Z`UQ'X4F*NV-8XBCA4FGR[]\U2-^U2-^U2-^U!-^U!-^U!0&gt;U2X&gt;U2X&gt;U2X&gt;U!X&gt;U!X&gt;U'X;,N#&amp;,H2:WRG3YEGB*'G3)!E'2=EBY5FY%J[%BVMF0!F0QJ0Q*$S%+/&amp;*?"+?B#@B9:A3HI1HY5FY%BZ3.:*M(2W?B)@U#HA#HI!HY!FY++G!*Q!)CA7*AS2A+(!'&amp;Q&amp;0Q"0Q=+G!*_!*?!+?A!?X!J[!*_!*?!)?BL2:C59T&gt;H2Y3#/(R_&amp;R?"Q?BY@5=HA=(I@(Y8&amp;Y+#?(R_&amp;R)*S#4H)1Z!RS!JQ&lt;B]@BY33(R_&amp;R?"Q?BQ&gt;8?U0?:G;E'4M[0!;0Q70Q'$Q'$SFE]"A]"I`"9`#16A;0Q70Q'$Q'$[6E]"A]"I]"9B3FP)RERE!DS"!-(HZNN6B\3^&amp;)L08[.)],6&lt;5!61N,N7"5#U(VAF5P4P6#6"/NGE$6R+A?705A+E"6966#6;!/(0@9$NNC'WS.L&lt;!FNM#[=?B@$DQ=$NLP^^LN&gt;NJON^JM.FKPVVKN6FIOFVIM&amp;OK[\L2&lt;P;5@W]WU,TVQ0&gt;KH,X@P(_`@@8T]`OU.Z[_@0^RVE_];&gt;NS8`I7^5;^/`VRKDJY!E,!WHA!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.30.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CAEObj.constructor.vi" Type="VI" URL="../CAEObj.constructor.vi"/>
		<Item Name="CAEObj.destructor.vi" Type="VI" URL="../CAEObj.destructor.vi"/>
		<Item Name="CAEObj.evt status.vi" Type="VI" URL="../CAEObj.evt status.vi"/>
		<Item Name="CAEObj.get data to modify.vi" Type="VI" URL="../CAEObj.get data to modify.vi"/>
		<Item Name="CAEObj.get library version.vi" Type="VI" URL="../CAEObj.get library version.vi"/>
		<Item Name="CAEObj.is active.vi" Type="VI" URL="../CAEObj.is active.vi"/>
		<Item Name="CAEObj.send command.vi" Type="VI" URL="../CAEObj.send command.vi"/>
		<Item Name="CAEObj.set modified data.vi" Type="VI" URL="../CAEObj.set modified data.vi"/>
		<Item Name="CAEObj.start.vi" Type="VI" URL="../CAEObj.start.vi"/>
		<Item Name="CAEObj.stop.vi" Type="VI" URL="../CAEObj.stop.vi"/>
		<Item Name="CAEObj.thread.vi" Type="VI" URL="../CAEObj.thread.vi"/>
		<Item Name="CAEObj.thread_example.vi" Type="VI" URL="../CAEObj.thread_example.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CAEObj.analyze URL.vi" Type="VI" URL="../CAEObj.analyze URL.vi"/>
		<Item Name="CAEObj.event definition.ctl" Type="VI" URL="../CAEObj.event definition.ctl"/>
		<Item Name="CAEObj.evt await.vi" Type="VI" URL="../CAEObj.evt await.vi"/>
		<Item Name="CAEObj.evt create lvnot.vi" Type="VI" URL="../CAEObj.evt create lvnot.vi"/>
		<Item Name="CAEObj.evt create msg.vi" Type="VI" URL="../CAEObj.evt create msg.vi"/>
		<Item Name="CAEObj.evt create trend msg.vi" Type="VI" URL="../CAEObj.evt create trend msg.vi"/>
		<Item Name="CAEObj.evt create trend not.vi" Type="VI" URL="../CAEObj.evt create trend not.vi"/>
		<Item Name="CAEObj.evt create.vi" Type="VI" URL="../CAEObj.evt create.vi"/>
		<Item Name="CAEObj.evt delete.vi" Type="VI" URL="../CAEObj.evt delete.vi"/>
		<Item Name="CAEObj.evt enable.vi" Type="VI" URL="../CAEObj.evt enable.vi"/>
		<Item Name="CAEObj.evt replace.vi" Type="VI" URL="../CAEObj.evt replace.vi"/>
		<Item Name="CAEObj.get evt loop counter.vi" Type="VI" URL="../CAEObj.get evt loop counter.vi"/>
		<Item Name="CAEObj.get i attribute.vi" Type="VI" URL="../CAEObj.get i attribute.vi"/>
		<Item Name="CAEObj.get notifier object.vi" Type="VI" URL="../CAEObj.get notifier object.vi"/>
		<Item Name="CAEObj.get queue refnum.vi" Type="VI" URL="../CAEObj.get queue refnum.vi"/>
		<Item Name="CAEObj.get stop.vi" Type="VI" URL="../CAEObj.get stop.vi"/>
		<Item Name="CAEObj.i loop attribute.vi" Type="VI" URL="../CAEObj.i loop attribute.vi"/>
		<Item Name="CAEObj.set i attribute.vi" Type="VI" URL="../CAEObj.set i attribute.vi"/>
		<Item Name="CAEObj.unfold trend data.vi" Type="VI" URL="../CAEObj.unfold trend data.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="CAEObj.evt add.vi" Type="VI" URL="../CAEObj.evt add.vi"/>
		<Item Name="CAEObj.evt create notification.vi" Type="VI" URL="../CAEObj.evt create notification.vi"/>
		<Item Name="CAEObj.evt remove.vi" Type="VI" URL="../CAEObj.evt remove.vi"/>
		<Item Name="CAEObj.acquire event lock.vi" Type="VI" URL="../CAEObj.acquire event lock.vi"/>
		<Item Name="CAEObj.release event lock.vi" Type="VI" URL="../CAEObj.release event lock.vi"/>
		<Item Name="CAEObj.evt lock.vi" Type="VI" URL="../CAEObj.evt lock.vi"/>
		<Item Name="CAEObj.i attribute.ctl" Type="VI" URL="../CAEObj.i attribute.ctl"/>
		<Item Name="CAEObj.i attribute.vi" Type="VI" URL="../CAEObj.i attribute.vi"/>
		<Item Name="CAEObj.URL keys 2 DIM service parameters.vi" Type="VI" URL="../CAEObj.URL keys 2 DIM service parameters.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
	</Item>
	<Item Name="CAEObj.contents.vi" Type="VI" URL="../CAEObj.contents.vi"/>
</Library>
