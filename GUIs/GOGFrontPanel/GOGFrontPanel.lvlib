﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">Provides frontpanel for user friendly use of methodes with parameters of simple data type. This class is intended for use together with the GeneralObjectGUI.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2001  Dietrich Beck, Holger Brand, Mathias Richter
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de or h.brand@gsi.de
Last update: 20-Mar-2013

INFO2SF</Property>
	<Property Name="NI.Lib.FriendGUID" Type="Str">ecd9bb94-8957-4343-9418-167b3f6ee9b9</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*:!!!*Q(C=\:4"&lt;&gt;N!%%7`D!$/R1&gt;W%-AF4!5"6)*Z^GV;U.6'9E!N4!NK1:=5-#WI"=(((!)QD[O2&lt;=3W;#-RY!$?Z:$=0\MTD\P,F6LZ,(X3-&amp;6UN,T10T['OS&lt;XQ\A3GAWKTN6[D@__0ERU*0`L_$`],`)`XD*0_I=J``#-HX*Z?:TP@`@`5@\?0`V\8UVW?O-CT2IJFFBADNH_NUXU2%`U2%`U2!`U1!`U1!`U1(&gt;U2X&gt;U2X&gt;U2T&gt;U1T&gt;U1T&gt;U/ZQ&gt;[%)8OKS&gt;$EHS*&amp;%3.!G1&gt;!:&amp;S3XB38A3HI3(JB+?B#@B38A3(LIIY5FY%J[%*_&amp;BG"+?B#@B38A3(E)VEGQ6(:[%B`!+?!+?A#@A#8B)K9!H!!C3"9'$)'!I=!9P!5`!%`$QKI!HY!FY!J[!"\=#HI!HY!FY!B['N&amp;G*2D.7&gt;(A))Y@(Y8&amp;Y("[(B^"S?"Q?B]@B=8B)*Y@(Y8%AH)2/="$E$()[/!W(R_(B)9@(Y8&amp;Y("[("V&gt;&lt;)7]T-^+-&amp;2U?A]@A-8A-(I/(%$*Y$"[$R_!R?!ALA]@A-8A-(I/(6$*Y$"[$RQ!REJ*?2D"DI.(*%!Q?LL:&lt;L+V3."*LN8\.`5:6&lt;5$6RF*N'.6'5#WQ;O&amp;5#[+;;.5%KC:'^='K$V%"KB+L!KI[;M&gt;^CWWQ.&lt;&lt;#FFC0,&lt;!ZVIV$`X((X7[H\8;LT7;D^8KNV7KFZ8+JPO_V7#QUH]`6&gt;&gt;X&gt;;@7&amp;OC_TQ\FUR@NIX]\0LL^@&gt;&amp;^(O_G\;^KTAW`34G_RH^APWD_/W0W9`&lt;HU(MZ'H7BY=-QT2\]"#+R(Q1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.30.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Friends List" Type="Friends List">
		<Item Name="GeneralObjectGUI.lvlib" Type="Friended Library" URL="../../GeneralObjectGUI/GeneralObjectGUI.lvlib"/>
		<Item Name="DeviceBaseGUI.lvlib" Type="Friended Library" URL="../../GOGUserGUIs/DeviceBaseGUI/DeviceBaseGUI.lvlib"/>
		<Item Name="DSCAlarmGUI.lvlib" Type="Friended Library" URL="../../GOGUserGUIs/DSCAlarmGUI/DSCAlarmGUI.lvlib"/>
		<Item Name="GOGControllerGUI.lvlib" Type="Friended Library" URL="../../GOGControllerGUI/GOGControllerGUI.lvlib"/>
		<Item Name="CS_Start.vi" Type="Friended VI" URL="../../../../../CS_Start.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="GOGFrontPanel.constructor.vi" Type="VI" URL="../GOGFrontPanel.constructor.vi"/>
		<Item Name="GOGFrontPanel.destructor.vi" Type="VI" URL="../GOGFrontPanel.destructor.vi"/>
		<Item Name="GOGFrontPanel.get data to modify.vi" Type="VI" URL="../GOGFrontPanel.get data to modify.vi"/>
		<Item Name="GOGFrontPanel.get library version.vi" Type="VI" URL="../GOGFrontPanel.get library version.vi"/>
		<Item Name="GOGFrontPanel.panel.vi" Type="VI" URL="../GOGFrontPanel.panel.vi"/>
		<Item Name="GOGFrontPanel.set modified data.vi" Type="VI" URL="../GOGFrontPanel.set modified data.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="GOGFrontPanel.get i attribute.vi" Type="VI" URL="../GOGFrontPanel.get i attribute.vi"/>
		<Item Name="GOGFrontPanel.set i attribute.vi" Type="VI" URL="../GOGFrontPanel.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="GOGFrontPanel.parameter.ctl" Type="VI" URL="../GOGFrontPanel.parameter.ctl"/>
		<Item Name="GOGFrontPanel.construct setting file path.vi" Type="VI" URL="../GOGFrontPanel.construct setting file path.vi"/>
		<Item Name="GOGFrontPanel.execute method.vi" Type="VI" URL="../GOGFrontPanel.execute method.vi"/>
		<Item Name="GOGFrontPanel.i attribute.vi" Type="VI" URL="../GOGFrontPanel.i attribute.vi"/>
		<Item Name="GOGFrontPanel.load settings.vi" Type="VI" URL="../GOGFrontPanel.load settings.vi"/>
		<Item Name="GOGFrontPanel.save settings.vi" Type="VI" URL="../GOGFrontPanel.save settings.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="forFriendsOnly" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">4</Property>
		<Item Name="GOGFrontPanel.i attribute.ctl" Type="VI" URL="../GOGFrontPanel.i attribute.ctl"/>
	</Item>
	<Item Name="GOGFrontPanel.contents.vi" Type="VI" URL="../GOGFrontPanel.contents.vi"/>
	<Item Name="GOGFrontPanel.rtm" Type="Document" URL="../GOGFrontPanel.rtm"/>
</Library>
