﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">In case unbuffered message events (notifiers) are defined in an object of the CAEObj class (or it's children), an object of the NotifierObj class is created for handling these events. 

Then, the NotifierObj waits for a notification. When a notification is received, the message received is stored in the NotifierObj and an event is sent to the assiciated object via the message queue. Then, the associated object retrieves the message from the NotifierObj via a direct method call. 

IMPORTANT: Note that this library should only be used by CEAObj.lvlib.

Each associated object has none (if not notifications are used) or exactly one NotifierObj. All notifaction events defined for the associated object are handeld by the same NotifierObj.

author: Dietrich Beck, GSI
maintainer: Dietrich Beck, GSI; d.beck@gsi.de

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: M.Richter@gsi.de, H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 17-JUN-2004

INFO2SF
</Property>
	<Property Name="NI.Lib.FriendGUID" Type="Str">694b5520-491b-42d5-ba56-017fd366ed1f</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+$!!!*Q(C=\:1R=B."%%7`Q)&amp;63B"6Z#Z&gt;I4.C=12&gt;I;_A6!F6#CFH(@A!VB'MB!.U3KAL[!L,W^E73'8D"1R6"-SIN4O`?\L@TM[/V.KV&gt;+6OL/H:^B0_PL&gt;@OW`C-+]*8?E6-\B,U"0_'H3HS'@G&gt;^`$+^O4^6`[@0`^Z].(QM6I2ON_[-@&gt;&gt;:="Z`Z_&gt;N&gt;&gt;:PB6`WTW_`0(_%;@&lt;X2^(L?8_]=`\]VIU&amp;^OUO4U.397G'-W@+7*HOC*HOC*HOC"(OC"(OC"(OC/\OC/\OC/\OC'&lt;OC'&lt;OC'&lt;K@4!FXI1J?V9S)JHB2+EC9*EG"1F0QF0!F0QJ0Q-&amp;4#E`!E0!F0QE/)%J[%*_&amp;*?")?JCHB38A3HI1HY3&amp;6)]H7U?&amp;*?%CPA#@A#8A#HI#(EAJY!I#A7*!Y3!+'!G&gt;Q%`!%0!%0NQJY!J[!*_!*?(!LY!FY!J[!*_"B3FO6;$2^2Y?(.(*Y("[(R_&amp;R?%ANB]@B=8A=(I?(=H*Y("Y(QCHI*!&gt;"TC1HQ"EY0!Y0&amp;TE]$I`$Y`!Y0,D;'`+W-DV.X^(B-8A-(I0(Y$&amp;Y3#'$R_!R?!Q?AY?U-HA-(I0(Y$&amp;Y+#7$R_!R?!Q1ISDF:31T*BJ"BG$Q]'O\R&gt;J&lt;CE:CL&gt;?H/7R5V1:5&lt;3T6BF&amp;N".5,6LUYV1N2,&lt;2K!65,IXJAV9/I!&amp;7&amp;61F6A4LS@]$WW!\&lt;9GNMB3WR"4&lt;PJ`\BQ/0RK-0BI0V_L^VOJ_VWK`6[L&gt;6KJ?6SK=6CI@F]`OWUOK%0&lt;8)[FT&lt;=^X&lt;`@HLX=0POQ]0NFY^=6`&gt;PJX=HX[C^`DT:8.V-.N.0E]V]/^C&lt;[_(;;\WPDTG&lt;-ZR,`],:K&amp;@KTIZZVOALO#"F+A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.21.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Friends List" Type="Friends List">
		<Item Name="CAEObj.lvlib" Type="Friended Library" URL="../../CAEObj/CAEObj.lvlib"/>
		<Item Name="CSClassUtilities.TestClass.vi" Type="Friended VI" URL="../../../../CSUtilities/CSClassUtilities.TestClass.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="NotifierObj.add notifier.vi" Type="VI" URL="../NotifierObj.add notifier.vi"/>
		<Item Name="NotifierObj.constructor.vi" Type="VI" URL="../NotifierObj.constructor.vi"/>
		<Item Name="NotifierObj.destructor.vi" Type="VI" URL="../NotifierObj.destructor.vi"/>
		<Item Name="NotifierObj.get data to modify.vi" Type="VI" URL="../NotifierObj.get data to modify.vi"/>
		<Item Name="NotifierObj.get library version.vi" Type="VI" URL="../NotifierObj.get library version.vi"/>
		<Item Name="NotifierObj.get notifier message.vi" Type="VI" URL="../NotifierObj.get notifier message.vi"/>
		<Item Name="NotifierObj.remove all notifiers.vi" Type="VI" URL="../NotifierObj.remove all notifiers.vi"/>
		<Item Name="NotifierObj.remove notifier.vi" Type="VI" URL="../NotifierObj.remove notifier.vi"/>
		<Item Name="NotifierObj.set modified data.vi" Type="VI" URL="../NotifierObj.set modified data.vi"/>
		<Item Name="NotifierObj.thread.vi" Type="VI" URL="../NotifierObj.thread.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">4</Property>
		<Item Name="NotifierObj.get i attribute.vi" Type="VI" URL="../NotifierObj.get i attribute.vi"/>
		<Item Name="NotifierObj.set i attribute.vi" Type="VI" URL="../NotifierObj.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="NotifierObj.get command ID.vi" Type="VI" URL="../NotifierObj.get command ID.vi"/>
		<Item Name="NotifierObj.i attribute.ctl" Type="VI" URL="../NotifierObj.i attribute.ctl"/>
		<Item Name="NotifierObj.i attribute.vi" Type="VI" URL="../NotifierObj.i attribute.vi"/>
		<Item Name="NotifierObj.notify associated object.vi" Type="VI" URL="../NotifierObj.notify associated object.vi"/>
		<Item Name="NotifierObj.wait 4 notification.vi" Type="VI" URL="../NotifierObj.wait 4 notification.vi"/>
	</Item>
	<Item Name="NotifierObj.contents.vi" Type="VI" URL="../NotifierObj.contents.vi"/>
</Library>
