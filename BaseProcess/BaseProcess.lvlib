﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">The typical base class used for device processes. (Almost) all classes of the framework will inherit and be direct children of this class.

The BaseProcess class provides the following:
- "Call" mechanism. Alls events except notification are buffered.
-- synchronous call: caller waits for answer from callee
-- asynchronous call:  caller does not wait for an answer from callee. The answer is sent to a specified object
-- simple call: callee will not answer
-- calls can be received via message queue or notification
- event thread: handles events defined in activate and calls method ProcCases of THIS class.
- periodic action thread: handles periodic action and calls method ProcPeriodic of THIS class.

If "data in" is empty, the constructor tries to retrieve attribute data from the data base. In case "data in" is not empty, the constructor assumes that "data in" contains attribute data of THIS class flattened to string (this feature should only be used in the "local" LabVIEW execution system). In case "data in" yields "N/A" the constructor does not read attribute data from the database and will just initialize the attribute data of THIS class with default values.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI; d.neidherr@gsi.de

License Agreement for this software:

Copyright (C) 2001  Dietrich Beck, Holger Brand, Mathias Richter
GSI Helmholtzzentrum für Schwerionenforschung GmbH
Planckstraße 1
D-64291 Darmstadt
Germany

Contact: d.beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by  the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.neidherr@gsi.de or h.brand@gsi.de
Last update: 20-Mar-2013

INFO2SF</Property>
	<Property Name="NI.Lib.FriendGUID" Type="Str">222dd521-1328-455b-b29d-6531fbb4b74e</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*E!!!*Q(C=\:1R&lt;NN!%%7`!B='5A3]A;(/&lt;O=+KFQ;5/6_LK"7J6I8"D*)F]ZK8?I+=Y.!6^!*!N#0SS5NQ99:)!Y1Q.\65/3@W:H(X?6+J:V,:WKHGNZM@_)P-@V`7_R:[B`'XEMHQGCNBL%P`-_DW[/3J_06DP7FIW,P]([@`P(BR;9Z^8_N\;0[*_&lt;HF@&lt;X`OH0?TU:^)_&lt;."O_]-1#=]T[,T@2%TX2%TX2%TX1!TX1!TX1!^X2(&gt;X2(&gt;X2(&gt;X1$&gt;X1$&gt;X1&lt;4A^U)5O&gt;&amp;EZ':,C3;%E;:)A#1:&amp;S38B38A3HI3(2S5]#5`#E`!E0)1IY5FY%J[%*_&amp;BG"+?B#@B38A3(F)6ECQ&gt;(:[%B`1+?!+?A#@A#8AIK9!H!!C+"9G$*'!I=!9X!5`!%`"QKY!HY!FY!J[!"\=#HI!HY!FY!B['F&amp;G*1N.V&gt;(B))Y@(Y8&amp;Y("[(B^2S?"Q?B]@B=8AI*Y@(Y8%AH)*/=B$E$()#H!?(R_(B4Q[0Q_0Q/$Q/$[[S1FZGJK0J/DI]"I`"9`!90!90+74Q'$Q'D]&amp;D]*"7"I`"9`!90!90J74Q'$Q'DQ&amp;C&amp;+7]D'4'1#0)%!Q?@G7X7&amp;GF+#27?PUU_YWKOA&amp;6.Z&lt;KBF(&gt;#+I,L,JQKAOC/N'K%[A[-;IPL0ICKI#KB654KA&lt;KQ(70\&lt;!NNM&amp;7W"*&lt;9(/M[9;_=_$B=."_P^&gt;ON^.WO^6GM^&amp;KN&gt;*SO&gt;2CM&gt;"]0F@4./.J&gt;5(PWWQYF^&lt;=&gt;`&lt;^RW.T@X0V%,_O(O[P\4*O@^Y.PEH\^HOW0D^\W\K9IT(^O@1`H)X[IP&lt;IG'?/HA$3=GA:!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">3.30.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Friends List" Type="Friends List">
		<Item Name="CS_Start.vi" Type="Friended VI" URL="../../../../CS_Start.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="BaseProcess.call process 1.vi" Type="VI" URL="../BaseProcess.call process 1.vi"/>
		<Item Name="BaseProcess.call process 2.vi" Type="VI" URL="../BaseProcess.call process 2.vi"/>
		<Item Name="BaseProcess.call process.vi" Type="VI" URL="../BaseProcess.call process.vi"/>
		<Item Name="BaseProcess.check process.vi" Type="VI" URL="../BaseProcess.check process.vi"/>
		<Item Name="BaseProcess.constructor.vi" Type="VI" URL="../BaseProcess.constructor.vi"/>
		<Item Name="BaseProcess.destructor.vi" Type="VI" URL="../BaseProcess.destructor.vi"/>
		<Item Name="BaseProcess.fill baseproc attrib.vi" Type="VI" URL="../BaseProcess.fill baseproc attrib.vi"/>
		<Item Name="BaseProcess.get data to modify.vi" Type="VI" URL="../BaseProcess.get data to modify.vi"/>
		<Item Name="BaseProcess.get descriptors.vi" Type="VI" URL="../BaseProcess.get descriptors.vi"/>
		<Item Name="BaseProcess.get library version.vi" Type="VI" URL="../BaseProcess.get library version.vi"/>
		<Item Name="BaseProcess.get version of library.vi" Type="VI" URL="../BaseProcess.get version of library.vi"/>
		<Item Name="BaseProcess.ping.vi" Type="VI" URL="../BaseProcess.ping.vi"/>
		<Item Name="BaseProcess.Reset.vi" Type="VI" URL="../BaseProcess.Reset.vi"/>
		<Item Name="BaseProcess.set evt preset.vi" Type="VI" URL="../BaseProcess.set evt preset.vi"/>
		<Item Name="BaseProcess.set modified data.vi" Type="VI" URL="../BaseProcess.set modified data.vi"/>
		<Item Name="BaseProcess.set pdc interval.vi" Type="VI" URL="../BaseProcess.set pdc interval.vi"/>
		<Item Name="BaseProcess.set pdc preset.vi" Type="VI" URL="../BaseProcess.set pdc preset.vi"/>
		<Item Name="BaseProcess.start.vi" Type="VI" URL="../BaseProcess.start.vi"/>
		<Item Name="BaseProcess.stop.vi" Type="VI" URL="../BaseProcess.stop.vi"/>
		<Item Name="BaseProcess.threads.vi" Type="VI" URL="../BaseProcess.threads.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="BaseProcess.add descriptors.vi" Type="VI" URL="../BaseProcess.add descriptors.vi"/>
		<Item Name="BaseProcess.descriptor definition.ctl" Type="VI" URL="../BaseProcess.descriptor definition.ctl"/>
		<Item Name="BaseProcess.descriptor string definition.ctl" Type="VI" URL="../BaseProcess.descriptor string definition.ctl"/>
		<Item Name="BaseProcess.event definition.ctl" Type="VI" URL="../BaseProcess.event definition.ctl"/>
		<Item Name="BaseProcess.evt create.vi" Type="VI" URL="../BaseProcess.evt create.vi"/>
		<Item Name="BaseProcess.evt delete.vi" Type="VI" URL="../BaseProcess.evt delete.vi"/>
		<Item Name="BaseProcess.exit loop.vi" Type="VI" URL="../BaseProcess.exit loop.vi"/>
		<Item Name="BaseProcess.get event timeout.vi" Type="VI" URL="../BaseProcess.get event timeout.vi"/>
		<Item Name="BaseProcess.get i attribute.vi" Type="VI" URL="../BaseProcess.get i attribute.vi"/>
		<Item Name="BaseProcess.get pdc interval.vi" Type="VI" URL="../BaseProcess.get pdc interval.vi"/>
		<Item Name="BaseProcess.get pdc loop counter.vi" Type="VI" URL="../BaseProcess.get pdc loop counter.vi"/>
		<Item Name="BaseProcess.get super proc.vi" Type="VI" URL="../BaseProcess.get super proc.vi"/>
		<Item Name="BaseProcess.ProcCases.vi" Type="VI" URL="../BaseProcess.ProcCases.vi"/>
		<Item Name="BaseProcess.ProcPeriodic.vi" Type="VI" URL="../BaseProcess.ProcPeriodic.vi"/>
		<Item Name="BaseProcess.set event error.vi" Type="VI" URL="../BaseProcess.set event error.vi"/>
		<Item Name="BaseProcess.set event status.vi" Type="VI" URL="../BaseProcess.set event status.vi"/>
		<Item Name="BaseProcess.set i attribute.vi" Type="VI" URL="../BaseProcess.set i attribute.vi"/>
		<Item Name="BaseProcess.set periodic error.vi" Type="VI" URL="../BaseProcess.set periodic error.vi"/>
		<Item Name="BaseProcess.set periodic status.vi" Type="VI" URL="../BaseProcess.set periodic status.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="BaseProcess.answer call.vi" Type="VI" URL="../BaseProcess.answer call.vi"/>
		<Item Name="BaseProcess.call proc cases.vi" Type="VI" URL="../BaseProcess.call proc cases.vi"/>
		<Item Name="BaseProcess.call proc periodic.vi" Type="VI" URL="../BaseProcess.call proc periodic.vi"/>
		<Item Name="BaseProcess.i attribute.ctl" Type="VI" URL="../BaseProcess.i attribute.ctl"/>
		<Item Name="BaseProcess.i attribute.vi" Type="VI" URL="../BaseProcess.i attribute.vi"/>
		<Item Name="BaseProcess.i loop attribute.vi" Type="VI" URL="../BaseProcess.i loop attribute.vi"/>
		<Item Name="BaseProcess.ProcConstructor.vi" Type="VI" URL="../BaseProcess.ProcConstructor.vi"/>
		<Item Name="BaseProcess.ProcDestructor.vi" Type="VI" URL="../BaseProcess.ProcDestructor.vi"/>
		<Item Name="BaseProcess.ProcEvents.vi" Type="VI" URL="../BaseProcess.ProcEvents.vi"/>
		<Item Name="BaseProcess.wait call.vi" Type="VI" URL="../BaseProcess.wait call.vi"/>
	</Item>
	<Item Name="forFriendsOnly" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">4</Property>
		<Item Name="BaseProcess.temp queue.vi" Type="VI" URL="../BaseProcess.temp queue.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="INHERITPRIV.ProcCases.vi" Type="VI" URL="../inheritance/INHERITPRIV.ProcCases.vi"/>
		<Item Name="INHERITPRIV.ProcEvents.vi" Type="VI" URL="../inheritance/INHERITPRIV.ProcEvents.vi"/>
		<Item Name="INHERITPRIV.ProcPeriodic.vi" Type="VI" URL="../inheritance/INHERITPRIV.ProcPeriodic.vi"/>
		<Item Name="INHERITPUB.constructor.vi" Type="VI" URL="../inheritance/INHERITPUB.constructor.vi"/>
		<Item Name="INHERITPUB.destructor.vi" Type="VI" URL="../inheritance/INHERITPUB.destructor.vi"/>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="PRIVATE.ProcEvents.vi" Type="VI" URL="../inheritance/PRIVATE.ProcEvents.vi"/>
		<Item Name="PRIVATE.ProcPeriodic.vi" Type="VI" URL="../inheritance/PRIVATE.ProcPeriodic.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="BaseProcess.contents.vi" Type="VI" URL="../BaseProcess.contents.vi"/>
	<Item Name="BaseProcess_db.ini" Type="Document" URL="../BaseProcess_db.ini"/>
</Library>
